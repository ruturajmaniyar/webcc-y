(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webcc.updateAction", {
        _init: function () {
            var self = this;
            $.proxy(self._answerActionUpdate, self)();
            $.proxy(self._generateUsername, self)();
            $.proxy(self._generatePassword, self)();
            $.proxy(self._showPassword, self)();
        },

        _answerActionUpdate: function () {
            var self = this;

            self.noAnswerActionElement = $('#sipendpoint-no_answer_action_id', self.element);
            self.noAnswerDestinationElement = $('#sipendpoint-no_answer_destination_id', self.element);

            if (self.noAnswerActionElement.val() === '2') {
                self.noAnswerDestinationElement.attr('disabled', false);
            } else {
                self.noAnswerDestinationElement.val('');
                self.noAnswerDestinationElement.attr('disabled', true);
            }

            $(self.noAnswerActionElement).change(function () {
                if (self.noAnswerActionElement.val() === '2') {
                    self.noAnswerDestinationElement.attr('disabled', false);
                } else {
                    self.noAnswerDestinationElement.val('0');
                    self.noAnswerDestinationElement.attr('disabled', true);
                }
            });
        },
        _generateUsername: function () {
            var self = this;

            self.sipEndpointUsernameElement = $('#sipsubscriber-username', self.element);
            self.sipEndpointUsernameActionElement = $('#sip-endpoint-username-regenerate', self.element);

            $(self.sipEndpointUsernameActionElement).click(function () {

                var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
                var maxLength = Math.floor(12 + Math.random()*(20+1 - 12));
                var username = '';
                for (var i=0; i<maxLength; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    username += chars.substring(rnum,rnum+1);
                }

                if(username !== ""){
                    return $(self.sipEndpointUsernameElement).val(username);
                }

                alert("Somewent went wrong, Please regenerate username.");
            });
        },
        _generatePassword: function () {
            var self =this;

            self.sipEndpointPasswordElement = $('#sipsubscriber-password', self.element);
            self.sipEndpointPasswordActionElement = $('#sip-endpoint-password-regenerate', self.element);

            $(self.sipEndpointPasswordActionElement).click(function () {

                var chars = "abcdefghiklmnopqrstuvwxyz+-$*!%ABCDEFGHIJKLMNOPQRSTUVWXTZ0123456789";
                var maxLength = Math.floor(12 + Math.random()*(20+1 - 12));
                var password = '';
                for (var i=0; i<maxLength; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    password += chars.substring(rnum,rnum+1);
                }

                if(password !== ""){
                    return $(self.sipEndpointPasswordElement).val(password);
                }

                alert("Somewent went wrong, Please regenerate passwords.");
            });
        },
        _showPassword: function () {
            var self =this;

            self.sipEndpointPasswordElement = $('#sipsubscriber-password', self.element);
            self.sipEndpointPasswordShowElement = $('#sip-endpoint-password-show', self.element);

            $(self.sipEndpointPasswordShowElement).click(function () {
                if($(self.sipEndpointPasswordElement).attr('type') === 'password'){
                    $(self.sipEndpointPasswordElement).attr('type', '');
                } else {
                    $(self.sipEndpointPasswordElement).attr('type', 'password');
                }
            });
        }
    });
}));

$(document).ready(function () {
    $("#sip-endpoint-form").updateAction();
});
