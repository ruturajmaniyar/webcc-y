(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webccy.callReason", {
        _create: function () {
            var self = this;

            $.proxy(self._mainCallReason, self)();
            $.proxy(self._addCallReason, self)();
            $.proxy(self._addCallAction, self)();
            $.proxy(self._removeCallReason, self)();
            $.proxy(self._removeCallAction, self)();
            $.proxy(self.mainCallReason, self)();
            $.proxy(self._removeChildElement, self)();
            $.proxy(self._appendErrors, self)();
        },

        _mainCallReason: function () {
            $('.append-element').each(
                function () {
                    var callActionLength = $('.each-call-action-block', $(this)).length;
                    var subReasonLength = $('.each-sub-reason-block', $(this)).length;
                    if (callActionLength > 0) {
                        $('#addSubRason').hide();
                        $('#addCallAction').hide();
                    } else {
                        $('#addSubRason').show();
                        $('#addCallAction').show();
                    }
                    if (subReasonLength > 0) {
                        $('#addCallAction').hide();
                    }
                }
            );
        },

        _addCallReason: function () {
            var self = this;
            self.subReason = $('#sub-reason-block');
            self.callReasonCount = $('#call-reason-count').text();
            self.subReason.css('display', 'none');
            $(document).on('click', '#addSubRason', function (e) {
                $('.append-element').each(
                    function () {
                        var length = $('.each-sub-reason-block', $(this)).length;
                        if (length < self.callReasonCount) {
                            $('.append-element').append(self.subReason.html());
                        } else {
                            alert('Call Reason limit reached.');
                        }
                    }
                );
                self._mainCallReason();
            })

        },

        _addCallAction: function () {
            var self = this;
            self.callAction = $('#call-action-block');
            self.callAction.css('display', 'none');
            $(document).on('click', '#addCallAction', function (e) {
                $('.append-element').append(self.callAction.html());
                self._mainCallReason();
            })
        },

        _removeCallReason: function () {
            var self = this;
            $(document).on('click', '.removeSubReason', function (e) {
                e.target.closest('.each-sub-reason-block').remove();
                $(".each-sub-reason-block").last().show();
                self._mainCallReason();
            });
        },

        _removeCallAction: function () {
            var self = this;
            $(document).on('click', '.removeActionMember', function (e) {
                e.target.closest('.each-call-action-block').remove();
                $(".call-reason").last().show();
                self._mainCallReason();
            });
        },

        _removeChildElement: function () {
            var self = this;
            $(document).on('click', '.removeChildElement', function (e) {
                e.target.closest('.child-sub-reason').remove();
                $(".sub-reason").last().show();
                self._mainCallReason();
                var length = $('div.sub-reason', $(this).parent().parent().parent().parent().children('.child-element')).length;
                var countButton = self.currentAppendedSubReason.children('.group-button').length;
                if (length == 0 && countButton == 0) {
                    self.currentAppendedSubReason.append("<div class='col-xl-4 col-md-6 col-xs-12 group-button'>" + $('.group-button').html() + "</div>");
                }
            });
        },

        mainCallReason: function () {
            $('#select-action').css('display', 'none');
            var self = this;
            self.callActionValue = $('#call-action-value');
            self.callDestination = $('#call-destination');
            self.callDestinationValue = $('#call-destination-value');
            self.nonCallActionValue = $('#non-call-action-value');
            self.nonCallDestination = $('#non-call-destination');
            self.nonCallDestinationValue = $('#non-call-destination-value');
            if (self.callActionValue.val() == '' || self.callActionValue.val() == '1') {
                self.callDestination.css('display', 'none');
            }
            $(document).on('change', '#call-action-value', function (e) {
                if (e.target.value == '' || e.target.value == '1') {
                    $('#call-destination').css('display', 'none');
                    $('#call-destination').val('');
                } else {
                    $('#call-destination').css('display', 'block');
                    $.get('/call-reason/get-call-destination', {'id': e.target.value}, function (response, status) {
                        var callDestination = $.parseJSON(response);
                        var callDestinationHTML = "";

                        $.each(callDestination, function (i, item) {
                            callDestinationHTML += "<option value='" + i + "'>" + item + "</option>";
                        });
                        $('#call-destination-value').html(callDestinationHTML);
                    });
                }
            });
            if (self.nonCallActionValue.val() == '') {
                self.nonCallDestination.css('display', 'none');
            }
            $(document).on('change', '#non-call-action-value', function (e) {
                if (e.target.value == '' || e.target.value == '1') {
                    $('#non-call-destination').css('display', 'none');
                    $('#call-destination').val('');
                } else {
                    $('#non-call-destination').css('display', 'block');
                    $.get('/call-reason/get-call-destination', {'id': e.target.value}, function (response, status) {
                        var callDestination = $.parseJSON(response);
                        var callDestinationHTML = "";

                        $.each(callDestination, function (i, item) {
                            callDestinationHTML += "<option value='" + i + "'>" + item + "</option>";
                        });
                        $('#non-call-destination-value').html(callDestinationHTML);
                    });
                }
            });
        },

        _appendErrors: function () {
            var self = this;
            self.callReasonErrors = $.parseJSON($('#call-reason-errors').text());

            $.each(self.callReasonErrors, function (index, value) {
                if (typeof value['reason'] === "undefined")
                    value['reason'] = '';

                var reason = '<div class="help-block help-block-error">' + value['reason'] + '</div>';

                var i = 0;
                $('.append-element').find('.each-sub-reason-block').each(function () {
                    if (i == index) {
                        if (value['reason'] != '') {
                            $(this).find('.each-sub-reason-block').addClass('has-error');
                            $(this).find('input[name="reason[]"]').after(reason);
                        }
                    }
                    i++;
                });
            });
        }
    });
}));

$(document).ready(function () {
    $("#call-reason-configuration-active-form").callReason();
});