(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webcc.sortableElements", {
        _init: function () {
            var self = this;
            $.proxy(self._sortable, self)();
            $.proxy(self._addRingMembers, self)();
            $.proxy(self._removeRingMembers, self)();
            $.proxy(self._appendErrors, self)();
        },

        _sortable: function () {
            var self = this;
            var list = document.getElementById('listWithHandle');

            Sortable.create(list, {
                handle: '.glyphicon-move',
                animation: 150
            });
            $('.hidden-ringgroup-member').css('display', 'none');
        },
        _addRingMembers: function () {
            var self = this;
            $(document).on('click', '#addMember', function (e) {
                $('#listWithHandle').append($('.hidden-ringgroup-member').html());
            });
            self._sortable();
        },
        _removeRingMembers: function () {
            var self = this;
            $(document).on('click', '.removeMemberBtn', function (e) {
                e.target.closest('.each-ringgroup-member').remove();
            });
            self._sortable();
        },
        _appendErrors: function () {
            var self = this;
            self.ringMemberError = $.parseJSON($('#ringgroup-member-errors').text());

            $.each(self.ringMemberError, function (index, value) {
                if (typeof value['sip_endpoint_id'] === "undefined")
                    value['sip_endpoint_id'] = '';
                if (typeof value['ring_timeout'] === "undefined")
                    value['ring_timeout'] = '';

                var sipEndpoint = '<div class="help-block help-block-error">' + value['sip_endpoint_id'] + '</div>';
                var ringTimeout = '<div class="help-block help-block-error">' + value['ring_timeout'] + '</div>';

                var i = 0;
                $('#listWithHandle').find('.each-ringgroup-member').each(function () {
                    if (i == index) {
                        if (value['sip_endpoint_id'] != '') {
                            $(this).find('.sip_endpoint_id').addClass('has-error');
                            $(this).find('select[name="sip_endpoint_id[]"]').after(sipEndpoint);
                        }
                        if (value['ring_timeout'] != '') {
                            $(this).find('.ring_timeout').addClass('has-error');
                            $(this).find('input[name="ring_timeout[]"]').after(ringTimeout);
                        }
                    }
                    i++;
                });
            });
        }
    });
}));

$(document).ready(function () {
    $("#ring-group-form").sortableElements();
});
