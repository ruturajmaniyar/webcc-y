(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webccy.officeHourDateTime", {
        _create: function () {
            var self = this;

            $.proxy(self.mainDate, self)();
            $.proxy(self.addDate, self)();
            $.proxy(self.removeDate, self)();

            $.proxy(self.mainTime, self)();
            $.proxy(self.addTime, self)();
            $.proxy(self.removeTime, self)();

            $.proxy(self.mainDateTime, self)();
            $.proxy(self.addDateTime, self)();
            $.proxy(self.removeDateTime, self)();

            $.proxy(self.intializeDateTime, self)();
            $.proxy(self.weekdays, self)();
            $.proxy(self.appendDateErrors, self)();
            $.proxy(self.appendTimeErrors, self)();
            $.proxy(self.appendHolidayErrors, self)();
        },

        intializeDateTime: function () {
            $('.datepicker').datepicker({
                 format: 'yyyy-mm-dd'
            });
            $('.timepicker').timepicker({
                timeFormat: 'HH:mm:ss',
                minuteStep: 1,
                defaultTime: 'null',
            });
            $('.holidayDpicker').datepicker({
                 format: 'yyyy-mm-dd'
            });
            $('.holidayTpicker').timepicker({
                timeFormat: 'HH:mm:ss',
                minuteStep: 1,
                defaultTime: 'null',
            });
        },
        weekdays: function () {
            $('select.weekdays').select2({
                width: '100%',
            });
        },


        mainDate: function () {
            var self = this;
            $('.hidden-dateslot').css('display', 'none');
            $('#office-hour-date-slot-id').each(
                function () {
                    var length = $('.each-dateslot', $(this)).length;
                    if (length === 1) {
                        $('#removeDate').hide();
                    }
                }
            );
            self.intializeDateTime();
        },
        addDate: function () {
            var self = this;
            $(document).on('click', '#addDate', function (e) {
                $('.officehour-dateslot').append($('.hidden-dateslot').html());
                $('#office-hour-date-slot-id').each(
                    function () {
                        var length = $('.each-dateslot', $(this)).length;
                        if (length > 1) {
                            $('#removeDate').show();
                        }
                    }
                );
                $('#office-hour-date-slot-id').each(
                    function () {
                        var length = $('.each-dateslot', $(this)).length;
                        if (length === 1) {
                            $('#removeDate').hide();
                        }
                    }
                );
                self.intializeDateTime();
            });
        },
        removeDate: function () {
            var self = this;
            $(document).on('click', '.removeDateBtn', function (e) {
                e.target.closest('.each-dateslot').remove();
                $(".each-dateslot").last().show();
                $('#office-hour-date-slot-id').each(
                    function () {
                        var length = $('.each-dateslot', $(this)).length;
                        if (length === 1) {
                            $('#removeDate').hide();
                        }
                    }
                );
                self.intializeDateTime();
            });
        },

        mainTime: function () {
            var self = this;
            $('.hidden-timeslot').css('display', 'none');
            $('#office-hour-time-slot-id').each(
                function () {
                    var length = $('.each-timeslot', $(this)).length;
                    if (length === 1) {
                        $('#removeTime').hide();
                    }
                }
            );
            self.intializeDateTime();
        },
        addTime: function () {
            var self = this;
            var element = $('.hidden-timeslot').html();
            $(document).on('click', '#addTime', function (e) {
                $('.officehour-timeslot').append(element);
                $('#office-hour-time-slot-id').each(function () {
                        var length = $('.each-timeslot', $(this)).length;
                        if (length > 1) {
                            $('#removeTime').show();
                        }
                        var timeSlotElement = $('.each-timeslot',$(this)).find('.days');
                        timeSlotElement.each(function(i){
                            var child = $(this).find('.weekdays');
                            child.attr('name','run_on_days['+i+'][]');
                        })
                    }
                );
                $('#office-hour-time-slot-id').each(function () {
                        var length = $('.each-timeslot', $(this)).length;
                        if (length === 1) {
                            $('#removeTime').hide();
                        }
                    }
                );
                self.intializeDateTime();
                self.weekdays();
            });
        },
        removeTime: function () {
            var self = this;
            $(document).on('click', '.removeTimeBtn', function (e) {
                e.target.closest('.each-timeslot').remove();
                $(".each-timeslot").last().show();
                $('#office-hour-time-slot-id').each(
                    function () {
                        var length = $('.each-timeslot', $(this)).length;
                        if (length === 1) {
                            $('#removeTime').hide();
                        }
                        var timeSlotElement = $('.each-timeslot',$(this)).find('.days');
                        timeSlotElement.each(function(i){
                            var child = $(this).find('.weekdays');
                            child.attr('name','run_on_days['+i+'][]');
                        })
                    }
                );
                self.intializeDateTime();
                self.weekdays();
            });
        },

        mainDateTime: function () {
            var self = this;
            $('.hidden-datetime').css('display', 'none');
            self.intializeDateTime();
        },
        addDateTime: function () {
            var self = this;
            $(document).on('click', '#addDateTime', function (e) {
                $('.holiday-datetime').append($('.hidden-datetime').html());
                self.intializeDateTime();
           });
        },
        removeDateTime: function () {
            var self = this;
            $(document).on('click', '.removeDateTimeBtn', function (e) {
                e.target.closest('.each-holiday-datetime').remove();
                $(".each-holiday-datetime").last().show();
                self.intializeDateTime();
            });
        },

        appendDateErrors: function () {
            var self = this;
            self.dateError = $.parseJSON($('#officehour-dateslot-errors').text());

            $.each(self.dateError, function (index, value) {
                if (typeof value['start_date'] === "undefined")
                    value['start_date'] = '';
                if (typeof value['end_date'] === "undefined")
                    value['end_date'] = '';

                var startError = '<div class="help-block help-block-error">' + value['start_date'] + '</div>';
                var endError = '<div class="help-block help-block-error">' + value['end_date'] + '</div>';

                var i = 0;
                $('.officehour-dateslot').find('.each-dateslot').each(function () {
                    if (i == index) {
                        if (value['start_date'] != '') {
                            $(this).find('.startdate').addClass('has-error');
                            $(this).find('div.startdate-error').after(startError);
                        }
                        if (value['end_date'] != '') {
                            $(this).find('.enddate').addClass('has-error');
                            $(this).find('div.enddate-error').after(endError);
                        }
                    }
                    i++;
                });
            });
        },

        appendTimeErrors: function () {
            var self = this;
            self.timeError = $.parseJSON($('#officehour-timeslot-errors').text());

            $.each(self.timeError, function (index, value) {
                if (typeof value['start_time'] === "undefined")
                    value['start_time'] = '';
                if (typeof value['end_time'] === "undefined")
                    value['end_time'] = '';
                if (typeof value['run_on_days'] === "undefined")
                    value['run_on_days'] = '';

                var startError = '<div class="help-block help-block-error">' + value['start_time'] + '</div>';
                var endError = '<div class="help-block help-block-error">' + value['end_time'] + '</div>';
                var dayError = '<div class="help-block help-block-error">' + value['run_on_days'] + '</div>';

                var i = 0;
                $('.officehour-timeslot').find('.each-timeslot').each(function () {
                    if (i == index) {
                        if (value['start_time'] != '') {
                            $(this).find('.starttime').addClass('has-error');
                            $(this).find('div.starttime-error').after(startError);
                        }
                        if (value['end_time'] != '') {
                            $(this).find('.endtime').addClass('has-error');
                            $(this).find('div.endtime-error').after(endError);
                        }
                        if (value['run_on_days'] != '') {
                            $(this).find('.days').addClass('has-error');
                            $(this).find('div.days-error').after(dayError);
                        }
                    }
                    i++;
                });
            });
        },

        appendHolidayErrors: function () {
            var self = this;
            self.holidayError = $.parseJSON($('#holiday-date-time-errors').text());

            $.each(self.holidayError, function (index, value) {
                if (typeof value['start_time'] === "undefined")
                    value['start_time'] = '';
                if (typeof value['end_time'] === "undefined")
                    value['end_time'] = '';

                var startTime = '<div class="help-block help-block-error">' + value['start_time'] + '</div>';
                var endTime = '<div class="help-block help-block-error">' + value['end_time'] + '</div>';

                var i = 0;
                $('.holiday-datetime').find('.each-holiday-datetime').each(function () {
                    if (i == index) {
                        if (value['start_time'] != '') {
                            $(this).find('.holidayStartDate').addClass('has-error');
                            $(this).find('div.holidayStartDate-error').after(startTime);
                        }
                        if (value['end_time'] != '') {
                            $(this).find('.holidayEndDate').addClass('has-error');
                            $(this).find('div.holidayEndDate-error').after(endTime);
                        }
                    }
                    i++;
                });
            });
        },
    });
}));

$(document).ready(function () {
    $("#office-hour-form").officeHourDateTime();
});