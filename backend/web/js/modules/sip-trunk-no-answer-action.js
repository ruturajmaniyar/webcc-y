(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webcc.updateAction", {
        _init: function () {
            var self = this;
            $.proxy(self._answerActionUpdate, self)();
        },

        _answerActionUpdate: function () {
            var self = this;

            self.noAnswerActionElement = $('#siptrunk-no_answer_action_id', self.element);
            self.noAnswerDestinationElement = $('#siptrunk-no_answer_destination_id', self.element);

            if (self.noAnswerActionElement.val() === '2') {
                self.noAnswerDestinationElement.attr('disabled', false);
            } else {
                self.noAnswerDestinationElement.val('');
                self.noAnswerDestinationElement.attr('disabled', true);
            }

            $(self.noAnswerActionElement).change(function () {
                if (self.noAnswerActionElement.val() === '2') {
                    self.noAnswerDestinationElement.attr('disabled', false);
                } else {
                    self.noAnswerDestinationElement.val('0');
                    self.noAnswerDestinationElement.attr('disabled', true);
                }
            });

        }
    });
}));

$(document).ready(function () {
    $("#voicemail-form").updateAction();
});
