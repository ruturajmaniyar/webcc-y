(function (factory) {
    'use strict';
    factory(window.jQuery);
}(function ($) {
    'use strict';
    $.widget("webccy.blacklistedIP", {
        _create: function () {
            var self = this;
            $.proxy(self.mainIp, self)();
            $.proxy(self.addIp, self)();
            $.proxy(self.removeIp, self)();
            $.proxy(self.appendErrors, self)();
        },

        mainIp: function () {
            $('.hidden-ip').css('display', 'none');
            /*$('#appendMain').each(
                function () {
                    var length = $('.eachRule', $(this)).length;
                    if (length === 1) {
                        $('#removeBtn').hide();
                    }
                }
            );*/
        },
        addIp: function () {
            var self = this;
            $(document).on('click', '#addIp', function (e) {
                $('.blacklisted-ip').append($('.hidden-ip').html());
                /*$('#appendMain').each(
                    function () {
                        var length = $('.eachRule', $(this)).length;
                        if (length > 1) {
                            $('#removeBtn').show();
                        }
                    }
                );
                $('#appendMain').each(
                    function () {
                        var length = $('.eachRule', $(this)).length;
                        if (length === 1) {
                            $('#removeBtn').hide();
                        }
                    }
                );*/
            });
        },
        removeIp: function () {
            $(document).on('click', '.removeIpBtn', function (e) {
                console.log(e.target.closest('.each-blacklisted-ip'));
                e.target.closest('.each-blacklisted-ip').remove();
                $(".each-blacklisted-ip").last().show();
                /*$('#appendMain').each(
                    function () {
                        var length = $('.eachRule', $(this)).length;
                        if (length === 1) {
                            $('#removeBtn').hide();
                        }
                    }
                );*/
            });
        },
        appendErrors: function () {
            var self = this;
            self.ipError = $.parseJSON($('#blacklisted_errors').text());

            $.each(self.ipError, function (index, value) {
                if (typeof value['ip'] === "undefined")
                    value['ip'] = '';
                if (typeof value['netmask'] === "undefined")
                    value['netmask'] = '';
                if (typeof value['duration'] === "undefined")
                    value['duration'] = '';

                var ipError = '<div class="help-block help-block-error">' + value['ip'] + '</div>';
                var netmaskError = '<div class="help-block help-block-error">' + value['netmask'] + '</div>';
                var durationError = '<div class="help-block help-block-error">' + value['duration'] + '</div>';
                var i = 0;
                $('.blacklisted-ip').find('.each-blacklisted-ip').each(function () {
                    if (i == index) {
                        if (value['ip'] != '') {
                            $(this).find('.ip').addClass('has-error');
                            $(this).find('input[name="ip[]"]').after(ipError);
                        }
                        if (value['netmask'] != '') {
                            $(this).find('.netmask').addClass('has-error');
                            $(this).find('input[name="netmask[]"]').after(netmaskError);
                        }
                        if (value['duration'] != '') {
                            $(this).find('.duration').addClass('has-error');
                            $(this).find('input[name="duration[]"]').after(durationError);
                        }
                    }
                    i++;
                });
            });
        }
    });
}));

$(document).ready(function () {
    $("#company-blacklisted-ip-form").blacklistedIP();
});