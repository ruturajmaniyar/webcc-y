jQuery(document).ready(function () {
    // Select2
    if ($(".select2").length) {
        $(".select2").select2({
            width: '100%'
        });
    }

    if ($('#spinner4').length) {
        $('#spinner4').spinner({value: 0, step: 5, min: 0, max: 200});
        $('#spinner4').click(function () {
            $('#spinner4').spinner({value: 0, step: 5, min: 0, max: 200});
        });
    }

    if ($(".only-numeric").length) {
        $(".only-numeric").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }
});

/**
 * @param t
 * @param search_form_selector
 */
var change_records_per_page = function (t, search_form_selector) {
    $per_page = $(search_form_selector).find('input[name="per-page"]');
    if ($per_page.length) {
        $per_page.val(t.val());
    } else {
        $(search_form_selector).append('<input type="hidden" name="per-page" value="' + t.val() + '" >');
    }
    $(search_form_selector).submit();
};