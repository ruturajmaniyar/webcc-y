<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyCallSettingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-group company-call-setting-search-class" id="company-call-setting-search-id">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#company-call-setting-search-id"
                   href="#company-call-setting-search-id-collapse"
                   aria-expanded="false" class="collapsed">
                    <?= Yii::t('app', 'Search') ?>
                </a>
            </h4>
        </div>
    <?php $form = ActiveForm::begin([
        'id' => 'company-call-setting-search-form',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
        <div class="panel-collapse collapse" id="company-call-setting-search-id-collapse" aria-expanded="false">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'concurrent_call_per_ip',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>

                        <?= $form->field($model, 'daily_call_per_ip',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>
                    </div>
                </div>

                <div class="break-rule"></div>
                <div class="row">
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'Search'),
                            ['class' => 'btn btn-form btn-primary btn-rounded']) ?>
                        <?= Html::a(Yii::t('app', 'Reset'), ['index', 'page' => Yii::$app->session->get('page')],
                            ['class' => 'btn btn-form btn-default btn-rounded']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
