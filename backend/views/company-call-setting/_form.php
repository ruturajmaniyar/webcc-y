<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyCallSetting */
/* @var $form yii\widgets\ActiveForm */
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

<div class="company-call-setting-form-class" id="company-call-setting-form-id">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'company-call-setting-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>

    <div class="row">
        <div class="form-group">
            <div id="concurrent_call_per_ip_spinner">
                <?= $form->field($model, 'concurrent_call_per_ip',
                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                    'class' => 'spinner-input form-control',
                    'maxlength' => true
                ]) ?>
            </div>
            <div id="daily_call_per_ip_spinner">
                <?= $form->field($model, 'daily_call_per_ip',
                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                    'class' => 'spinner-input form-control',
                    'maxlength' => true
                ]) ?>
            </div>
        </div>
    </div>

    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?php if ($model->isNewRecord) {
                echo Html::submitButton(Yii::t('app', 'Save'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
            } else {
                echo Html::submitButton(Yii::t('app', 'Update'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                echo Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
            }
            echo Html::a(Yii::t('app', 'Back'),
                ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJsFile('@web/library/spinner/spinner.min.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>
