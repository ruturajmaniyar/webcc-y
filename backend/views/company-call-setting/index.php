<?php

use backend\models\CompanyCallSetting;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii2mod\editable\EditableColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CompanyCallSettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Company Call Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-call-setting-index-class" id="company-call-setting-index-id">
    <?php
    if (CompanyCallSetting::find()->where(['company_id' => Yii::$app->user->identity->company_id])->count() < 1) {
        echo Html::a(Yii::t('app', 'Add New'), ['create'],
            ['class' => 'btn btn-success pull-right btn-rounded']);
    }
    ?>
    <h1><?php echo Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'pjax-company-call-setting-index']); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card card-default">
                <div class="card-header">
                    <?= GridView::widget([
                        'id' => 'grid-company-call-setting-index',
                        'dataProvider' => $dataProvider,
                        'layout' => Yii::$app->helper->get_layout_str('#company-call-setting-search-form'),
                        'showOnEmpty' => false,
                        'options' => [
                            'class' => 'grid-view-color text-center',
                        ],
                        'columns' => [
                            [
                                'class' => EditableColumn::class,
                                'attribute' => 'concurrent_call_per_ip',
                                'url' => ['change-concurrent-call-ip'],
                                'type' => 'text',
                                'contentOptions' => ['class' => 'text-center', 'style' => 'width: 2%;'],
                                'header' => 'ConCurrent Call Per IP',
                                'clientOptions' => [
                                    'tpl' => '<input type="number" class="form-control" maxlength="3">',
                                    'success' => (new \yii\web\JsExpression('function() {
                            $.pjax.reload({container: "#grid-company-call-setting-index"}); // this code for update grid
                        }'))
                                ],
                            ],
                            [
                                'class' => EditableColumn::class,
                                'attribute' => 'daily_call_per_ip',
                                'url' => ['change-daily-call-ip'],
                                'type' => 'text',
                                'contentOptions' => ['class' => 'text-center', 'style' => 'width: 5%;'],
                                'header' => 'Daily Call Per IP',
                                'enableSorting' => true,
                                'clientOptions' => [
                                    'tpl' => '<input type="number" class="form-control" maxlength="3">',
                                    'success' => (new \yii\web\JsExpression('function() {
                        $.pjax.reload({container: "#grid-company-call-setting-index"}); // this code for update grid
                    }'))
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>