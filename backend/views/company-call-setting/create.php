<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CompanyCallSetting */

$this->title = Yii::t('app', 'Create Company Call Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company Call Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-call-setting-create-class" id="company-call-setting-create-id">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
