<?php

use backend\models\OfficeHourTimeSlot;
use backend\models\Timezone;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OfficeHour */
/* @var $form yii\widgets\ActiveForm */
/* @var $dateErrors array */
/* @var $timeErrors array */
/* @var $holidayErrors array */
/* @var $dateSlotData array */
/* @var $timeSlotData array */
/* @var $holidayData array */

isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

    <div class="office-hour-form-class" id="office-hour-form-id">

        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'office-hour-form',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]
        ]); ?>

        <div class="row">
            <div class="form-group">
                <?= $form->field($model, 'name',
                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                    'class' => 'form-control',
                    'maxlength' => true
                ]) ?>
                <?= $form->field($model, 'timezone_id', ['options' => ['class' => 'col-xs-12 col-md-6']])
                    ->dropDownList(Timezone::getTimezoneList(),
                        ['class' => 'select2 select2-offscreen', 'prompt' => 'Select Timezone']) ?>
            </div>
        </div>
        <div class="break-rule"></div>
        <div class="row">
            <div class="form-group">
                <div class="col-sm-2">
                    <h4><label>Date Slot</label></h4>
                </div>
                <div class="col-sm-2">
                    <a id="addDate" href="javascript:void(0);"
                       class="btn btn-primary btn-sm fa fa-plus mb-2">
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div id="officehour-dateslot-errors" hidden="true"><?= json_encode($dateErrors) ?></div>
        <div class="officehour-dateslot" id="office-hour-date-slot-id">
            <?php
            if (!empty($dateSlotData)) {
                foreach ($dateSlotData as $key => $dateSlot) {
                    ?>
                    <div class="each-dateslot">
                        <div class="row">
                            <div class="startdate col-xs-4 col-md-3">
                                <div class="input-group startdate-error">
                                    <?= Html::textInput('start_date[]', $dateSlot['start_date'], [
                                        'class' => 'form-control datepicker',
                                        'placeholder' => 'Start Date (yyyy-mm-dd)'
                                    ]); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="enddate col-xs-4 col-md-3">
                                <div class="input-group enddate-error">
                                    <?= Html::textInput('end_date[]', $dateSlot['end_date'], [
                                        'class' => 'form-control datepicker',
                                        'placeholder' => 'End Date (yyyy-mm-dd)'
                                    ]); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-xs-4 col-md-3" id="removeDate">
                                <a href="javascript:void(0);"
                                   class="btn btn-danger btn-sm fa fa-minus removeDateBtn">
                                </a>
                            </div>
                        </div>
                        <br>
                    </div>
                <?php }
            } else { ?>
                <div class="each-dateslot">
                    <div class="row">
                        <div class="startdate col-xs-4 col-md-3">
                            <div class="input-group startdate-error">
                                <?= Html::textInput('start_date[]', '', [
                                    'class' => 'form-control datepicker',
                                    'placeholder' => 'Start Date (yyyy-mm-dd)'
                                ]); ?>
                                <span class="input-group-addon addon-background"><i
                                            class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="enddate col-xs-4 col-md-3">
                            <div class="input-group enddate-error">
                                <?= Html::textInput('end_date[]', '', [
                                    'class' => 'form-control datepicker',
                                    'placeholder' => 'End Date (yyyy-mm-dd)'
                                ]); ?>
                                <span class="input-group-addon addon-background"><i
                                            class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-xs-4 col-md-3" id="removeDate">
                            <a href="javascript:void(0);"
                               class="btn btn-danger btn-sm fa fa-minus removeDateBtn">
                            </a>
                        </div>
                    </div>
                    <br>
                </div>
            <?php } ?>
        </div>
        <div class="break-rule"></div>
        <div class="row">
            <div class="form-group">
                <div class="col-sm-2">
                    <h4><label>Time Slot</label></h4>
                </div>
                <div class="col-sm-2">
                    <a id="addTime" href="javascript:void(0);"
                       class="btn btn-primary btn-sm fa fa-plus mb-2">
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div id="officehour-timeslot-errors" hidden="true"><?= json_encode($timeErrors) ?></div>
        <div class="officehour-timeslot" id="office-hour-time-slot-id">
            <?php
            if (!empty($timeSlotData)) {
                foreach ($timeSlotData as $key => $timeSlot) {
                    ?>
                    <div class="each-timeslot">
                        <div class="row">
                            <div class="starttime col-xs-4 col-md-3">
                                <div class="input-group bootstrap-timepicker starttime-error">
                                    <?= Html::textInput('start_time[]', $timeSlot['start_time'],
                                        ['class' => 'form-control timepicker', 'placeholder' => 'Start Time']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                            <div class="endtime col-xs-4 col-md-3">
                                <div class="input-group bootstrap-timepicker endtime-error">
                                    <?= Html::textInput('end_time[]', $timeSlot['end_time'],
                                        ['class' => 'form-control timepicker', 'placeholder' => 'End Time']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                            <div class="days col-xs-4 col-md-3">
                                <div class="days-error">
                                    <?= Html::dropDownList('run_on_days[' . $key . '][]',
                                        isset($timeSlot['run_on_days']) ? $timeSlot['run_on_days'] : [],
                                        OfficeHourTimeSlot::weekDays(),
                                        ['multiple' => 'multiple', 'class' => 'weekdays', 'placeholder' => 'Days']); ?>
                                </div>
                            </div>

                            <div class="col-xs-4 col-md-3" id="removeTime">
                                <a href="javascript:void(0);"
                                   class="btn btn-danger btn-sm fa fa-minus removeTimeBtn">
                                </a>
                            </div>
                        </div>
                        <br>
                    </div>
                <?php }
            } else {
                ?>
                <div class="each-timeslot">
                    <div class="row">
                        <div class="starttime col-xs-4 col-md-3">
                            <div class="input-group bootstrap-timepicker starttime-error">
                                <?= Html::textInput('start_time[]', '',
                                    ['class' => 'form-control timepicker', 'placeholder' => 'Start Time']); ?>
                                <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="endtime col-xs-4 col-md-3">
                            <div class="input-group bootstrap-timepicker endtime-error">
                                <?= Html::textInput('end_time[]', '',
                                    ['class' => 'form-control timepicker', 'placeholder' => 'End Time']); ?>
                                <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                        <div class="days col-xs-4 col-md-3">
                            <div class="days-error">
                                <?= Html::dropDownList('run_on_days[1][]', '', OfficeHourTimeSlot::weekDays(),
                                    ['multiple' => 'multiple', 'class' => 'weekdays', 'placeholder' => 'Days']); ?>
                            </div>
                        </div>

                        <div class="col-xs-4 col-md-3" id="removeTime">
                            <a href="javascript:void(0);"
                               class="btn btn-danger btn-sm fa fa-minus removeTimeBtn">
                            </a>
                        </div>
                    </div>
                    <br>
                </div>
            <?php } ?>
        </div>
        <div class="break-rule"></div>
        <div class="row">
            <div class="form-group">
                <div class="col-sm-2">
                    <h4><label>Holiday</label></h4>
                </div>
                <div class="col-sm-2">
                    <a id="addDateTime" href="javascript:void(0);"
                       class="btn btn-primary btn-sm fa fa-plus mb-2">
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div id="holiday-date-time-errors" hidden="true"><?= json_encode($holidayErrors) ?></div>
        <div class="holiday-datetime">
            <?php
            foreach ($holidayData as $key => $holiday) {
                ?>
                <div class="each-holiday-datetime">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="holidayStartDate col-xs-5 col-md-5">
                                <div class="input-group holidayStartDate-error">
                                    <?= Html::textInput('holidayStartDate[]', $holiday['holidayStartDate'],
                                        ['class' => 'form-control holidayDpicker', 'placeholder' => 'Start Date']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="holidayStartTime col-xs-5 col-md-5">
                                <div class="input-group bootstrap-timepicker holidayStartTime-error">
                                    <?= Html::textInput('holidayStartTime[]', $holiday['holidayStartTime'],
                                        ['class' => 'form-control holidayTpicker', 'placeholder' => 'Start Time']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="holidayEndDate col-xs-5 col-md-5">
                                <div class="input-group holidayEndDate-error">
                                    <?= Html::textInput('holidayEndDate[]', $holiday['holidayEndDate'],
                                        ['class' => 'form-control holidayDpicker', 'placeholder' => 'End Date']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="holidayEndTime col-xs-5 col-md-5">
                                <div class="input-group bootstrap-timepicker holidayEndTime-error">
                                    <?= Html::textInput('holidayEndTime[]', $holiday['holidayEndTime'],
                                        ['class' => 'form-control holidayTpicker', 'placeholder' => 'End Time']); ?>
                                    <span class="input-group-addon addon-background"><i
                                                class="fa fa-clock-o"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1 col-md-1" id="removeDateTime">
                            <a href="javascript:void(0);"
                               class="btn btn-danger btn-sm fa fa-minus removeDateTimeBtn">
                            </a>
                        </div>
                    </div>
                    <br>
                </div>
            <?php } ?>
        </div>
        <div class="break-rule"></div>
        <div class="row">
            <div class="form-group text-center">
                <?php if ($model->isNewRecord) {
                    echo Html::submitButton(Yii::t('app', 'Save'),
                        ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
                } else {
                    echo Html::submitButton(Yii::t('app', 'Update'),
                        ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                    echo Html::submitButton(Yii::t('app', 'Apply'),
                        ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
                }
                echo Html::a(Yii::t('app', 'Back'),
                    ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                    ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="hidden-dateslot">
        <div class="each-dateslot">
            <div class="row">
                <div class="startdate col-xs-4 col-md-3">
                    <div class="input-group startdate-error">
                        <?= Html::textInput('start_date[]', '',
                            ['class' => 'form-control datepicker', 'placeholder' => 'Start Date (yyyy-mm-dd)']); ?>
                        <span class="input-group-addon addon-background"><i
                                    class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <div class="enddate col-xs-4 col-md-3">
                    <div class="input-group enddate-error">
                        <?= Html::textInput('end_date[]', '',
                            ['class' => 'form-control datepicker', 'placeholder' => 'End Date (yyyy-mm-dd)']); ?>
                        <span class="input-group-addon addon-background"><i
                                    class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>

                <div class="col-xs-4 col-md-3" id="removeDate">
                    <a href="javascript:void(0);"
                       class="btn btn-danger btn-sm fa fa-minus removeDateBtn">
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="hidden-timeslot">
        <div class="each-timeslot">
            <div class="row">
                <div class="starttime col-xs-4 col-md-3">
                    <div class="input-group bootstrap-timepicker starttime-error">
                        <?= Html::textInput('start_time[]', '',
                            ['class' => 'form-control timepicker', 'placeholder' => 'Start Time']); ?>
                        <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                    </div>
                </div>
                <div class="endtime col-xs-4 col-md-3">
                    <div class="input-group bootstrap-timepicker endtime-error">
                        <?= Html::textInput('end_time[]', '',
                            ['class' => 'form-control timepicker', 'placeholder' => 'End Time']); ?>
                        <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                    </div>
                </div>
                <div class="days col-xs-4 col-md-3">
                    <div class="days-error">
                        <?= Html::dropDownList('run_on_days[][]', '', OfficeHourTimeSlot::weekDays(),
                            ['multiple' => 'multiple', 'class' => 'weekdays', 'placeholder' => 'Days']); ?>
                    </div>
                </div>

                <div class="col-xs-4 col-md-3" id="removeTime">
                    <a href="javascript:void(0);"
                       class="btn btn-danger btn-sm fa fa-minus removeTimeBtn">
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="hidden-datetime">
        <div class="each-holiday-datetime">
            <div class="row">
                <div class="col-md-5">
                    <div class="holidayStartDate col-xs-5 col-md-5">
                        <div class="input-group holidayStartDate-error">
                            <?= Html::textInput('holidayStartDate[]', '',
                                ['class' => 'form-control holidayDpicker', 'placeholder' => 'Start Date']); ?>
                            <span class="input-group-addon addon-background"><i
                                        class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="holidayStartTime col-xs-5 col-md-5">
                        <div class="input-group bootstrap-timepicker holidayStartTime-error">
                            <?= Html::textInput('holidayStartTime[]', '',
                                ['class' => 'form-control holidayTpicker', 'placeholder' => 'Start Time']); ?>
                            <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="holidayEndDate col-xs-5 col-md-5">
                        <div class="input-group holidayEndDate-error">
                            <?= Html::textInput('holidayEndDate[]', '',
                                ['class' => 'form-control holidayDpicker', 'placeholder' => 'End Date']); ?>
                            <span class="input-group-addon addon-background"><i
                                        class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="holidayEndTime col-xs-5 col-md-5">
                        <div class="input-group bootstrap-timepicker holidayEndTime-error">
                            <?= Html::textInput('holidayEndTime[]', '',
                                ['class' => 'form-control holidayTpicker', 'placeholder' => 'End Time']); ?>
                            <span class="input-group-addon addon-background"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1 col-md-1" id="removeDateTime">
                    <a href="javascript:void(0);"
                       class="btn btn-danger btn-sm fa fa-minus removeDateTimeBtn">
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>

<?php

$this->registerCssFile("@web/library/timepicker/bootstrap-timepicker.min.css", [
    'depends' => [\backend\assets\WcAppAsset::className()],
]);

$this->registerCssFile("@web/library/timepicker/bootstrap-datepicker.min.css", [
    'depends' => [\backend\assets\WcAppAsset::className()],
]);

$this->registerJsFile('@web/library/timepicker/bootstrap-timepicker.min.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/library/timepicker/bootstrap-datepicker.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/js/modules/dateslot-elements.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>