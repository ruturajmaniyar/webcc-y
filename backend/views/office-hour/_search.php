<?php

use backend\models\Timezone;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OfficeHourSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-group office-hour-search-class" id="office-hour-search-id">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#office-hour-search-id"
                   href="#office-hour-search-id-collapse"
                   aria-expanded="false" class="collapsed">
                    <?= Yii::t('app', 'Search') ?>
                </a>
            </h4>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'office-hour-search-form',
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>
        <div class="panel-collapse collapse" id="office-hour-search-id-collapse" aria-expanded="false">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>
                        <div class="form-group">
                            <?= $form->field($model, 'is_active',
                                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                                ['class' => 'select2 select2-offscreen', 'prompt' => 'All']) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'timezone_id',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Timezone::getTimezoneList(),
                            ['class' => 'select2 select2-offscreen', 'prompt' => 'All']) ?>
                    </div>
                </div>

                <div class="break-rule"></div>
                <div class="row">
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'Search'),
                            ['class' => 'btn btn-form btn-primary btn-rounded']) ?>
                        <?= Html::a(Yii::t('app', 'Reset'), ['index', 'page' => Yii::$app->session->get('page')],
                            ['class' => 'btn btn-form btn-default btn-rounded']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
