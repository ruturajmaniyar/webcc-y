<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OfficeHour */
/* @var $dateErrors array */
/* @var $timeErrors array */
/* @var $holidayErrors array */
/* @var $dateSlotData array */
/* @var $timeSlotData array */
/* @var $holidayData array */

$this->title = 'Update Office Hour';
$this->params['breadcrumbs'][] = ['label' => 'Office Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="office-hour-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dateSlotData' => $dateSlotData,
        'timeSlotData' => $timeSlotData,
        'holidayData' => $holidayData,
        'dateErrors' => $dateErrors,
        'timeErrors' => $timeErrors,
        'holidayErrors' => $holidayErrors,
    ]); ?>

</div>
