<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\OfficeHour */
/* @var $dateErrors array */
/* @var $timeErrors array */
/* @var $holidayErrors array */
/* @var $dateSlotData array */
/* @var $timeSlotData array */
/* @var $holidayData array */

$this->title = 'Create Office Hour';
$this->params['breadcrumbs'][] = ['label' => 'Office Hours', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-hour-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dateSlotData' => $dateSlotData,
        'timeSlotData' => $timeSlotData,
        'holidayData' => $holidayData,
        'dateErrors' => $dateErrors,
        'timeErrors' => $timeErrors,
        'holidayErrors' => $holidayErrors,
    ]); ?>

</div>
