<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyEmail */

$this->title = Yii::t('app', 'Update Email for Voicemail: {nameAttribute}', [
    'nameAttribute' => $model->email,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Email for Voicemails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="company-email-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
