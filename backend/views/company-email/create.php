<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CompanyEmail */

$this->title = Yii::t('app', 'Email for Voicemail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Email for Voicemails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-email-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
