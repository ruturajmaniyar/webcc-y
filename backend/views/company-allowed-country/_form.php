<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyAllowedCountry */
/* @var $countryModel backend\models\Country */
/* @var $countryList array */
/* @var $companyAllowedCountryModel backend\models\CompanyAllowedCountry */
/* @var $companyAllowedCountryList array */
/* @var $form yii\widgets\ActiveForm */

$arrayDiffAccoc = array_diff_assoc($companyAllowedCountryList, $countryList);

$selected = array();
if (!empty($arrayDiffAccoc)) {
    foreach ($arrayDiffAccoc as $value) {
        $selected[$value] = array('selected' => 'selected');
    }
}
?>
    <div class="company-allowed-country-form-class" id="company-allowed-country-form-id">
        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'company-allowed-country-form',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]
        ]); ?>

        <div class="row">
            <div class="form-group">
                <?= $form->field($model, 'country_id',
                    ['options' => ['class' => 'col-xs-12 col-md-6']])->listBox($countryList,
                    [
                        'class' => 'multi-select',
                        'id' => 'company_allowed_country_multi_select3',
                        'multiple' => 'multiple',
                        'options' => $selected
                    ]) ?>
            </div>
        </div>

        <div class="break-rule"></div>
        <div class="row">
            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']) ?>
                <?= Html::a(Yii::t('app', 'Home'),
                    [Url::to([Yii::$app->homeUrl[0]])],
                    ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php
$this->registerCssFile("@web/library/jquery-multi-select/multi-select.css", [
    'depends' => [\backend\assets\WcAppAsset::className()],
]);

$this->registerJsFile('@web/library/jquery-multi-select/jquery.multi-select.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/library/jquery-multi-select/jquery.quicksearch.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/js/modules/company-allowed-country.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>