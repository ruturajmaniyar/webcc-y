<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyAllowedCountry */
/* @var $countryModel backend\models\Country */
/* @var $countryList array */
/* @var $companyAllowedCountryModel backend\models\CompanyAllowedCountry */
/* @var $companyAllowedCountryList array */

$this->title = Yii::t('app', 'Company Allowed Countries');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-allowed-country-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'countryModel' => $countryModel,
        'countryList' => $countryList,
        'companyAllowedCountryModel' => $companyAllowedCountryModel,
        'companyAllowedCountryList' => $companyAllowedCountryList,
    ]) ?>
</div>
