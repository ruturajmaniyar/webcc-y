<?php

use backend\models\Timezone;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="bg-picture" style="background-image:url('images/bg_6.jpg')">
            <span class="bg-picture-overlay"></span><!-- overlay -->
            <!-- meta -->
            <div class="box-layout meta bottom">
                <div class="col-sm-6 clearfix">
                    <span class="img-wrapper pull-left m-r-15"><img src="img/avatar-2.jpg" alt="" style="width:64px"
                                                                    class="br-radius"></span>
                    <div class="media-body">
                        <h3 class="text-white mb-2 m-t-10 ellipsis"><?= $model->company->contact_name ?></h3>
                        <h5 class="text-white"> <?= $model->company->country->name ?></h5>
                    </div>
                </div>
            </div>
            <!--/ meta -->
        </div>
    </div>
</div>

<div class="row m-t-30">
    <div class="col-sm-12">
        <div class="panel panel-default p-0">
            <div class="panel-body p-0">
                <ul class="nav nav-tabs profile-tabs">
                    <li class="active"><a data-toggle="tab" href="#aboutme">About Me</a></li>
                    <li class=""><a data-toggle="tab" href="#session">Session</a></li>
                </ul>

                <div class="tab-content m-0">

                    <div id="aboutme" class="tab-pane active">
                        <div class="profile-desk">
                            <h1><?= Yii::$app->user->identity->name ?></h1>
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th colspan="3"><h3>Profile Information</h3></th>
                                </tr>
                                </thead>
                                <?php $form = ActiveForm::begin([
                                    'options' => [
                                        'class' => 'form-horizontal',
                                        'id' => 'profile    -form',
                                        'role' => 'form'
                                    ]
                                ]); ?>
                                <tbody>
                                <tr>
                                    <td><b>Contact Name</b></td>
                                    <td>
                                        <?= $form->field($model->company, 'contact_name',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                            'class' => 'form-control',
                                            'maxlength' => true
                                        ])->label(false) ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Company</b></td>
                                    <td>
                                        <?= $form->field($model->company, 'company',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                            'class' => 'form-control',
                                            'maxlength' => true,
                                            'readonly' => true
                                        ])->label(false) ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Email Id</b></td>
                                    <td><?= $form->field($model->company, 'email',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                            'class' => 'form-control',
                                            'maxlength' => true,
                                            'readonly' => true
                                        ])->label(false) ?></td>
                                </tr>
                                <tr>
                                    <td><b>Contact Number</b></td>
                                    <td>
                                        <?= $form->field($model->company, 'contact_number',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                            'class' => 'form-control',
                                            'maxlength' => true
                                        ])->label(false) ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Web Site</b></td>
                                    <td>
                                        <?= $form->field($model->company, 'website',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                            'class' => 'form-control',
                                            'maxlength' => true
                                        ])->label(false) ?>

                                    </td>
                                </tr>
                                <tr>
                                    <td><b>TimeZone</b></td>
                                    <td>
                                        <?= $form->field($model->timezone, 'timezone',
                                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(
                                            Timezone::getActiveTimezoneArray(),
                                            [
                                                'class' => 'form-control',
                                                'maxlength' => true
                                            ])->label(false) ?>

                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <?= Html::submitButton(Yii::t('app', 'Update'),
                                            ['class' => 'btn btn-success']) ?>
                                    </td>
                                </tr>
                                </tbody>
                                <?php ActiveForm::end(); ?>
                            </table>
                        </div> <!-- end profile-desk -->
                    </div> <!-- about-me -->


                    <!-- session -->
                    <div id="session" class="tab-pane">
                        <?=
                        GridView::widget([
                            'dataProvider' => $userSessionModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'admin_id',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],
                                [
                                    'attribute' => 'login_time',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],
                                [
                                    'attribute' => 'browser',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],
                                [
                                    'attribute' => 'platform',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],
                                [
                                    'attribute' => 'user_agent',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],
                                [
                                    'attribute' => 'public_ip',
                                    'format' => 'raw',
                                    'enableSorting' => true,
                                ],


                            ],
                        ]);
                        ?>
                    </div>

                    <!-- settings -->
                    <div id="edit-profile" class="tab-pane">
                        <div class="user-profile-content">
                            <form role="form">
                                <div class="form-group">
                                    <label for="FullName">Full Name</label>
                                    <input type="text" value="John Doe" id="FullName" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Email">Email</label>
                                    <input type="email" value="first.last@example.com" id="Email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Username">Username</label>
                                    <input type="text" value="john" id="Username" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="Password">Password</label>
                                    <input type="password" placeholder="6 - 15 Characters" id="Password"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="RePassword">Re-Password</label>
                                    <input type="password" placeholder="6 - 15 Characters" id="RePassword"
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="AboutMe">About Me</label>
                                    <textarea style="height: 125px;" id="AboutMe" class="form-control">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</textarea>
                                </div>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </form>
                        </div>
                    </div>


                    <!-- profile -->
                    <div id="projects" class="tab-pane">
                        <div class="row m-t-10">
                            <div class="col-md-12">
                                <div class="portlet"><!-- /primary heading -->
                                    <div id="portlet2" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Project Name</th>
                                                        <th>Start Date</th>
                                                        <th>Due Date</th>
                                                        <th>Status</th>
                                                        <th>Assign</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Velonic Admin</td>
                                                        <td>01/01/2015</td>
                                                        <td>07/05/2015</td>
                                                        <td><span class="label label-info">Work in Progress</span></td>
                                                        <td>Coderthemes</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Velonic Frontend</td>
                                                        <td>01/01/2015</td>
                                                        <td>07/05/2015</td>
                                                        <td><span class="label label-success">Pending</span></td>
                                                        <td>Coderthemes</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Velonic Admin</td>
                                                        <td>01/01/2015</td>
                                                        <td>07/05/2015</td>
                                                        <td><span class="label label-pink">Done</span></td>
                                                        <td>Coderthemes</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>Velonic Frontend</td>
                                                        <td>01/01/2015</td>
                                                        <td>07/05/2015</td>
                                                        <td><span class="label label-purple">Work in Progress</span>
                                                        </td>
                                                        <td>Coderthemes</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>Velonic Admin</td>
                                                        <td>01/01/2015</td>
                                                        <td>07/05/2015</td>
                                                        <td><span class="label label-warning">Coming soon</span></td>
                                                        <td>Coderthemes</td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- /Portlet -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
