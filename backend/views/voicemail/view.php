<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Voicemail */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Voicemails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>
<div class="voicemail-view-class" id="voicemail-view-id">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id],
            ['class' => 'btn btn-form btn-purple btn-rounded']) ?>
        <?= Html::a(Yii::t('app', 'Listing'), ['index', 'page' => $page],
            ['class' => 'btn btn-form btn-inverse btn-rounded']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => Yii::t('app', 'Voicemail Prompt'),
                'value' => isset($model->voicemailPrompt->name) ? $model->voicemailPrompt->name : Yii::t('app',
                    'Unavailable'),
            ],
            [
                'label' => Yii::t('app', 'Email for Voicemail'),
                'value' => $model->company->email,
            ],
            'created_on',
            [
                'label' => Yii::t('app', 'Status'),
                'value' => ($model->is_active) ? Yii::t('app', 'Active') : Yii::t('app', 'Inactive'),
            ],
        ],
    ]) ?>

</div>
