<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Voicemail */

$this->title = Yii::t('app', 'Voicemail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Voicemails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voicemail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
