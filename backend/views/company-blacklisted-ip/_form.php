<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyCallSetting */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-blacklisted-ip-active-form-class" id="company-blacklisted-ip-active-form-id">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'company-blacklisted-ip-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>
    <div class="row">
        <div class="form-group">
            <div class="col-sm-2">
                <a id="addIp" href="javascript:void(0);"
                   class="btn btn-primary btn-sm fa fa-plus mb-2">
                </a>
            </div>
        </div>
    </div>
    <div id="blacklisted_errors" hidden="true"><?= json_encode($blacklistedIpError) ?></div>
    <div class="blacklisted-ip">
        <?php
        foreach ($allBlacklistedIp as $key => $blacklistedIp) {
            ?>
            <div class="each-blacklisted-ip">
                <div class="row">
                    <div class="ip col-xs-4 col-md-3">
                        <?= Html::textInput('ip[]', $blacklistedIp['ip'],
                            ['class' => 'form-control', 'placeholder' => 'IP']); ?>
                    </div>
                    <div class="netmask col-xs-4 col-md-3">
                        <?= Html::textInput('netmask[]', $blacklistedIp['netmask'],
                            ['class' => 'form-control', 'placeholder' => 'Netmask']); ?>
                    </div>
                    <div class="duration col-xs-4 col-md-3">
                        <?= Html::textInput('duration[]', $blacklistedIp['duration'],
                            ['class' => 'form-control', 'placeholder' => 'Duration']); ?>
                    </div>

                    <div class="col-xs-4 col-md-3" id="removeIp">
                        <a href="javascript:void(0);"
                           class="btn btn-danger btn-sm fa fa-minus removeIpBtn">
                        </a>
                    </div>
                </div>
                <br>
            </div>
        <?php } ?>
    </div>
    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('app', 'Apply'),
                ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']); ?>
            <?= Html::a(Yii::t('app', 'Home'),
                [Url::to([Yii::$app->homeUrl[0]])],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div class="hidden-ip">
    <div class="each-blacklisted-ip">
        <div class="row">
            <div class="ip col-xs-4 col-md-3">
                <?= Html::textInput('ip[]', '', ['class' => 'form-control', 'placeholder' => 'IP']); ?>
            </div>
            <div class="netmask col-xs-4 col-md-3">
                <?= Html::textInput('netmask[]', '', ['class' => 'form-control', 'placeholder' => 'Netmask']); ?>
            </div>
            <div class="duration col-xs-4 col-md-3">
                <?= Html::textInput('duration[]', '', ['class' => 'form-control', 'placeholder' => 'Duration']); ?>
            </div>
            <div class="col-xs-4 col-md-3" id="removeIp">
                <a href="javascript:void(0);"
                   class="btn btn-danger btn-sm fa fa-minus removeIpBtn">
                </a>
            </div>
        </div>
        <br>
    </div>
</div>
<?php
$this->registerJsFile('@web/js/modules/blacklisted-ip-elements.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>
