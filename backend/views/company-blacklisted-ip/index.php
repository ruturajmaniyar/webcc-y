<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\CompanyAllowedCountry */

$this->title = Yii::t('app', 'Blacklisted IP');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-black-listed-ip-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
    	'allBlacklistedIp' => $allBlacklistedIp,
        'blacklistedIpError' => $blacklistedIpError,
    ]) ?>
</div>
