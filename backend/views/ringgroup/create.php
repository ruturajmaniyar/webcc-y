<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ringgroup */
/* @var $sipEndpointList backend\models\SipEndpoint*/
/* @var $ringMembers array*/
/* @var $ringMembersErrors array*/

$this->title = Yii::t('app', 'Create Ringgroup');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ringgroups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ringgroup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sipEndpointList' => $sipEndpointList,
        'ringMembers' => $ringMembers,
        'ringMembersErrors' => $ringMembersErrors,
    ]) ?>

</div>
