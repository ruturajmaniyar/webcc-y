<?php

use backend\models\Voicemail;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Ringgroup */
/* @var $form yii\widgets\ActiveForm */
/* @var $sipEndpointList backend\models\SipEndpoint */
/* @var $ringMembers array */
/* @var $ringMembersErrors array */

isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

<div class="ringgroup-form-class" id="ringgroup-form-id">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'ring-group-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control',
                'maxlength' => true,
            ]) ?>
            <?= $form->field($model, 'is_active',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                ['class' => 'select2 select2-offscreen']) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'no_answer_action_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['noActionId'],
                ['class' => 'select2 select2-offscreen']) ?>

            <?= $form->field($model, 'no_answer_destination_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Voicemail::getVoicemailList(), [
                'prompt' => 'Choose a Voicemail',
                'class' => 'select2 select2-offscreen',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'call_recording',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['boolean'],
                ['class' => 'select2 select2-offscreen']) ?>

            <?= $form->field($model, 'call_type',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['ringGroupCallType'],
                [
                    'prompt' => 'Choose a Call-Type',
                    'class' => 'select2 select2-offscreen',
                ]) ?>
        </div>
    </div>
    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group">
            <div class="col-sm-2">
                <h4><label>Add Members</label></h4>
            </div>
            <div class="col-sm-2">
                <a id="addMember" href="javascript:void(0);"
                   class="btn btn-primary btn-sm fa fa-plus mb-2">
                </a>
            </div>
        </div>
    </div>
    <div id="ringgroup-member-errors" hidden="true"><?= json_encode($ringMembersErrors) ?></div>
    <div class="ringgroup-member">
        <div id="listWithHandle" class="list-group">
            <?php
            foreach ($ringMembers as $key => $ring) {
                ?>
                <div class="each-ringgroup-member list-group-item">
                    <div class="row">
                        <div class="col-sm-1"><i class="glyphicon glyphicon-move"></i></div>
                        <div class="sip_endpoint_id col-xs-4 col-md-3">
                            <?= Html::dropDownList('sip_endpoint_id[]', $ring['sip_endpoint_id'], $sipEndpointList, [
                                'class' => 'form-control',
                                'prompt' => 'Select',
                            ]); ?>
                        </div>
                        <div class="ring_timeout col-xs-4 col-md-3">
                            <?= Html::textInput('ring_timeout[]', $ring['ring_timeout'], [
                                'class' => 'form-control',
                                'placeholder' => 'Ring Timeout',
                            ]); ?>
                        </div>
                        <div class="is_active col-xs-4 col-md-2">
                            <?= Html::dropDownList('is_active[]', $ring['is_active'], [1 => 'Active', 0 => 'Inactive'],
                                [
                                    'class' => 'form-control',
                                    'placeholder' => 'Status',
                                ]); ?>
                        </div>

                        <div class="col-xs-4 col-md-2" id="removeMember">
                            <a href="javascript:void(0);"
                               class="btn btn-danger btn-sm fa fa-minus removeMemberBtn">
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?php if ($model->isNewRecord) {
                echo Html::submitButton(Yii::t('app', 'Save'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
            } else {
                echo Html::submitButton(Yii::t('app', 'Update'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                echo Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
            }
            echo Html::a(Yii::t('app', 'Back'),
                ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<div class="hidden-ringgroup-member">
    <div class="each-ringgroup-member list-group-item">
        <div class="row">
            <div class="col-sm-1"><i class="glyphicon glyphicon-move"></i></div>
            <div class="sip_endpoint_id col-xs-4 col-md-3">
                <?= Html::dropDownList('sip_endpoint_id[]', '', $sipEndpointList, [
                    'class' => 'form-control',
                    'prompt' => 'Select',
                ]); ?>
            </div>
            <div class="ring_timeout col-xs-4 col-md-3">
                <?= Html::textInput('ring_timeout[]', '', [
                    'class' => 'form-control',
                    'placeholder' => 'Ring Timeout',
                ]); ?>
            </div>
            <div class="is_active col-xs-4 col-md-2">
                <?= Html::dropDownList('is_active[]', '', [1 => 'Active', 0 => 'Inactive'], [
                    'class' => 'form-control',
                    'placeholder' => 'Status',
                ]); ?>
            </div>

            <div class="col-xs-4 col-md-2" id="removeMember">
                <a href="javascript:void(0);"
                   class="btn btn-danger btn-sm fa fa-minus removeMemberBtn">
                </a>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJsFile('@web/js/modules/Sortable.min.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/js/modules/ring-group-no-answer-action.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
$this->registerJsFile('@web/js/modules/ringgroup-elements.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>
