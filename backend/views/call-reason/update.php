<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CallReason */

$this->title = Yii::t('app', 'Update Call Reason: {nameAttribute}', [
    'nameAttribute' => $model->reason,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Reasons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reason, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="call-reason-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
