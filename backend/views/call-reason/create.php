<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CallReason */

$this->title = Yii::t('app', 'Create Call Reason');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Reasons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-reason-create-class" id="call-reason-create-id">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
