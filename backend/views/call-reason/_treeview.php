<?php
/**
 * Created by PhpStorm.
 * User: akshay
 * Date: 2/5/18
 * Time: 9:44 PM
 */

use backend\models\CallReason;

/* @var $allReasonData array */

?>
<?php
function nestedTreeDisplay($jsTreeData)
{
    foreach ($jsTreeData as $key => $value) {
        if (!is_array($value)) {
            $id = CallReason::fetchIdbyName($value);
            ?>
            <li data-jstree='{"opened":true,"icon":"ion-chevron-right"}'>
            <a href="<?= Yii::$app->urlManager->createUrl(['/call-reason/configuration?id=' . $id]); ?>"><b>  <?php echo $value; ?></b></a>

        <?php } else {
            echo "<ul>";
            nestedTreeDisplay($value);
            echo "</ul>";
            ?>
        <?php } ?>
    <?php }
}
?>
    <div id="call-reason-tree">
        <ul>
            <?php nestedTreeDisplay($allReasonData); ?>
            </li>
        </ul>
    </div>
<?php

$this->registerCssFile('@web/css/jstree.css', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJsFile('@web/js/treeview.min.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

$this->registerJs('$(document).ready(function () {
            $("#call-reason-tree").jstree();
             $("#call-reason-tree").on("changed.jstree", function (e, data) {
            window.location.href = data.node.a_attr.href;
        });
});');
?>