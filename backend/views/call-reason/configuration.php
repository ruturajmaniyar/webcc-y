<?php
/**
 * Created by PhpStorm.
 * User: akshay
 * Date: 20/4/18
 * Time: 12:25 PM
 */


use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CallReason */
/* @var $callReasonMembers array */
/* @var $callReasonErrors array */
/* @var $callSubReasons array */
/* @var $allReasonData array */
/* @var $callReasonCount integer */


$this->title = Yii::t('app', 'Configuration Call Reason: {nameAttribute}', [
    'nameAttribute' => $model->reason,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Call Reasons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reason];
$this->params['breadcrumbs'][] = Yii::t('app', 'Configuration');
?>
<div class="call-reason-update-class" id="call-reason-update-id">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_configuration', [
        'model' => $model,
        'callReasonCount' => $callReasonCount,
        'allReasonData' => $allReasonData,
        'callReasonMembers' => $callReasonMembers,
        'callReasonErrors' => $callReasonErrors,
        'callActionMembers' => $callActionMembers,
    ]) ?>

</div>
