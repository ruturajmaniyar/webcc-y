<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CallReasonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-group call-reason-search-class" id="call-reason-search-id">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#call-reason-search-id"
                   href="#call-reason-search-id-collapse"
                   aria-expanded="false" class="collapsed">
                    <?= Yii::t('app', 'Search') ?>
                </a>
            </h4>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'call-reason-search-form',
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>
        <div class="panel-collapse collapse" id="call-reason-search-id-collapse" aria-expanded="false">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'reason',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>

                        <?= $form->field($model, 'is_active',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                            ['class' => 'select2', 'prompt' => 'All']) ?>
                    </div>
                </div>
                <div class="break-rule"></div>
                <div class="row">
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'Search'),
                            ['class' => 'btn btn-form btn-primary btn-rounded']) ?>
                        <?= Html::a(Yii::t('app', 'Reset'), ['index', 'page' => Yii::$app->session->get('page')],
                            ['class' => 'btn btn-form btn-default btn-rounded']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
