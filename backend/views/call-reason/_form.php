<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CallReason */
/* @var $form yii\widgets\ActiveForm */
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

<div class="call-reason-form-class" id="call-reason-form-id">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'call-reason-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>
    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'reason', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control',
                'maxlength' => true,
                'placeholder' => 'Reason',
            ]) ?>
            <?= $form->field($model, 'description', ['options' => ['class' => 'col-xs-12 col-md-6']])->textarea([
                'class' => 'form-control',
                'maxlength' => true,
                'rows' => 2,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php
            if (!$model->isNewRecord) {
                echo $form->field($model, 'is_active', ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList([
                    1 => 'Active',
                    0 => 'Inactive'
                ], [
                    'class' => 'form-control',
                ]);
            }
            ?>
        </div>
    </div>
    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?php if ($model->isNewRecord) {
                echo Html::submitButton(Yii::t('app', 'Save'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
            } else {
                echo Html::submitButton(Yii::t('app', 'Update'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                echo Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
            }
            echo Html::a(Yii::t('app', 'Back'),
                ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
