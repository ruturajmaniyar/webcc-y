<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CallReasonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Call Reasons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-reason-index-class" id="call-reason-index-id">
    <?= Html::a(Yii::t('app', 'Add New'), ['create'], ['class' => 'btn btn-success pull-right btn-rounded']); ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'pjax-call-reason-index']); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card card-default">
                <div class="card-header">
                    <?= GridView::widget([
                        'id' => 'grid-call-reason-index',
                        'dataProvider' => $dataProvider,
                        'layout' => Yii::$app->helper->get_layout_str('#call-reason-search-form'),
                        'showOnEmpty' => false,
                        'options' => [
                            'class' => 'grid-view-color text-center',
                        ],
                        'columns' => [
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header' => Yii::t('app', 'Action'),
                                'template' => '{update} {configuration}',
                                'headerOptions' => ['class' => 'text-center'],
                                'contentOptions' => [
                                    'class' => 'text-center inline-class action_space',
                                    'style' => 'width: 10%;'
                                ],
                                'buttons' => [
                                    'update' => function ($url) {
                                        isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
                                        $url = $url . "&page=" . $page;

                                        return (Html::a('<i class="fa fa-pencil"></i>', $url, [
                                            'data-toggle' => 'popover',
                                            'data-placement' => 'top',
                                            'data-trigger' => "hover",
                                            'data-content' => '',
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-icon btn-purple btn-rounded',
                                        ]));
                                    },
                                    'configuration' => function ($url) {
                                        isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
                                        $url = $url . "&page=" . $page;

                                        return (Html::a('<i class="fa fa-cog"></i>', $url, [
                                            'data-toggle' => 'popover',
                                            'data-placement' => 'top',
                                            'data-trigger' => "hover",
                                            'data-content' => '',
                                            'data-pjax' => 0,
                                            'class' => 'btn btn-icon btn-warning btn-rounded',
                                        ]));
                                    },
                                ],
                            ],
                            [
                                'attribute' => 'is_active',
                                'header' => Yii::t('app', 'Status'),
                                'headerOptions' => ['class' => 'text-center', 'style' => 'width:7%'],
                                'contentOptions' => ['class' => 'text-center'],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->is_active) {
                                        return '<span class="label label-success text-center">' . Yii::t('app',
                                                'Active') . '</span>';
                                    } else {
                                        return '<span class="label label-danger text-center">' . Yii::t('app',
                                                'Inactive') . '</span>';
                                    }
                                },
                                'enableSorting' => true,
                            ],
                            'reason',
                            'description',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
