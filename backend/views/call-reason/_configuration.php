<?php

use backend\models\CallAction;
use backend\models\OfficeHour;
use backend\models\Ringgroup;
use backend\models\SipEndpoint;
use backend\models\SipTrunk;
use backend\models\Voicemail;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CallReason */
/* @var $form yii\widgets\ActiveForm */
/* @var $callReasonMembers array */
/* @var $callReasonErrors array */
/* @var $callSubReasons array */
/* @var $allReasonData array */
/* @var $callReasonCount integer */
?>

<div class="row">
    <div class="col-xl-2 col-md-4 col-xs-12">
        <div class="form-group">
            <?php if (isset($model->parent_id)) { ?>
                <a class="btn btn-sm btn-warning"
                   href="<?php echo Url::to(['/call-reason/configuration/?id=' . $model->parent_id]) ?>">GO TO
                    Parent</a>
            <?php } ?>
        </div>
        <div class="tile-stats white-bg">
            <div id="call-reason-tree-view" class="col-xl-2 col-md-4 col-xs-12 contacts">
                <?php echo $this->render('_treeview', ['allReasonData' => $allReasonData]); ?>
            </div>
        </div>
    </div>
    <div class="col-xl-10 col-md-8 col-xs-12">
        <div class="call-reason-configuration-form">

            <?php $form = ActiveForm::begin([
                'id' => 'call-reason-configuration-active-form',
            ]); ?>
            <div class="row">
                <div class="form-group">
                    <?= $form->field($model, 'reason',
                        ['options' => ['class' => 'col-xs-12 col-md-8']])->textInput([
                        'class' => 'form-control',
                        'maxlength' => true,
                        'placeholder' => 'Reason',
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <a id="addSubRason" href="javascript:void(0);"
                       class="btn btn-primary btn-sm m-t-15">Sub Reason
                    </a>
                </div>
                <div class="col-sm-2">
                    <a id="addCallAction" href="javascript:void(0);"
                       class="btn btn-primary btn-sm m-t-15">Call Action
                    </a>
                </div>
                <div id="call-reason-count" hidden><?php echo $callReasonCount ?></div>
            </div>
            <div id="call-reason-errors" hidden><?php echo json_encode($callReasonErrors); ?></div>
            <div class="row">
                <div class="form-group append-element">
                    <?php
                    foreach ($callReasonMembers as $key => $member) {
                        ?>
                        <div class="each-sub-reason-block">
                            <div class="reason col-sm-8">
                                <?php echo Html::textInput('reason[]', $member['reason'],
                                    ['class' => 'form-control', 'placeholder' => 'Sub Reason']) ?>
                            </div>
                            <div class="col-sm-2" id="removeMember">
                                <a href="javascript:void(0);"
                                   class="btn btn-danger btn-sm fa fa-times removeSubReason">
                                </a>
                            </div>
                            <br><br>
                        </div>
                    <?php }

                    if (isset($callActionMembers) && !empty($callActionMembers)) {
                        ?>
                        <div class="each-call-action-block" style="display: inline-flex;">
                            <div class="call-reason-border">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Office Hour</label>
                                        <?php
                                        $officeHourList = ArrayHelper::map(OfficeHour::getOfficeHourList(), 'id',
                                            'name');
                                        echo Html::dropDownList('office_hour_id',
                                            $callActionMembers['office_hour_id'],
                                            $officeHourList,
                                            ['prompt' => 'Select', 'class' => 'form-control']) ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Action</label>
                                        <?php
                                        $callActions = ArrayHelper::map(CallAction::getCallAction(), 'id',
                                            'action');
                                        $callDestination = [];
                                        $nonCallDestination = [];
                                        switch ($callActionMembers['office_hours_action_id']) {
                                            case 2:
                                                $callDestination = ArrayHelper::map(SipEndpoint::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'given_name');
                                                break;
                                            case 3:
                                                $callDestination = ArrayHelper::map(SipTrunk::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 4:
                                                $callDestination = ArrayHelper::map(Ringgroup::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 5:
                                                $callDestination = ArrayHelper::map(Voicemail::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 6:
                                                $callDestination = [];
                                                break;
                                        }
                                        switch ($callActionMembers['non_office_hours_action_id']) {
                                            case 2:
                                                $nonCallDestination = ArrayHelper::map(SipEndpoint::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'given_name');
                                                break;
                                            case 3:
                                                $nonCallDestination = ArrayHelper::map(SipTrunk::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 4:
                                                $nonCallDestination = ArrayHelper::map(Ringgroup::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 5:
                                                $nonCallDestination = ArrayHelper::map(Voicemail::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                                                    'id', 'name');
                                                break;
                                            case 6:
                                                $nonCallDestination = [];
                                                break;
                                        }
                                        echo Html::dropDownList('office_hours_action_id',
                                            $callActionMembers['office_hours_action_id'], $callActions,
                                            [
                                                'prompt' => 'Select',
                                                'class' => 'form-control',
                                                'id' => 'call-action-value'
                                            ]) ?>
                                    </div>
                                    <div class="col-sm-4" id="call-destination">
                                        <label>Destination</label>
                                        <?php
                                        echo Html::dropDownList('office_hours_destination_id',
                                            $callActionMembers['office_hours_destination_id'], $callDestination,
                                            [
                                                'prompt' => 'Select',
                                                'class' => 'form-control',
                                                'id' => 'call-destination-value'
                                            ]) ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label>Non-Office Hour</label>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label>Action</label>
                                            <?php
                                            echo Html::dropDownList('non_office_hours_action_id',
                                                $callActionMembers['non_office_hours_action_id'], $callActions,
                                                [
                                                    'prompt' => 'Select',
                                                    'class' => 'form-control',
                                                    'id' => 'non-call-action-value'
                                                ]) ?>
                                        </div>
                                        <div class="col-sm-4" id="non-call-destination">
                                            <label>Destination</label>
                                            <?php
                                            echo Html::dropDownList('non_office_hours_destination_id',
                                                $callActionMembers['non_office_hours_destination_id'],
                                                $nonCallDestination,
                                                [
                                                    'prompt' => 'Select',
                                                    'class' => 'form-control',
                                                    'id' => 'non-call-destination-value'
                                                ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2" id="removeMember">
                                <a href="javascript:void(0);"
                                   class="btn btn-danger btn-sm fa fa-times removeActionMember">
                                </a>
                            </div>
                            <br><br>
                        </div>
                    <?php }
                    ?>

                </div>
            </div>

            <div class="break-rule"></div>
            <div class="row">
                <div class="form-group text-center">
                    <?php if ($model->isNewRecord) {
                        echo Html::submitButton(Yii::t('app', 'Save'),
                            ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
                    } else {
                        echo Html::submitButton(Yii::t('app', 'Update'),
                            [
                                'class' => 'btn btn-form btn-success btn-rounded',
                                'name' => 'update',
                                'value' => 'update'
                            ]);
                        echo Html::submitButton(Yii::t('app', 'Apply'),
                            ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
                    }
                    echo Html::a(Yii::t('app', 'Cancel'),
                        ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                        ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
                    ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJsFile('@web/js/modules/call-reason-elements.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);

?>
<div id="sub-reason-block">
    <div class="each-sub-reason-block">
        <div class="reason col-sm-8">
            <?php echo Html::textInput('reason[]', '',
                ['class' => 'form-control', 'placeholder' => 'Sub Reason']) ?>
        </div>
        <div class="col-sm-2" id="removeMember">
            <a href="javascript:void(0);"
               class="btn btn-danger btn-sm fa fa-times removeSubReason">
            </a>
        </div>
        <br><br>
    </div>
</div>
<div id="call-action-block">
    <div class="each-call-action-block" style="display: inline-flex;">
        <div class="call-reason-border">
            <div class="row">
                <div class="col-sm-4">
                    <label>Office Hour</label>
                    <?php
                    $officeHourList = ArrayHelper::map(OfficeHour::getOfficeHourList(), 'id', 'name');
                    echo Html::dropDownList('office_hour_id', '', $officeHourList,
                        ['prompt' => 'Select', 'class' => 'form-control']) ?>
                </div>
                <div class="col-sm-4">
                    <label>Action</label>
                    <?php
                    $callActions = ArrayHelper::map(CallAction::getCallAction(), 'id', 'action');
                    echo Html::dropDownList('office_hours_action_id', '', $callActions,
                        ['prompt' => 'Select', 'class' => 'form-control', 'id' => 'call-action-value']) ?>
                </div>
                <div class="col-sm-4" id="call-destination">
                    <label>Destination</label>
                    <?php
                    echo Html::dropDownList('office_hours_destination_id', '', [],
                        [
                            'prompt' => 'Select',
                            'class' => 'form-control',
                            'id' => 'call-destination-value'
                        ]) ?>
                </div>
            </div>
            <br>
            <div class="row">
                <label>Non-Office Hour</label>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label>Action</label>
                        <?php
                        echo Html::dropDownList('non_office_hours_action_id', '', $callActions,
                            [
                                'prompt' => 'Select',
                                'class' => 'form-control',
                                'id' => 'non-call-action-value'
                            ]) ?>
                    </div>
                    <div class="col-sm-4" id="non-call-destination">
                        <label>Destination</label>
                        <?php
                        echo Html::dropDownList('non_office_hours_destination_id', '', [],
                            [
                                'prompt' => 'Select',
                                'class' => 'form-control',
                                'id' => 'non-call-destination-value'
                            ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-md-2" id="removeMember">
            <a href="javascript:void(0);"
               class="btn btn-danger btn-sm fa fa-times removeActionMember">
            </a>
        </div>
        <br><br>
    </div>
</div>
