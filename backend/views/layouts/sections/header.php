<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 30/12/17
 * Time: 7:53 PM
 */
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<header class="top-head container-fluid">
    <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <!-- Search -->
    <form role="search" class="navbar-left app-search pull-left hidden-xs">
        <input type="text" placeholder="Search..." class="form-control">
    </form>

    <!-- Left navbar -->
    <!--<nav class=" navbar-default hidden-xs" role="navigation">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">English <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="#">German</a></li>
                    <li><a href="#">French</a></li>
                    <li><a href="#">Italian</a></li>
                    <li><a href="#">Spanish</a></li>
                </ul>
            </li>
        </ul>
    </nav>-->

    <!-- Right navbar -->
    <ul class="list-inline navbar-right top-menu top-right-menu">
        <!-- user login dropdown start-->
        <li class="dropdown text-center">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="img/avatar-2.jpg" class="img-circle profile-img thumb-sm">
                <span class="username"><?= Yii::$app->user->identity->login_name ?></span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003"
                style="overflow: hidden; outline: none;">
                <li><a href="<?= Url::to(['admin/profile']) ?>"><i class="fa fa-briefcase"></i>Profile</a></li>

                <li><a href="#" onclick="document.getElementById('logout_form').submit();"><i class="fa fa-sign-out"></i>Logout</a></li>
                <?php
                    echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logout_form']);
                    echo Html::endForm();
                ?>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!-- End right navbar -->

</header>
