<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 30/12/17
 * Time: 7:53 PM
 */

use yii\helpers\Url;

?>
<!-- Aside Start-->
<aside class="left-panel">

    <!-- brand -->
    <div class="logo">
        <a href="<?=\yii\helpers\Url::to(['/admin/dashboard']) ?>" class="logo-expanded">
            <img src="img/single-logo.png" alt="logo">
            <span class="nav-label"><?= Yii::t('app', 'Web-CC') ?></span>
        </a>
    </div>
    <!-- / brand -->

    <!-- Navbar Start -->
    <nav class="navigation">
        <ul class="list-unstyled">
            <li><a href="<?= Url::to([Yii::$app->homeUrl[0]]); ?>"><i class="ion-home"></i> <span
                            class="nav-label"><?= Yii::t('app', 'Home') ?></span></a></li>
            <li class="has-submenu"><a href="#"><i class="ion-ios7-recording"></i> <span class="nav-label"><?= Yii::t('app', 'Manage Voicemail') ?></span></a>
                <ul class="list-unstyled">
                    <li><a href="<?= Url::to(['company-email/']); ?>"><?= Yii::t('app', 'Email for Voicemail') ?></a>
                    </li>
                    <li><a href="<?= Url::to(['voicemail/']);?>"><?= Yii::t('app', 'Voicemail Box') ?></a></li>
                </ul>
            </li>
            <li class="has-submenu"><a href="#"><i class="ion-android-keypad"></i> <span class="nav-label"><?= Yii::t('app', 'Manage SIP') ?></span></a>
                <ul class="list-unstyled">
                    <li><a href="<?= Url::to(['sip-endpoint/']);?>"><?= Yii::t('app', 'SIP Endpoint') ?></a></li>
                    <li><a href="<?= Url::to(['sip-trunk/']);?>"><?= Yii::t('app', 'SIP Trunk') ?></a></li>
                </ul>
            </li>
            <li class="has-submenu"><a href="#"><i class="ion-settings"></i> <span class="nav-label"><?= Yii::t('app', 'Manage Configuration') ?></span></a>
                <ul class="list-unstyled">
                    <li><a href="<?= Url::to(['company-allowed-country/']);?>"><?= Yii::t('app', 'Allow Country') ?></a></li>
                    <li><a href="<?= Url::to(['company-call-setting/']);?>"><?= Yii::t('app', 'Call Limit') ?></a></li>
                    <li><a href="<?= Url::to(['company-blacklisted-ip/']);?>"><?= Yii::t('app', 'Blacklisted IP') ?></a></li>
                </ul>
            </li>
            <li><a href="<?= Url::to(['office-hour/']); ?>"><i class="fa fa-clock-o"></i> <span
                            class="nav-label"><?= Yii::t('app', 'Office Hour') ?></span></a></li>
            <li><a href="<?= Url::to(['ringgroup/']); ?>"><i class="fa fa-life-ring"></i> <span
                            class="nav-label"><?= Yii::t('app', 'Ring Group') ?></span></a></li>
            <li><a href="<?= Url::to(['call-reason/']); ?>"><i class="fa fa-phone"></i> <span
                            class="nav-label"><?= Yii::t('app', 'Call Reason') ?></span></a></li>
        </ul>
    </nav>

</aside>
<!-- Aside Ends-->
