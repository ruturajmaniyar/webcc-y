<?php

use backend\models\Voicemail;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SipEndpoint */
/* @var $sipSubscriptionModel backend\models\SipSubscriber */
/* @var $form yii\widgets\ActiveForm */
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

<div class="sip-endpoint-form-class" id="sip-endpoint-form-id">
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'sip-endpoint-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>

    <div class="row">
        <div class="form-group">
            <?php
            if ($model->isNewRecord) {
                echo $form->field($sipSubscriptionModel, 'username', [
                    'options' => ['class' => 'col-xs-12 col-md-6'],
                    'template' => '{label}<div class="input-group m-t-10">{input}<span class="input-group-btn">
                    <button type="button" id="sip-endpoint-username-regenerate" class="btn btn-effect-ripple btn-primary"><i class="ion-refresh"></i></button>
                </span>{error}{hint}</div>'
                ])->textInput([
                    'class' => 'form-control input-lg',
                    'maxlength' => true,
                    'readonly' => true
                ]);
            } else {
                echo $form->field($model->sipSubscriber, 'username', [
                    'options' => ['class' => 'col-xs-12 col-md-6'],
                ])->textInput([
                    'class' => 'form-control input-lg',
                    'maxlength' => true,
                    'readonly' => true
                ]);
            } ?>

            <?= $form->field(($model->isNewRecord) ? $sipSubscriptionModel : $model->sipSubscriber, 'password', [
                'options' => ['class' => 'col-xs-12 col-md-6'],
                'template' => '{label}<div class="input-group m-t-10">{input}
                            <span class="input-group-btn"><button type="button" id="sip-endpoint-password-show" class="btn btn-effect-ripple btn-warning"><i class="ion-eye"></i></button></span>
                            <span class="input-group-btn"><button type="button" id="sip-endpoint-password-regenerate" class="btn btn-effect-ripple btn-primary"><i class="ion-refresh"></i></button></span>
                            {error}{hint}</div>'
            ])->passwordInput([
                'class' => 'form-control input-lg',
                'maxlength' => true,
                'readonly' => true
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'given_name', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control input-lg',
                'maxlength' => true
            ]) ?>
            <?= $form->field($model, 'is_active',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                ['class' => 'select2 select2-offscreen']) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'no_answer_action_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['noActionId'],
                ['class' => 'select2 select2-offscreen']) ?>

            <?= $form->field($model, 'no_answer_destination_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Voicemail::getVoicemailList(), [
                'prompt' => 'Choose a Voicemail',
                'class' => 'select2 select2-offscreen',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'call_recording',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['boolean'],
                ['class' => 'select2 select2-offscreen']) ?>

            <?php if (!$model->isNewRecord) { ?>
                <?= $form->field($model->company, 'website',
                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                    'class' => 'form-control input-lg',
                    'maxlength' => true,
                    'readonly' => true,
                    'value' => str_replace('www.', '', $model->company->website),
                ])->label(Yii::t('app', 'Domain')) ?>
            <?php } ?>
        </div>
    </div>

    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?php if ($model->isNewRecord) {
                echo Html::submitButton(Yii::t('app', 'Save'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
            } else {
                echo Html::submitButton(Yii::t('app', 'Update'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                echo Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
            }
            echo Html::a(Yii::t('app', 'Back'),
                ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJsFile('@web/js/modules/sip-endpoint-no-answer-action.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>
