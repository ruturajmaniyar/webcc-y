<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SipEndpoint */
/* @var $sipSubscriptionModel backend\models\SipSubscriber */

$this->title = Yii::t('app', 'Create Sip Endpoint');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Endpoints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sip-endpoint-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sipSubscriptionModel' => $sipSubscriptionModel,
    ]) ?>

</div>
