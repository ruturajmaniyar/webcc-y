<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SipEndpoint */

$this->title = Yii::t('app', 'Update Sip Endpoint: {nameAttribute}', [
    'nameAttribute' => $model->given_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Endpoints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sip-endpoint-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
