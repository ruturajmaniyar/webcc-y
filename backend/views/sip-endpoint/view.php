<?php

use backend\models\Voicemail;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SipEndpoint */

$this->title = $model->given_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Endpoints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>
<div class="sip-endpoint-view-class" id="sip-endpoint-view-class">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id],
            ['class' => 'btn btn-form btn-purple btn-rounded']) ?>
        <?= Html::a(Yii::t('app', 'Listing'), ['index', 'page' => $page],
            ['class' => 'btn btn-form btn-inverse btn-rounded']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'given_name',
            'sipSubscriber.username',
            'sipSubscriber.password',
            [
                'label' => Yii::t('app', 'No Answer Call'),
                'value' => ($model->no_answer_action_id == 1) ? Yii::t('app', 'Disconnect Call') : Yii::t('app', 'Drop Voicemail')
            ],
            [
                'label' => Yii::t('app', 'No Answer Destination'),
                'value' => (!empty($model->no_answer_destination_id)) ? Voicemail::findOne($model->no_answer_destination_id)->name : Yii::t('app', 'Unavailable')
            ],
            [
                'label' => Yii::t('app', 'Call Recording'),
                'value' => ($model->call_recording) ? Yii::t('app', 'Enabled') : Yii::t('app', 'Disabled')
            ],
            [
                'label' => Yii::t('app', 'Record After Connect'),
                'value' => ($model->record_after_connect == 1) ? Yii::t('app', 'Enabled') : Yii::t('app', 'Disabled')
            ],
            'created_on',
            [
                'label' => Yii::t('app', 'Status'),
                'value' => ($model->is_active) ? Yii::t('app', 'Active') : Yii::t('app', 'Inactive')
            ]
        ],
    ]) ?>

</div>
