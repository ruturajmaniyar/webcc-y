<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SipTrunkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel-group sip-trunk-search-class" id="sip-trunk-search-id">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#sip-trunk-search-id"
                   href="#sip-trunk-search-id-collapse"
                   aria-expanded="false" class="collapsed">
                    <?= Yii::t('app', 'Search') ?>
                </a>
            </h4>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'sip-trunk-search-form',
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>
        <div class="panel-collapse collapse" id="sip-trunk-search-id-collapse" aria-expanded="false">
            <div class="panel-body">
                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'name',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>

                        <?= $form->field($model, 'is_active',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                            ['class' => 'select2', 'prompt' => 'All']) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'trunk_ip',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>

                        <?= $form->field($model, 'backup_trunk_ip',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                            'class' => 'form-control input-lg',
                            'maxlength' => true
                        ]) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <?= $form->field($model, 'no_answer_action_id',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['noActionId'],
                            ['class' => 'select2', 'prompt' => 'All']) ?>

                        <?= $form->field($model, 'call_recording',
                            ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['boolean'],
                            ['class' => 'select2', 'prompt' => 'All']) ?>

                    </div>
                </div>

                <div class="break-rule"></div>
                <div class="row">
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'Search'),
                            ['class' => 'btn btn-form btn-primary btn-rounded']) ?>
                        <?= Html::a(Yii::t('app', 'Reset'), ['index', 'page' => Yii::$app->session->get('page')],
                            ['class' => 'btn btn-form btn-default btn-rounded']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>