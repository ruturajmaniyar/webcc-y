<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SipTrunk */

$this->title = Yii::t('app', 'Create Sip Trunk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sip Trunks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sip-trunk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
