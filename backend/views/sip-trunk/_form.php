<?php

use backend\models\Voicemail;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SipTrunk */
/* @var $form yii\widgets\ActiveForm */
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
?>

<div class="sip-trunk-form-class" id="sip-trunk-form-class">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'sip-trunk-form',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]
    ]); ?>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'name', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control input-lg',
                'maxlength' => true
            ]) ?>
            <?= $form->field($model, 'is_active',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['status'],
                ['class' => 'select2 select2-offscreen']) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'trunk_ip', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control input-lg',
                'maxlength' => true
            ]) ?>

            <?= $form->field($model, 'backup_trunk_ip', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                'class' => 'form-control input-lg',
                'maxlength' => true
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'no_answer_action_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['noActionId'],
                ['class' => 'select2 select2-offscreen']) ?>

            <?= $form->field($model, 'no_answer_destination_id',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Voicemail::getVoicemailList(), [
                'prompt' => 'Choose a Voicemail',
                'class' => 'select2 select2-offscreen',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= $form->field($model, 'call_recording',
                ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList(Yii::$app->params['boolean'],
                ['class' => 'select2 select2-offscreen']) ?>
        </div>
    </div>

    <div class="break-rule"></div>
    <div class="row">
        <div class="form-group text-center">
            <?php if ($model->isNewRecord) {
                echo Html::submitButton(Yii::t('app', 'Save'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'save', 'value' => 'save']);
            } else {
                echo Html::submitButton(Yii::t('app', 'Update'),
                    ['class' => 'btn btn-form btn-success btn-rounded', 'name' => 'update', 'value' => 'update']);
                echo Html::submitButton(Yii::t('app', 'Apply'),
                    ['class' => 'btn btn-form btn-purple btn-rounded', 'name' => 'apply', 'value' => 'apply']);
            }
            echo Html::a(Yii::t('app', 'Back'),
                ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                ['class' => 'btn btn-form btn-danger btn-round-right btn-rounded']);
            ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJsFile('@web/js/modules/sip-trunk-no-answer-action.js', [
    'depends' => [\backend\assets\WcAppAsset::className()]
]);
?>
