<?php
/**
 * Created by PhpStorm.
 * User: akshay
 * Date: 26/3/18
 * Time: 12:14 AM
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Html;

/**
 * Class Helper
 * @package app\components
 */
class Helper extends Component
{
    /**
     * To get common layout for Grid View
     *
     * @param $search_form_selector
     * @return string
     */
    public function get_layout_str($search_form_selector)
    {
        return '<p class="pull-left">' . Html::dropDownList('records_per_page',
                Yii::$app->session->get('per-page-result'), Yii::$app->params['recordsPerPage'], [
                    'class' => 'form-control',
                    'id' => 'change-records-per-page',
                    'onchange' => 'change_records_per_page($(this), "' . $search_form_selector . '")',
                ]) . '
                </p>
                <div class="pull-right">
                    {summary}
                </div>
                {items}
                <div class="pull-right">
                    {pager}
                </div>';
    }

    /**
     * Set number of records per page
     *
     * @return mixed
     */
    public function get_per_page_record_count()
    {
        // check if it in a get request
        if (Yii::$app->request->get('per-page')) {
            Yii::$app->session->set('per-page-result', Yii::$app->request->get('per-page'));
        } else {
            // check if stored in session
            if (!Yii::$app->session->get('per-page-result')) {
                // if not then store it in session
                Yii::$app->session->set('per-page-result', Yii::$app->params['defaultPageSize']);
            }
        }

        return Yii::$app->session->get('per-page-result');
    }


    /**
     * @param $date
     * @param $time
     * @return string
     */
    public function joinDateTime($date, $time)
    {
        $time24Format = $this->get24HourFormat($time);
        return $date . ' ' . $time24Format;
    }

    /**
     * @param $time
     * @return false|string
     */
    public function get24HourFormat($time)
    {
        return date("H:i:s", strtotime($time));
    }

    /**
     * @param $datetime
     * @return array
     */
    public function splitDateTime($datetime)
    {
        $result = explode(' ', $datetime);
        $time12Format = $this->get12HourFormat($result[1]);
        return ['date' => $result[0], 'time' => $time12Format];
    }

    /**
     * @param $time
     * @return false|string
     */
    public function get12HourFormat($time)
    {
        return date("g:i a", strtotime($time));
    }
}