<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_domain".
 *
 * @property int $company_id
 * @property string $domain
 * @property string $note_for_invalid_country
 * @property int $is_active
 *
 * @property Company $company
 */
class CompanyDomain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_domain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'domain'], 'required'],
            [['company_id', 'is_active'], 'integer'],
            [['domain'], 'string', 'max' => 255],
            [['note_for_invalid_country'], 'string', 'max' => 560],
            [['company_id', 'domain'], 'unique', 'targetAttribute' => ['company_id', 'domain']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'domain' => Yii::t('app', 'Domain'),
            'note_for_invalid_country' => Yii::t('app', 'Note For Invalid Country'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
