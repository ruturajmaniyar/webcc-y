<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ringgroup_member_call_failed".
 *
 * @property int $ringgroup_call_id
 * @property int $sip_endpoint_id
 * @property string $failed_reason
 * @property string $call_start_time
 * @property string $call_end_time
 *
 * @property RinggroupCall $ringgroupCall
 * @property SipEndpoint $sipEndpoint
 */
class RinggroupMemberCallFailed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ringgroup_member_call_failed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ringgroup_call_id', 'sip_endpoint_id', 'failed_reason'], 'required'],
            [['ringgroup_call_id', 'sip_endpoint_id'], 'integer'],
            [['call_start_time', 'call_end_time'], 'safe'],
            [['failed_reason'], 'string', 'max' => 255],
            [['ringgroup_call_id', 'sip_endpoint_id'], 'unique', 'targetAttribute' => ['ringgroup_call_id', 'sip_endpoint_id']],
            [['ringgroup_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => RinggroupCall::className(), 'targetAttribute' => ['ringgroup_call_id' => 'id']],
            [['sip_endpoint_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipEndpoint::className(), 'targetAttribute' => ['sip_endpoint_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ringgroup_call_id' => Yii::t('app', 'Ringgroup Call ID'),
            'sip_endpoint_id' => Yii::t('app', 'Sip Endpoint ID'),
            'failed_reason' => Yii::t('app', 'Failed Reason'),
            'call_start_time' => Yii::t('app', 'Call Start Time'),
            'call_end_time' => Yii::t('app', 'Call End Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCall()
    {
        return $this->hasOne(RinggroupCall::className(), ['id' => 'ringgroup_call_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoint()
    {
        return $this->hasOne(SipEndpoint::className(), ['id' => 'sip_endpoint_id']);
    }
}
