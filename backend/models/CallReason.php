<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "call_reason".
 *
 * @property int $id
 * @property int $company_id
 * @property int $parent_id
 * @property string $reason
 * @property string $description
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 * @property string $action_type
 *
 * @property Company $company
 * @property CallReason $parent
 * @property CallReason[] $callReasons
 * @property Admin $admin
 * @property CallReasonDestination $callReasonDestination
 * @property RinggroupCall[] $ringgroupCalls
 * @property SipEndpointCall[] $sipEndpointCalls
 * @property SipTrunkCall[] $sipTrunkCalls
 * @property VoicemailCall[] $voicemailCalls
 */
class CallReason extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_reason';
    }

    public static function callReasonAction()
    {
        return [
            'SUB_REASON' => 'Sub Reason',
            'CALL_ACTION' => 'Call Action',
        ];
    }

    public static function getCallReasonCount($company_id, $nested = null)
    {
        $callReason = static::find()->where(['company_id' => $company_id, 'parent_id' => null])->all();
        $count = count($callReason);
        if ($nested) {
            foreach ($callReason as $key => $reason) {
                $callReasonCount = static::find()->where([
                    'company_id' => $company_id,
                    'parent_id' => $reason->id
                ])->count();
                $count = $count + $callReasonCount;
            }
            return $count;
        }
        return $count;
    }

    public static function fetchIdbyName($reason)
    {
        $callReason = static::findOne(['reason' => $reason]);
        if($callReason instanceof CallReason){
            return $callReason->id;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'reason', 'admin_id'], 'required'],
            [['company_id', 'parent_id', 'admin_id', 'is_active'], 'integer'],
            [['created_on'], 'safe'],
            [['reason', 'description'], 'string', 'max' => 255],
            [['company_id', 'reason'], 'unique', 'targetAttribute' => ['company_id', 'reason']],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['parent_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CallReason::className(),
                'targetAttribute' => ['parent_id' => 'id']
            ],
            [
                ['admin_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Admin::className(),
                'targetAttribute' => ['admin_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'reason' => Yii::t('app', 'Reason'),
            'description' => Yii::t('app', 'Description'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
            'action_type' => Yii::t('app', 'Action Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CallReason::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReasons()
    {
        return $this->hasMany(CallReason::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReasonDestination()
    {
        return $this->hasOne(CallReasonDestination::className(), ['call_reason_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCalls()
    {
        return $this->hasMany(RinggroupCall::className(), ['call_reason_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCalls()
    {
        return $this->hasMany(SipEndpointCall::className(), ['call_reason_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunkCalls()
    {
        return $this->hasMany(SipTrunkCall::className(), ['call_reason_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailCalls()
    {
        return $this->hasMany(VoicemailCall::className(), ['call_reason_id' => 'id']);
    }
}
