<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "office_hour_time_slot".
 *
 * @property int $office_hour_id
 * @property string $start_time
 * @property string $end_time
 * @property string $run_on_days 1-Sunday and so on, comma seperated
 *
 * @property OfficeHour $officeHour
 */
class OfficeHourTimeSlot extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_hour_time_slot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time','run_on_days'], 'required'],
            [['office_hour_id'], 'integer'],
            [['start_time', 'end_time'], 'time','format' => 'php:H:i:s'],
            [['start_time', 'end_time'], 'safe'],
            ['start_time', 'compare', 'compareAttribute' => 'end_time', 'operator' => '<', 'enableClientValidation' => false],
            [['office_hour_id', 'start_time', 'end_time'], 'unique', 'targetAttribute' => ['office_hour_id', 'start_time', 'end_time']],
            [['office_hour_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeHour::className(), 'targetAttribute' => ['office_hour_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'office_hour_id' => Yii::t('app', 'Office Hour ID'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
            'run_on_days' => Yii::t('app', 'Run On Days'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHour()
    {
        return $this->hasOne(OfficeHour::className(), ['id' => 'office_hour_id']);
    }

    public static function weekDays()
    {
        return [
            '1' => 'Sunday',
            '2' => 'Monday',
            '3' => 'Tuesday',
            '4' => 'Wednesday',
            '5' => 'Thursday',
            '6' => 'Friday',
            '7' => 'Saturday',
        ];
    }
}
