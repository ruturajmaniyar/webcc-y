<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompanyCallSettingSearch represents the model behind the search form of `backend\models\CompanyCallSetting`.
 */
class CompanyCallSettingSearch extends CompanyCallSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'concurrent_call_per_ip', 'daily_call_per_ip'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyCallSetting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['company_id' => SORT_DESC]],
            'pagination' => ['pageSize' => Yii::$app->helper->get_per_page_record_count()],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_id' => Yii::$app->user->identity->company_id,
            'concurrent_call_per_ip' => $this->concurrent_call_per_ip,
            'daily_call_per_ip' => $this->daily_call_per_ip,
        ]);

        return $dataProvider;
    }
}
