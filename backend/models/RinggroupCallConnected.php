<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ringgroup_call_connected".
 *
 * @property int $ringgroup_call_id
 * @property int $sip_endpoint_id
 * @property string $call_recording timestamp_subscriberid_caller.wav
 * @property string $recording_upload_time
 *
 * @property RinggroupCall $ringgroupCall
 * @property SipEndpoint $sipEndpoint
 */
class RinggroupCallConnected extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ringgroup_call_connected';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ringgroup_call_id', 'sip_endpoint_id'], 'required'],
            [['ringgroup_call_id', 'sip_endpoint_id'], 'integer'],
            [['recording_upload_time'], 'safe'],
            [['call_recording'], 'string', 'max' => 255],
            [['ringgroup_call_id'], 'unique'],
            [['ringgroup_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => RinggroupCall::className(), 'targetAttribute' => ['ringgroup_call_id' => 'id']],
            [['sip_endpoint_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipEndpoint::className(), 'targetAttribute' => ['sip_endpoint_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ringgroup_call_id' => Yii::t('app', 'Ringgroup Call ID'),
            'sip_endpoint_id' => Yii::t('app', 'Sip Endpoint ID'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'recording_upload_time' => Yii::t('app', 'Recording Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCall()
    {
        return $this->hasOne(RinggroupCall::className(), ['id' => 'ringgroup_call_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoint()
    {
        return $this->hasOne(SipEndpoint::className(), ['id' => 'sip_endpoint_id']);
    }
}
