<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_trunk_call_failed".
 *
 * @property int $sip_trunk_call_id
 * @property string $failed_reason
 * @property int $action_id
 * @property int $destination_id Voicemail id
 * @property string $voicemail timestamp_voicemailid_caller_vm.wav
 * @property string $voicemail_upload_time
 *
 * @property SipTrunkCall $sipTrunkCall
 */
class SipTrunkCallFailed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_trunk_call_failed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sip_trunk_call_id', 'failed_reason', 'action_id'], 'required'],
            [['sip_trunk_call_id', 'action_id', 'destination_id'], 'integer'],
            [['voicemail_upload_time'], 'safe'],
            [['failed_reason', 'voicemail'], 'string', 'max' => 255],
            [['sip_trunk_call_id'], 'unique'],
            [['sip_trunk_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipTrunkCall::className(), 'targetAttribute' => ['sip_trunk_call_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sip_trunk_call_id' => Yii::t('app', 'Sip Trunk Call ID'),
            'failed_reason' => Yii::t('app', 'Failed Reason'),
            'action_id' => Yii::t('app', 'Action ID'),
            'destination_id' => Yii::t('app', 'Destination ID'),
            'voicemail' => Yii::t('app', 'Voicemail'),
            'voicemail_upload_time' => Yii::t('app', 'Voicemail Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunkCall()
    {
        return $this->hasOne(SipTrunkCall::className(), ['id' => 'sip_trunk_call_id']);
    }
}
