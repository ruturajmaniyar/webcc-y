<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_email".
 *
 * @property int $id
 * @property int $company_id
 * @property string $email
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property Company $company
 * @property Admin $admin
 * @property Voicemail[] $voicemails
 */
class CompanyEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'email', 'admin_id'], 'required'],
            [['company_id', 'admin_id', 'is_active'], 'integer'],
            [['created_on'], 'safe'],
            [['email'], 'string', 'max' => 255],
            ['email', 'email'],
            [['company_id', 'email'], 'unique', 'targetAttribute' => ['company_id', 'email']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'email' => Yii::t('app', 'Email'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemails()
    {
        return $this->hasMany(Voicemail::className(), ['company_email_id' => 'id']);
    }

    /**
     * @param $id
     * @return array
     */
    public static function getCompanyEmailArrayByAdminId($id){

        if (($model = self::findAll(['admin_id' => $id, 'is_active' => 1])) !== null) {
            return ArrayHelper::map($model, 'id', 'email');
        }
        return [];
    }
}
