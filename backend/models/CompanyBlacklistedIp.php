<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_blacklisted_ip".
 *
 * @property int $company_id
 * @property resource $ip inet6_aton
 * @property int $netmask
 * @property int $duration duration in days
 *
 * @property Company $company
 */
class CompanyBlacklistedIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_blacklisted_ip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'ip', 'duration'], 'required'],
            [['company_id', 'netmask', 'duration'], 'integer'],
            [['netmask'], 'default','value'=>0 ],
            [['ip'], 'ip','ipv4' => true, 'subnet' => null, 'expandIPv6' => false],
            [['company_id', 'ip'], 'unique', 'targetAttribute' => ['company_id', 'ip']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'ip' => Yii::t('app', 'Ip'),
            'netmask' => Yii::t('app', 'Netmask'),
            'duration' => Yii::t('app', 'Duration'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
