<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "call_reason_destination".
 *
 * @property int $call_reason_id
 * @property int $office_hour_id
 * @property int $office_hours_action_id Points to call_action
 * @property int $non_office_hours_action_id
 * @property int $office_hours_destination_id Points to respective table of Call action
 * @property int $non_office_hours_destination_id
 *
 * @property CallReason $callReason
 * @property OfficeHour $officeHour
 */
class CallReasonDestination extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_reason_destination';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_reason_id', 'office_hours_action_id', 'non_office_hours_action_id'], 'required'],
            [
                ['office_hours_destination_id', 'non_office_hours_destination_id'],
                'required',
                'when' => function ($model){
                    if($model->office_hours_action_id !=1){
                        return true;
                    }
                    return false;
                }
            ],
            [
                [
                    'call_reason_id',
                    'office_hour_id',
                    'office_hours_action_id',
                    'non_office_hours_action_id',
                    'office_hours_destination_id',
                    'non_office_hours_destination_id'
                ],
                'integer'
            ],
            [['call_reason_id'], 'unique'],
            [
                ['call_reason_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CallReason::className(),
                'targetAttribute' => ['call_reason_id' => 'id']
            ],
            [
                ['office_hour_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OfficeHour::className(),
                'targetAttribute' => ['office_hour_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'call_reason_id' => Yii::t('app', 'Call Reason ID'),
            'office_hour_id' => Yii::t('app', 'Office Hour ID'),
            'office_hours_action_id' => Yii::t('app', 'Office Hours Action ID'),
            'non_office_hours_action_id' => Yii::t('app', 'Non Office Hours Action ID'),
            'office_hours_destination_id' => Yii::t('app', 'Office Hours Destination ID'),
            'non_office_hours_destination_id' => Yii::t('app', 'Non Office Hours Destination ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReason()
    {
        return $this->hasOne(CallReason::className(), ['id' => 'call_reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHour()
    {
        return $this->hasOne(OfficeHour::className(), ['id' => 'office_hour_id']);
    }
}
