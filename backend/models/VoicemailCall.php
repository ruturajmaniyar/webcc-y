<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "voicemail_call".
 *
 * @property int $id
 * @property int $call_reason_id
 * @property int $voicemail_id
 * @property resource $caller_ip inet6_aton
 * @property int $country_id
 * @property string $call_reason
 * @property string $caller_number Number without country code and leading 0
 * @property string $caller_name
 * @property string $caller_email
 * @property string $domain
 * @property string $call_start_time
 * @property string $call_end_time
 * @property string $voicemail timestamp_voicemailid_caller_vm.wav
 * @property string $voicemail_upload_time
 *
 * @property CallReason $callReason
 * @property Voicemail $voicemail0
 * @property Country $country
 */
class VoicemailCall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voicemail_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_reason_id', 'voicemail_id', 'caller_ip', 'country_id', 'call_reason', 'caller_number', 'caller_name', 'caller_email', 'domain'], 'required'],
            [['call_reason_id', 'voicemail_id', 'country_id', 'caller_number'], 'integer'],
            [['call_start_time', 'call_end_time', 'voicemail_upload_time'], 'safe'],
            [['caller_ip'], 'string', 'max' => 16],
            [['call_reason', 'caller_name', 'caller_email', 'domain', 'voicemail'], 'string', 'max' => 255],
            [['call_reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallReason::className(), 'targetAttribute' => ['call_reason_id' => 'id']],
            [['voicemail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Voicemail::className(), 'targetAttribute' => ['voicemail_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'call_reason_id' => Yii::t('app', 'Call Reason ID'),
            'voicemail_id' => Yii::t('app', 'Voicemail ID'),
            'caller_ip' => Yii::t('app', 'Caller Ip'),
            'country_id' => Yii::t('app', 'Country ID'),
            'call_reason' => Yii::t('app', 'Call Reason'),
            'caller_number' => Yii::t('app', 'Caller Number'),
            'caller_name' => Yii::t('app', 'Caller Name'),
            'caller_email' => Yii::t('app', 'Caller Email'),
            'domain' => Yii::t('app', 'Domain'),
            'call_start_time' => Yii::t('app', 'Call Start Time'),
            'call_end_time' => Yii::t('app', 'Call End Time'),
            'voicemail' => Yii::t('app', 'Voicemail'),
            'voicemail_upload_time' => Yii::t('app', 'Voicemail Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReason()
    {
        return $this->hasOne(CallReason::className(), ['id' => 'call_reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemail0()
    {
        return $this->hasOne(Voicemail::className(), ['id' => 'voicemail_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
