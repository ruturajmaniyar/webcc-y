<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "call_action".
 *
 * @property int $id
 * @property string $action
 * @property string $description
 * @property string $action_table
 * @property int $is_active
 */
class CallAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_action';
    }

    public static function getNameById($id)
    {
        $model = static::findOne(['id' => $id]);
        if($model instanceof CallAction){
            return $model->action;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'description', 'action_table'], 'required'],
            [['is_active'], 'integer'],
            [['action', 'description', 'action_table'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'action' => Yii::t('app', 'Action'),
            'description' => Yii::t('app', 'Description'),
            'action_table' => Yii::t('app', 'Action Table'),
            'is_active' => Yii::t('app', 'Is Active'),
        ];
    }

    public static function getCallAction()
    {
        return static::find()->where(['is_active' => 1])->all();
    }
}
