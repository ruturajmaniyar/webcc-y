<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_plan_override_history".
 *
 * @property int $company_id
 * @property int $plan_id
 * @property string $updated_on
 * @property int $duration in Days
 * @property int $charge
 * @property int $currency_id
 * @property int $concurrent_calls
 * @property int $max_calls_per_day
 * @property int $max_agents
 * @property int $max_managers
 * @property int $max_sip_endpoints
 * @property int $max_sip_trunks
 * @property int $max_call_reasons
 * @property int $max_voicemail_boxes
 * @property int $expiry_period_vm In Days
 * @property int $allow_call_recording
 * @property int $expiry_period_recording In Days
 *
 * @property Company $company
 * @property Plan $plan
 * @property Currency $currency
 */
class CompanyPlanOverrideHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_plan_override_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'plan_id', 'updated_on', 'duration', 'charge', 'currency_id', 'concurrent_calls', 'max_calls_per_day', 'max_agents', 'max_managers', 'max_sip_endpoints', 'max_sip_trunks', 'max_call_reasons', 'max_voicemail_boxes', 'expiry_period_vm', 'expiry_period_recording'], 'required'],
            [['company_id', 'plan_id', 'duration', 'charge', 'currency_id', 'concurrent_calls', 'max_calls_per_day', 'max_agents', 'max_managers', 'max_sip_endpoints', 'max_sip_trunks', 'max_call_reasons', 'max_voicemail_boxes', 'expiry_period_vm', 'allow_call_recording', 'expiry_period_recording'], 'integer'],
            [['updated_on'], 'safe'],
            [['company_id'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'duration' => Yii::t('app', 'Duration'),
            'charge' => Yii::t('app', 'Charge'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'concurrent_calls' => Yii::t('app', 'Concurrent Calls'),
            'max_calls_per_day' => Yii::t('app', 'Max Calls Per Day'),
            'max_agents' => Yii::t('app', 'Max Agents'),
            'max_managers' => Yii::t('app', 'Max Managers'),
            'max_sip_endpoints' => Yii::t('app', 'Max Sip Endpoints'),
            'max_sip_trunks' => Yii::t('app', 'Max Sip Trunks'),
            'max_call_reasons' => Yii::t('app', 'Max Call Reasons'),
            'max_voicemail_boxes' => Yii::t('app', 'Max Voicemail Boxes'),
            'expiry_period_vm' => Yii::t('app', 'Expiry Period Vm'),
            'allow_call_recording' => Yii::t('app', 'Allow Call Recording'),
            'expiry_period_recording' => Yii::t('app', 'Expiry Period Recording'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}
