<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "voicemail".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $voicemail_prompt_id
 * @property int $company_email_id Voicemail to Email
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property Company $company
 * @property VoicemailPrompt $voicemailPrompt
 * @property Admin $admin
 * @property CompanyEmail $companyEmail
 * @property VoicemailCall[] $voicemailCalls
 */
class Voicemail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voicemail';
    }

    /**
     * Get Voicemail List
     *
     * @return array
     */
    public static function getVoicemailList()
    {
        if ($voicemailModel = self::findAll([
            'company_id' => Yii::$app->user->identity->company_id,
            'admin_id' => Yii::$app->user->id
        ])) {

            return ArrayHelper::map($voicemailModel, 'id', 'name');
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'admin_id'], 'required'],
            [['company_id', 'voicemail_prompt_id', 'company_email_id', 'admin_id', 'is_active'], 'integer'],
            [['created_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['company_id', 'name'], 'unique', 'targetAttribute' => ['company_id', 'name']],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['voicemail_prompt_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => VoicemailPrompt::className(),
                'targetAttribute' => ['voicemail_prompt_id' => 'id']
            ],
            [
                ['admin_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Admin::className(),
                'targetAttribute' => ['admin_id' => 'id']
            ],
            [
                ['company_email_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CompanyEmail::className(),
                'targetAttribute' => ['company_email_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company'),
            'name' => Yii::t('app', 'Name'),
            'voicemail_prompt_id' => Yii::t('app', 'Voicemail Prompt'),
            'company_email_id' => Yii::t('app', 'Email for Voicemail'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailPrompt()
    {
        return $this->hasOne(VoicemailPrompt::className(), ['id' => 'voicemail_prompt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyEmail()
    {
        return $this->hasOne(CompanyEmail::className(), ['id' => 'company_email_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailCalls()
    {
        return $this->hasMany(VoicemailCall::className(), ['voicemail_id' => 'id']);
    }
}
