<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_allowed_country".
 *
 * @property int $company_id
 * @property int $country_id NULL-Allow all the countries
 * @property int $is_active
 *
 * @property Company $company
 * @property Country $country
 */
class CompanyAllowedCountry extends ActiveRecord
{

    public static function primaryKey()
    {
        return ['company_id', 'country_id'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_allowed_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'country_id', 'is_active'], 'integer'],
            [['company_id', 'country_id'], 'unique', 'targetAttribute' => ['company_id', 'country_id']],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['country_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Country::className(),
                'targetAttribute' => ['country_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
