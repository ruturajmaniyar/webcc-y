<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "currency_conversion".
 *
 * @property int $from_currency_id
 * @property int $to_currency_id
 * @property double $rate
 *
 * @property Currency $fromCurrency
 * @property Currency $toCurrency
 */
class CurrencyConversion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_conversion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_currency_id', 'to_currency_id', 'rate'], 'required'],
            [['from_currency_id', 'to_currency_id'], 'integer'],
            [['rate'], 'number'],
            [['from_currency_id', 'to_currency_id'], 'unique', 'targetAttribute' => ['from_currency_id', 'to_currency_id']],
            [['from_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['from_currency_id' => 'id']],
            [['to_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['to_currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from_currency_id' => Yii::t('app', 'From Currency ID'),
            'to_currency_id' => Yii::t('app', 'To Currency ID'),
            'rate' => Yii::t('app', 'Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'from_currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'to_currency_id']);
    }
}
