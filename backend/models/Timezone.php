<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "timezone".
 *
 * @property int $id
 * @property int $country_id
 * @property string $ui_name
 * @property string $timezone mysql supported name
 * @property string $description
 * @property int $is_active
 *
 * @property Admin[] $admins
 * @property Company[] $companies
 * @property OfficeHour[] $officeHours
 * @property Country $country
 */
class Timezone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'ui_name', 'timezone'], 'required'],
            [['country_id', 'is_active'], 'integer'],
            [['ui_name', 'timezone'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
            [['country_id', 'ui_name', 'timezone'], 'unique', 'targetAttribute' => ['country_id', 'ui_name', 'timezone']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'ui_name' => Yii::t('app', 'Ui Name'),
            'timezone' => Yii::t('app', 'Timezone'),
            'description' => Yii::t('app', 'Description'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins()
    {
        return $this->hasMany(Admin::className(), ['timezone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['timezone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHours()
    {
        return $this->hasMany(OfficeHour::className(), ['timezone_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public static function getActiveTimezoneArray(){

        if($model = self::findAll(['is_active' => 1])){
            return ArrayHelper::map($model, 'ui_name', 'ui_name');
        }
        return [];
    }

    public static function getTimezoneList()
    {
        if($model = self::findAll(['is_active' => 1])){
            return ArrayHelper::map($model, 'country_id', 'ui_name');
        }
        return [];
    }
}
