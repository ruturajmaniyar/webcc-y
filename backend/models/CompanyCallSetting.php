<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_call_setting".
 *
 * @property int $company_id
 * @property int $concurrent_call_per_ip
 * @property int $daily_call_per_ip
 *
 * @property Company $company
 */
class CompanyCallSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_call_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'concurrent_call_per_ip', 'daily_call_per_ip'], 'required'],
            [['company_id', 'concurrent_call_per_ip', 'daily_call_per_ip'], 'integer'],
            [['company_id'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'concurrent_call_per_ip' => Yii::t('app', 'Concurrent Call Per Ip'),
            'daily_call_per_ip' => Yii::t('app', 'Daily Call Per Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
