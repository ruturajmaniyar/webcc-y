<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "voicemail_prompt".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $prompt_name Unavailable message for voicemail
 * @property string $unique_name Unique name for prompt. cid_yymmddhhmmss.wav
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property Voicemail[] $voicemails
 * @property Company $company
 * @property Admin $admin
 */
class VoicemailPrompt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'voicemail_prompt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'prompt_name', 'unique_name', 'admin_id'], 'required'],
            [['company_id', 'admin_id', 'is_active'], 'integer'],
            [['created_on'], 'safe'],
            [['name', 'prompt_name', 'unique_name'], 'string', 'max' => 255],
            [['unique_name'], 'unique'],
            [['company_id', 'name'], 'unique', 'targetAttribute' => ['company_id', 'name']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'name' => Yii::t('app', 'Name'),
            'prompt_name' => Yii::t('app', 'Prompt Name'),
            'unique_name' => Yii::t('app', 'Unique Name'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemails()
    {
        return $this->hasMany(Voicemail::className(), ['voicemail_prompt_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @param $id
     * @return array
     */
    public static function getVoicemailPromptArrayByAdminId($id){

        if (($model = self::findAll(['admin_id' => $id, 'is_active' => 1])) !== null) {
           return ArrayHelper::map($model, 'id', 'prompt_name');
        }
        return [];
    }
}
