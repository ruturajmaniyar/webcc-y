<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property int $id
 * @property int $plan_type 1-PBX,2-Call Center
 * @property string $name
 * @property string $description
 * @property int $concurrent_calls
 * @property int $max_domains Domain for company
 * @property int $max_calls_per_day
 * @property int $max_sip_endpoints
 * @property int $max_sip_trunks
 * @property int $max_ringgroups
 * @property int $max_call_reasons
 * @property int $max_voicemail_boxes
 * @property int $expiry_period_vm In Days
 * @property int $allow_call_recording
 * @property int $expiry_period_recording In Days
 * @property int $is_active
 *
 * @property CompanyPlanOverrideHistory[] $companyPlanOverrideHistories
 * @property PlanDuration[] $planDurations
 * @property PlanForCallCenter $planForCallCenter
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_type', 'concurrent_calls', 'max_domains', 'max_calls_per_day', 'max_sip_endpoints', 'max_sip_trunks', 'max_ringgroups', 'max_call_reasons', 'max_voicemail_boxes', 'expiry_period_vm', 'allow_call_recording', 'expiry_period_recording', 'is_active'], 'integer'],
            [['name', 'concurrent_calls', 'max_calls_per_day', 'max_sip_endpoints', 'max_sip_trunks', 'max_ringgroups', 'max_call_reasons', 'max_voicemail_boxes', 'expiry_period_vm', 'expiry_period_recording'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_type' => Yii::t('app', 'Plan Type'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'concurrent_calls' => Yii::t('app', 'Concurrent Calls'),
            'max_domains' => Yii::t('app', 'Max Domains'),
            'max_calls_per_day' => Yii::t('app', 'Max Calls Per Day'),
            'max_sip_endpoints' => Yii::t('app', 'Max Sip Endpoints'),
            'max_sip_trunks' => Yii::t('app', 'Max Sip Trunks'),
            'max_ringgroups' => Yii::t('app', 'Max Ringgroups'),
            'max_call_reasons' => Yii::t('app', 'Max Call Reasons'),
            'max_voicemail_boxes' => Yii::t('app', 'Max Voicemail Boxes'),
            'expiry_period_vm' => Yii::t('app', 'Expiry Period Vm'),
            'allow_call_recording' => Yii::t('app', 'Allow Call Recording'),
            'expiry_period_recording' => Yii::t('app', 'Expiry Period Recording'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPlanOverrideHistories()
    {
        return $this->hasMany(CompanyPlanOverrideHistory::className(), ['plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDurations()
    {
        return $this->hasMany(PlanDuration::className(), ['plan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanForCallCenter()
    {
        return $this->hasOne(PlanForCallCenter::className(), ['plan_id' => 'id']);
    }
}
