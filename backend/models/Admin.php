<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property int $id
 * @property int $company_id
 * @property int $role_id
 * @property int $timezone_id
 * @property string $name
 * @property string $login_name Portal login
 * @property string $password md5 hash
 * @property int $admin_id
 * @property string $created_on
 * @property string $last_login
 * @property int $status 0-Inactive,1-Active,2-Deleted
 *
 * @property Company $company
 * @property Role $role
 * @property Timezone $timezone
 * @property Admin $admin
 * @property Admin[] $admins
 * @property CallReason[] $callReasons
 * @property CompanyEmail[] $companyEmails
 * @property Ringgroup[] $ringgroups
 * @property SipEndpoint[] $sipEndpoints
 * @property SipTrunk[] $sipTrunks
 * @property UserSession[] $userSessions
 * @property Voicemail[] $voicemails
 * @property VoicemailPrompt[] $voicemailPrompts
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'role_id', 'timezone_id', 'name', 'login_name', 'password'], 'required'],
            [['company_id', 'role_id', 'timezone_id', 'admin_id', 'status'], 'integer'],
            [['created_on', 'last_login'], 'safe'],
            [['name', 'login_name', 'password'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['timezone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Timezone::className(), 'targetAttribute' => ['timezone_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'role_id' => Yii::t('app', 'Role ID'),
            'timezone_id' => Yii::t('app', 'Timezone ID'),
            'name' => Yii::t('app', 'Name'),
            'login_name' => Yii::t('app', 'Login Name'),
            'password' => Yii::t('app', 'Password'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'last_login' => Yii::t('app', 'Last Login'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins()
    {
        return $this->hasMany(Admin::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReasons()
    {
        return $this->hasMany(CallReason::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyEmails()
    {
        return $this->hasMany(CompanyEmail::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroups()
    {
        return $this->hasMany(Ringgroup::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoints()
    {
        return $this->hasMany(SipEndpoint::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunks()
    {
        return $this->hasMany(SipTrunk::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSessions()
    {
        return $this->hasMany(UserSession::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemails()
    {
        return $this->hasMany(Voicemail::className(), ['admin_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailPrompts()
    {
        return $this->hasMany(VoicemailPrompt::className(), ['admin_id' => 'id']);
    }
}
