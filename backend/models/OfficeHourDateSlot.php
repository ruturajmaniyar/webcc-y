<?php

namespace backend\models;

use Yii;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "office_hour_date_slot".
 *
 * @property int $office_hour_id
 * @property string $start_date
 * @property string $end_date Default means no end date
 *
 * @property OfficeHour $officeHour
 */
class OfficeHourDateSlot extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_hour_date_slot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date','end_date'], 'required'],
            [['office_hour_id'], 'integer'],
            [['start_date', 'end_date'], 'date','format' => 'php:Y-m-d'],
            [['start_date', 'end_date'], 'safe'],
            ['start_date', 'compare', 'compareAttribute' => 'end_date', 'operator' => '<', 'enableClientValidation' => false],
            [['office_hour_id', 'start_date'], 'unique', 'targetAttribute' => ['office_hour_id', 'start_date']],
            [['office_hour_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeHour::className(), 'targetAttribute' => ['office_hour_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'office_hour_id' => Yii::t('app', 'Office Hour ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHour()
    {
        return $this->hasOne(OfficeHour::className(), ['id' => 'office_hour_id']);
    }

    public function validateDate()
    {
        if (strtotime($this->end_date) <= strtotime($this->start_date)) {
            $this->addError('start_date', 'Start Date must be less than End Date');
            $this->addError('end_date', 'End Date must be grater than Start Date');
        }
    }
}
