<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ringgroup".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $call_type 0-Ringall,1-Sequential
 * @property int $no_answer_action_id allow only Voicemail(5) and HangUp(1)
 * @property int $no_answer_destination_id Ponts to respective table of Call action
 * @property int $call_recording plan overrides this option
 * @property int $record_after_connect when to start recording if enabled
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property Company $company
 * @property Admin $admin
 * @property RinggroupCall[] $ringgroupCalls
 * @property RinggroupMember[] $ringgroupMembers
 * @property SipEndpoint[] $sipEndpoints
 */
class Ringgroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ringgroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'admin_id'], 'required'],
            [
                [
                    'company_id',
                    'call_type',
                    'no_answer_action_id',
                    'call_recording',
                    'record_after_connect',
                    'admin_id',
                    'is_active'
                ],
                'integer'
            ],
            [
                'no_answer_destination_id',
                'required',
                'when' => function ($model) {
                    return $model->no_answer_action_id === '2';
                },
                'whenClient' => "function (attribute, value) {
                    return $('#sipendpoint-no_answer_action_id').val() === '2';
                }"
            ],
            [['created_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['company_id', 'name'], 'unique', 'targetAttribute' => ['company_id', 'name']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'name' => Yii::t('app', 'Name'),
            'call_type' => Yii::t('app', 'Call Type'),
            'no_answer_action_id' => Yii::t('app', 'No Answer Action'),
            'no_answer_destination_id' => Yii::t('app', 'No Answer Destination'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'record_after_connect' => Yii::t('app', 'Record After Connect'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCalls()
    {
        return $this->hasMany(RinggroupCall::className(), ['ringgroup_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupMembers()
    {
        return $this->hasMany(RinggroupMember::className(), ['ringgroup_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoints()
    {
        return $this->hasMany(SipEndpoint::className(), ['id' => 'sip_endpoint_id'])->viaTable('ringgroup_member', ['ringgroup_id' => 'id']);
    }
}
