<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_extra_field".
 *
 * @property int $company_id
 * @property string $field_name
 * @property string $field_value
 *
 * @property Company $company
 */
class CompanyExtraField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_extra_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'field_name', 'field_value'], 'required'],
            [['company_id'], 'integer'],
            [['field_name', 'field_value'], 'string', 'max' => 255],
            [['company_id', 'field_name'], 'unique', 'targetAttribute' => ['company_id', 'field_name']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'field_name' => Yii::t('app', 'Field Name'),
            'field_value' => Yii::t('app', 'Field Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
