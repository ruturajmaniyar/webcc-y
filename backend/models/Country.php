<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $name
 * @property string $country_code 3 characters country code
 * @property string $country_code_2_char 2 characters country code
 * @property string $country_prefix
 * @property int $currency_id What currency to use if customer is from this country
 * @property int $server_id SIP Proxy for country
 * @property int $is_active
 *
 * @property Company[] $companies
 * @property CompanyAllowedCountry[] $companyAllowedCountries
 * @property Company[] $companies0
 * @property CompanyUnconfirmed[] $companyUnconfirmeds
 * @property Currency $currency
 * @property Server $server
 * @property RinggroupCall[] $ringgroupCalls
 * @property SipEndpointCall[] $sipEndpointCalls
 * @property SipTrunkCall[] $sipTrunkCalls
 * @property Timezone[] $timezones
 * @property VoicemailCall[] $voicemailCalls
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_code', 'country_prefix', 'currency_id', 'server_id'], 'required'],
            [['currency_id', 'server_id', 'is_active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['country_code'], 'string', 'max' => 3],
            [['country_code_2_char'], 'string', 'max' => 2],
            [['country_prefix'], 'string', 'max' => 10],
            [['country_code'], 'unique'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['server_id'], 'exist', 'skipOnError' => true, 'targetClass' => Server::className(), 'targetAttribute' => ['server_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'country_code' => Yii::t('app', 'Country Code'),
            'country_code_2_char' => Yii::t('app', 'Country Code 2 Char'),
            'country_prefix' => Yii::t('app', 'Country Prefix'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'server_id' => Yii::t('app', 'Server ID'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAllowedCountries()
    {
        return $this->hasMany(CompanyAllowedCountry::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies0()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_allowed_country', ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUnconfirmeds()
    {
        return $this->hasMany(CompanyUnconfirmed::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(Server::className(), ['id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCalls()
    {
        return $this->hasMany(RinggroupCall::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCalls()
    {
        return $this->hasMany(SipEndpointCall::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunkCalls()
    {
        return $this->hasMany(SipTrunkCall::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezones()
    {
        return $this->hasMany(Timezone::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailCalls()
    {
        return $this->hasMany(VoicemailCall::className(), ['country_id' => 'id']);
    }
}
