<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_subscriber".
 *
 * @property int $sip_endpoint_id
 * @property string $username
 * @property string $domain
 * @property string $password
 *
 * @property SipEndpoint $sipEndpoint
 */
class SipSubscriber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_subscriber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sip_endpoint_id', 'username', 'domain', 'password'], 'required'],
            [['sip_endpoint_id'], 'integer'],
            [['domain'], 'string', 'max' => 64],
            [['password', 'username'], 'string', 'min' => 12, 'max' => 20],
            [['sip_endpoint_id'], 'unique'],
            [['sip_endpoint_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipEndpoint::className(), 'targetAttribute' => ['sip_endpoint_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sip_endpoint_id' => Yii::t('app', 'Sip Endpoint ID'),
            'username' => Yii::t('app', 'Username'),
            'domain' => Yii::t('app', 'Domain'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoint()
    {
        return $this->hasOne(SipEndpoint::className(), ['id' => 'sip_endpoint_id']);
    }
}
