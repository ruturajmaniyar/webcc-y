<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plan_for_call_center".
 *
 * @property int $plan_id
 * @property int $max_queues
 * @property int $max_agents
 * @property int $max_managers
 *
 * @property Plan $plan
 */
class PlanForCallCenter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_for_call_center';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'max_queues', 'max_agents', 'max_managers'], 'required'],
            [['plan_id', 'max_queues', 'max_agents', 'max_managers'], 'integer'],
            [['plan_id'], 'unique'],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => Yii::t('app', 'Plan ID'),
            'max_queues' => Yii::t('app', 'Max Queues'),
            'max_agents' => Yii::t('app', 'Max Agents'),
            'max_managers' => Yii::t('app', 'Max Managers'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'plan_id']);
    }
}
