<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_endpoint_call".
 *
 * @property int $id
 * @property int $call_reason_id
 * @property int $sip_endpoint_id
 * @property resource $caller_ip inet6_aton
 * @property int $country_id
 * @property string $call_reason
 * @property string $caller_number Number without country code and leading 0
 * @property string $caller_name
 * @property string $caller_email
 * @property string $domain
 * @property string $call_start_time
 * @property string $call_connect_time
 * @property string $call_end_time
 *
 * @property CallReason $callReason
 * @property SipEndpoint $sipEndpoint
 * @property Country $country
 * @property SipEndpointCallConnected $sipEndpointCallConnected
 * @property SipEndpointCallFailed $sipEndpointCallFailed
 */
class SipEndpointCall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_endpoint_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['call_reason_id', 'sip_endpoint_id', 'caller_ip', 'country_id', 'call_reason', 'caller_number', 'caller_name', 'caller_email', 'domain'], 'required'],
            [['call_reason_id', 'sip_endpoint_id', 'country_id', 'caller_number'], 'integer'],
            [['call_start_time', 'call_connect_time', 'call_end_time'], 'safe'],
            [['caller_ip'], 'string', 'max' => 16],
            [['call_reason', 'caller_name', 'caller_email', 'domain'], 'string', 'max' => 255],
            [['call_reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => CallReason::className(), 'targetAttribute' => ['call_reason_id' => 'id']],
            [['sip_endpoint_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipEndpoint::className(), 'targetAttribute' => ['sip_endpoint_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'call_reason_id' => Yii::t('app', 'Call Reason ID'),
            'sip_endpoint_id' => Yii::t('app', 'Sip Endpoint ID'),
            'caller_ip' => Yii::t('app', 'Caller Ip'),
            'country_id' => Yii::t('app', 'Country ID'),
            'call_reason' => Yii::t('app', 'Call Reason'),
            'caller_number' => Yii::t('app', 'Caller Number'),
            'caller_name' => Yii::t('app', 'Caller Name'),
            'caller_email' => Yii::t('app', 'Caller Email'),
            'domain' => Yii::t('app', 'Domain'),
            'call_start_time' => Yii::t('app', 'Call Start Time'),
            'call_connect_time' => Yii::t('app', 'Call Connect Time'),
            'call_end_time' => Yii::t('app', 'Call End Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReason()
    {
        return $this->hasOne(CallReason::className(), ['id' => 'call_reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoint()
    {
        return $this->hasOne(SipEndpoint::className(), ['id' => 'sip_endpoint_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCallConnected()
    {
        return $this->hasOne(SipEndpointCallConnected::className(), ['sip_endpoint_call_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCallFailed()
    {
        return $this->hasOne(SipEndpointCallFailed::className(), ['sip_endpoint_call_id' => 'id']);
    }
}
