<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_session".
 *
 * @property int $admin_id
 * @property string $login_time
 * @property string $browser
 * @property string $platform
 * @property string $user_agent
 * @property resource $public_ip inet6_aton
 *
 * @property Admin $admin
 */
class UserSession extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id', 'login_time', 'public_ip'], 'required'],
            [['admin_id'], 'integer'],
            [['login_time'], 'safe'],
            [['browser', 'platform', 'user_agent'], 'string', 'max' => 255],
            [['public_ip'], 'string', 'max' => 16],
            [['admin_id', 'login_time'], 'unique', 'targetAttribute' => ['admin_id', 'login_time']],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::className(), 'targetAttribute' => ['admin_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'admin_id' => Yii::t('app', 'Admin ID'),
            'login_time' => Yii::t('app', 'Login Time'),
            'browser' => Yii::t('app', 'Browser'),
            'platform' => Yii::t('app', 'Platform'),
            'user_agent' => Yii::t('app', 'User Agent'),
            'public_ip' => Yii::t('app', 'Public Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }
}
