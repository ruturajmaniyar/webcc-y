<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "office_hour_holiday".
 *
 * @property int $office_hour_id
 * @property string $start_time
 * @property string $end_time
 *
 * @property OfficeHour $officeHour
 */
class OfficeHourHoliday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_hour_holiday';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time'], 'required'],
            [['office_hour_id'], 'integer'],
            [['start_time', 'end_time'], 'datetime','format' => 'php:Y-m-d H:i:s'],
            ['start_time', 'compare', 'compareAttribute' => 'end_time', 'operator' => '<', 'enableClientValidation' => false],
            [['start_time', 'end_time'], 'safe'],
            [['office_hour_id', 'start_time'], 'unique', 'targetAttribute' => ['office_hour_id', 'start_time']],
            [['office_hour_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeHour::className(), 'targetAttribute' => ['office_hour_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'office_hour_id' => Yii::t('app', 'Office Hour ID'),
            'start_time' => Yii::t('app', 'Start Time'),
            'end_time' => Yii::t('app', 'End Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHour()
    {
        return $this->hasOne(OfficeHour::className(), ['id' => 'office_hour_id']);
    }
}
