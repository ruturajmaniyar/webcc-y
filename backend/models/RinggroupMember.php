<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ringgroup_member".
 *
 * @property int $ringgroup_id
 * @property int $sip_endpoint_id
 * @property int $ring_timeout Maxx allowed timeout is 180 seconds
 * @property int $is_active
 * @property int $priority Useful for sequential calling
 *
 * @property Ringgroup $ringgroup
 * @property SipEndpoint $sipEndpoint
 */
class RinggroupMember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ringgroup_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ring_timeout'], 'default', 'value' => 60],
            [['ringgroup_id', 'sip_endpoint_id', 'ring_timeout'], 'required'],
            [['ringgroup_id', 'sip_endpoint_id', 'ring_timeout', 'is_active', 'priority'], 'integer'],
            ['ring_timeout', 'integer', 'max' => 180],
            [['ringgroup_id', 'sip_endpoint_id'], 'unique', 'targetAttribute' => ['ringgroup_id', 'sip_endpoint_id']],
            [
                ['ringgroup_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Ringgroup::className(),
                'targetAttribute' => ['ringgroup_id' => 'id']
            ],
            [
                ['sip_endpoint_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SipEndpoint::className(),
                'targetAttribute' => ['sip_endpoint_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ringgroup_id' => Yii::t('app', 'Ringgroup ID'),
            'sip_endpoint_id' => Yii::t('app', 'Sip Endpoint ID'),
            'ring_timeout' => Yii::t('app', 'Ring Timeout'),
            'is_active' => Yii::t('app', 'Status'),
            'priority' => Yii::t('app', 'Priority'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroup()
    {
        return $this->hasOne(Ringgroup::className(), ['id' => 'ringgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoint()
    {
        return $this->hasOne(SipEndpoint::className(), ['id' => 'sip_endpoint_id']);
    }
}
