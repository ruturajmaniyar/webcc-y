<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $country_id
 * @property string $contact_name
 * @property string $company
 * @property string $email
 * @property string $contact_number without leading 00+Country code
 * @property string $website
 * @property int $timezone_id
 * @property int $plan_duration_id
 * @property string $expires_on
 * @property string $secret_key Key to be used in Widgets
 * @property string $created_on
 * @property int $status 0-Inactive,1-Active,2-Deleted
 *
 * @property Admin[] $admins
 * @property CallReason[] $callReasons
 * @property Timezone $timezone
 * @property Country $country
 * @property PlanDuration $planDuration
 * @property CompanyAllowedCountry[] $companyAllowedCountries
 * @property Country[] $countries
 * @property CompanyBlacklistedIp[] $companyBlacklistedIps
 * @property CompanyCallSetting $companyCallSetting
 * @property CompanyDomain[] $companyDomains
 * @property CompanyEmail[] $companyEmails
 * @property CompanyExtraField[] $companyExtraFields
 * @property CompanyPlanOverrideHistory $companyPlanOverrideHistory
 * @property OfficeHour[] $officeHours
 * @property Ringgroup[] $ringgroups
 * @property SipEndpoint[] $sipEndpoints
 * @property SipTrunk[] $sipTrunks
 * @property Voicemail[] $voicemails
 * @property VoicemailPrompt[] $voicemailPrompts
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'contact_name', 'company', 'email', 'contact_number', 'website', 'timezone_id', 'plan_duration_id', 'secret_key'], 'required'],
            [['country_id', 'contact_number', 'timezone_id', 'plan_duration_id', 'status'], 'integer'],
            [['expires_on', 'created_on'], 'safe'],
            [['contact_name', 'company', 'email', 'website', 'secret_key'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['timezone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Timezone::className(), 'targetAttribute' => ['timezone_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['plan_duration_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanDuration::className(), 'targetAttribute' => ['plan_duration_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'contact_name' => Yii::t('app', 'Contact Name'),
            'company' => Yii::t('app', 'Company'),
            'email' => Yii::t('app', 'Email'),
            'contact_number' => Yii::t('app', 'Contact Number'),
            'website' => Yii::t('app', 'Website'),
            'timezone_id' => Yii::t('app', 'Timezone ID'),
            'plan_duration_id' => Yii::t('app', 'Plan Duration ID'),
            'expires_on' => Yii::t('app', 'Expires On'),
            'secret_key' => Yii::t('app', 'Secret Key'),
            'created_on' => Yii::t('app', 'Created On'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins()
    {
        return $this->hasMany(Admin::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReasons()
    {
        return $this->hasMany(CallReason::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDuration()
    {
        return $this->hasOne(PlanDuration::className(), ['id' => 'plan_duration_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAllowedCountries()
    {
        return $this->hasMany(CompanyAllowedCountry::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['id' => 'country_id'])->viaTable('company_allowed_country', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyBlacklistedIps()
    {
        return $this->hasMany(CompanyBlacklistedIp::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCallSetting()
    {
        return $this->hasOne(CompanyCallSetting::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDomains()
    {
        return $this->hasMany(CompanyDomain::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyEmails()
    {
        return $this->hasMany(CompanyEmail::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyExtraFields()
    {
        return $this->hasMany(CompanyExtraField::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPlanOverrideHistory()
    {
        return $this->hasOne(CompanyPlanOverrideHistory::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHours()
    {
        return $this->hasMany(OfficeHour::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroups()
    {
        return $this->hasMany(Ringgroup::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpoints()
    {
        return $this->hasMany(SipEndpoint::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunks()
    {
        return $this->hasMany(SipTrunk::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemails()
    {
        return $this->hasMany(Voicemail::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoicemailPrompts()
    {
        return $this->hasMany(VoicemailPrompt::className(), ['company_id' => 'id']);
    }
}
