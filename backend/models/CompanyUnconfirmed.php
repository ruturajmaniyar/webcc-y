<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company_unconfirmed".
 *
 * @property int $plan_duration_id
 * @property int $country_id
 * @property string $contact_name
 * @property string $company
 * @property string $email
 * @property string $contact_number without leading 00+Country code
 * @property string $website
 * @property int $timezone_id
 * @property string $confirmation_token
 * @property string $created_on
 *
 * @property PlanDuration $planDuration
 * @property Country $country
 */
class CompanyUnconfirmed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_unconfirmed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_duration_id', 'country_id', 'contact_name', 'company', 'email', 'contact_number', 'website', 'timezone_id', 'confirmation_token'], 'required'],
            [['plan_duration_id', 'country_id', 'contact_number', 'timezone_id'], 'integer'],
            [['created_on'], 'safe'],
            [['contact_name', 'company', 'email', 'website', 'confirmation_token'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['plan_duration_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanDuration::className(), 'targetAttribute' => ['plan_duration_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_duration_id' => Yii::t('app', 'Plan Duration ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'contact_name' => Yii::t('app', 'Contact Name'),
            'company' => Yii::t('app', 'Company'),
            'email' => Yii::t('app', 'Email'),
            'contact_number' => Yii::t('app', 'Contact Number'),
            'website' => Yii::t('app', 'Website'),
            'timezone_id' => Yii::t('app', 'Timezone ID'),
            'confirmation_token' => Yii::t('app', 'Confirmation Token'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDuration()
    {
        return $this->hasOne(PlanDuration::className(), ['id' => 'plan_duration_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
}
