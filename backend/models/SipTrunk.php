<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_trunk".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property resource $trunk_ip inet6_aton
 * @property resource $backup_trunk_ip inet6_aton
 * @property int $no_answer_action_id allow only Voicemail(5) and HangUp(1)
 * @property int $no_answer_destination_id Ponts to respective table of Call action
 * @property int $call_recording plan overrides this option
 * @property int $record_after_connect when to start recording if enabled
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property Company $company
 * @property Admin $admin
 * @property SipTrunkCall[] $sipTrunkCalls
 */
class SipTrunk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_trunk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'trunk_ip', 'admin_id', 'no_answer_action_id', 'call_recording'], 'required'],
            [
                [
                    'company_id',
                    'no_answer_action_id',
                    'call_recording',
                    'record_after_connect',
                    'admin_id',
                    'is_active'
                ],
                'integer'
            ],
            ['is_active', 'default', 'value' => 1],
            ['call_recording', 'default', 'value' => 0],
            [
                'no_answer_destination_id',
                'required',
                'when' => function ($model) {
                    return $model->no_answer_action_id === '2';
                },
                'whenClient' => "function (attribute, value) {
                    return $('#siptrunk-no_answer_action_id').val() === '2';
                }"
            ],
            [['created_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['trunk_ip', 'backup_trunk_ip'], 'string', 'max' => 16],
            [['trunk_ip', 'backup_trunk_ip'], 'ip'],
            [['company_id', 'name'], 'unique', 'targetAttribute' => ['company_id', 'name']],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['admin_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Admin::className(),
                'targetAttribute' => ['admin_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'name' => Yii::t('app', 'Name'),
            'trunk_ip' => Yii::t('app', 'Trunk Ip'),
            'backup_trunk_ip' => Yii::t('app', 'Failover Trunk IP'),
            'no_answer_action_id' => Yii::t('app', 'No Answer Action'),
            'no_answer_destination_id' => Yii::t('app', 'No Answer Destination'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'record_after_connect' => Yii::t('app', 'Record After Connect'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunkCalls()
    {
        return $this->hasMany(SipTrunkCall::className(), ['sip_trunk_id' => 'id']);
    }
}
