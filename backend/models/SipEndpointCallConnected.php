<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_endpoint_call_connected".
 *
 * @property int $sip_endpoint_call_id
 * @property string $call_recording timestamp_subscriberid_caller.wav
 * @property string $recording_upload_time
 *
 * @property SipEndpointCall $sipEndpointCall
 */
class SipEndpointCallConnected extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_endpoint_call_connected';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sip_endpoint_call_id'], 'required'],
            [['sip_endpoint_call_id'], 'integer'],
            [['recording_upload_time'], 'safe'],
            [['call_recording'], 'string', 'max' => 255],
            [['sip_endpoint_call_id'], 'unique'],
            [['sip_endpoint_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipEndpointCall::className(), 'targetAttribute' => ['sip_endpoint_call_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sip_endpoint_call_id' => Yii::t('app', 'Sip Endpoint Call ID'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'recording_upload_time' => Yii::t('app', 'Recording Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCall()
    {
        return $this->hasOne(SipEndpointCall::className(), ['id' => 'sip_endpoint_call_id']);
    }
}
