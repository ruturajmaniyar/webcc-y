<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "server".
 *
 * @property int $id
 * @property string $name
 * @property int $server_type 1-SIP Proxy,2-Media Server,3-Web Server
 * @property int $ip inet_aton
 * @property string $description
 * @property int $parent_server_id To map SIP proxy with Media Server
 * @property int $is_active
 *
 * @property Country[] $countries
 */
class Server extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ip'], 'required'],
            [['server_type', 'ip', 'parent_server_id', 'is_active'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['server_type', 'ip'], 'unique', 'targetAttribute' => ['server_type', 'ip']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'server_type' => Yii::t('app', 'Server Type'),
            'ip' => Yii::t('app', 'Ip'),
            'description' => Yii::t('app', 'Description'),
            'parent_server_id' => Yii::t('app', 'Parent Server ID'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['server_id' => 'id']);
    }
}
