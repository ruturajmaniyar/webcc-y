<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ringgroup_call_failed".
 *
 * @property int $ringgroup_call_id
 * @property int $action_id
 * @property int $destination_id Voicemail id
 * @property string $voicemail timestamp_voicemailid_caller_vm.wav
 * @property string $voicemail_upload_time
 *
 * @property RinggroupCall $ringgroupCall
 */
class RinggroupCallFailed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ringgroup_call_failed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ringgroup_call_id', 'action_id'], 'required'],
            [['ringgroup_call_id', 'action_id', 'destination_id'], 'integer'],
            [['voicemail_upload_time'], 'safe'],
            [['voicemail'], 'string', 'max' => 255],
            [['ringgroup_call_id'], 'unique'],
            [['ringgroup_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => RinggroupCall::className(), 'targetAttribute' => ['ringgroup_call_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ringgroup_call_id' => Yii::t('app', 'Ringgroup Call ID'),
            'action_id' => Yii::t('app', 'Action ID'),
            'destination_id' => Yii::t('app', 'Destination ID'),
            'voicemail' => Yii::t('app', 'Voicemail'),
            'voicemail_upload_time' => Yii::t('app', 'Voicemail Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCall()
    {
        return $this->hasOne(RinggroupCall::className(), ['id' => 'ringgroup_call_id']);
    }
}
