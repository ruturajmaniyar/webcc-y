<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plan_duration_currency".
 *
 * @property int $plan_duration_id
 * @property int $currency_id
 * @property int $charge
 * @property int $is_active
 *
 * @property PlanDuration $planDuration
 * @property Currency $currency
 */
class PlanDurationCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_duration_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_duration_id', 'currency_id', 'charge'], 'required'],
            [['plan_duration_id', 'currency_id', 'charge', 'is_active'], 'integer'],
            [['plan_duration_id', 'currency_id'], 'unique', 'targetAttribute' => ['plan_duration_id', 'currency_id']],
            [['plan_duration_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanDuration::className(), 'targetAttribute' => ['plan_duration_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_duration_id' => Yii::t('app', 'Plan Duration ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'charge' => Yii::t('app', 'Charge'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDuration()
    {
        return $this->hasOne(PlanDuration::className(), ['id' => 'plan_duration_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}
