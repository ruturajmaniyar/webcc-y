<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "office_hour".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $timezone_id
 * @property int $is_active
 *
 * @property CallReasonDestination[] $callReasonDestinations
 * @property Company $company
 * @property Timezone $timezone
 * @property OfficeHourDateSlot[] $officeHourDateSlots
 * @property OfficeHourHoliday[] $officeHourHolidays
 * @property OfficeHourTimeSlot[] $officeHourTimeSlots
 */
class OfficeHour extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_hour';
    }

    public static function getOfficeHourList($company_id = null)
    {
        if($company_id == null){
            $company_id = Yii::$app->user->identity->company_id;
        }
        return static::findAll(['company_id' => $company_id]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name'], 'required'],
            [['company_id', 'timezone_id', 'is_active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['company_id', 'name'], 'unique', 'targetAttribute' => ['company_id', 'name']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['timezone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Timezone::className(), 'targetAttribute' => ['timezone_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'name' => Yii::t('app', 'Name'),
            'timezone_id' => Yii::t('app', 'Timezone'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallReasonDestinations()
    {
        return $this->hasMany(CallReasonDestination::className(), ['office_hour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimezone()
    {
        return $this->hasOne(Timezone::className(), ['id' => 'timezone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHourDateSlots()
    {
        return $this->hasMany(OfficeHourDateSlot::className(), ['office_hour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHourHolidays()
    {
        return $this->hasMany(OfficeHourHoliday::className(), ['office_hour_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeHourTimeSlots()
    {
        return $this->hasMany(OfficeHourTimeSlot::className(), ['office_hour_id' => 'id']);
    }
}
