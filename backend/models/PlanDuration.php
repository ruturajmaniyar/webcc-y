<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "plan_duration".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $duration in Days, 30,180,360 Days
 * @property int $plan_discount in %
 * @property string $description
 * @property int $is_active
 *
 * @property Company[] $companies
 * @property CompanyUnconfirmed[] $companyUnconfirmeds
 * @property Plan $plan
 * @property PlanDurationCurrency[] $planDurationCurrencies
 * @property Currency[] $currencies
 */
class PlanDuration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan_duration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'duration', 'description'], 'required'],
            [['plan_id', 'duration', 'plan_discount', 'is_active'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'duration' => Yii::t('app', 'Duration'),
            'plan_discount' => Yii::t('app', 'Plan Discount'),
            'description' => Yii::t('app', 'Description'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['plan_duration_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUnconfirmeds()
    {
        return $this->hasMany(CompanyUnconfirmed::className(), ['plan_duration_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDurationCurrencies()
    {
        return $this->hasMany(PlanDurationCurrency::className(), ['plan_duration_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['id' => 'currency_id'])->viaTable('plan_duration_currency', ['plan_duration_id' => 'id']);
    }
}
