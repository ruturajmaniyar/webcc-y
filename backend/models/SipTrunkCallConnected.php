<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_trunk_call_connected".
 *
 * @property int $sip_trunk_call_id
 * @property string $call_recording timestamp_subscriberid_caller.wav
 * @property string $recording_upload_time
 *
 * @property SipTrunkCall $sipTrunkCall
 */
class SipTrunkCallConnected extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_trunk_call_connected';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sip_trunk_call_id'], 'required'],
            [['sip_trunk_call_id'], 'integer'],
            [['recording_upload_time'], 'safe'],
            [['call_recording'], 'string', 'max' => 255],
            [['sip_trunk_call_id'], 'unique'],
            [['sip_trunk_call_id'], 'exist', 'skipOnError' => true, 'targetClass' => SipTrunkCall::className(), 'targetAttribute' => ['sip_trunk_call_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sip_trunk_call_id' => Yii::t('app', 'Sip Trunk Call ID'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'recording_upload_time' => Yii::t('app', 'Recording Upload Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipTrunkCall()
    {
        return $this->hasOne(SipTrunkCall::className(), ['id' => 'sip_trunk_call_id']);
    }
}
