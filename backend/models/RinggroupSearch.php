<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RinggroupSearch represents the model behind the search form of `backend\models\Ringgroup`.
 */
class RinggroupSearch extends Ringgroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'company_id',
                    'call_type',
                    'no_answer_action_id',
                    'no_answer_destination_id',
                    'call_recording',
                    'record_after_connect',
                    'admin_id',
                    'is_active'
                ],
                'integer'
            ],
            [['name', 'created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ringgroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => ['pageSize' => Yii::$app->helper->get_per_page_record_count()],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'company_id' => Yii::$app->user->identity->company_id,
            'call_type' => $this->call_type,
            'no_answer_action_id' => $this->no_answer_action_id,
            'no_answer_destination_id' => $this->no_answer_destination_id,
            'call_recording' => $this->call_recording,
            'record_after_connect' => $this->record_after_connect,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
