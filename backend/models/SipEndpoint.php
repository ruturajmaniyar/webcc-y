<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sip_endpoint".
 *
 * @property int $id
 * @property int $company_id
 * @property string $given_name User defined name for sip end point
 * @property int $no_answer_action_id allow only Voicemail(5) and HangUp(1)
 * @property int $no_answer_destination_id Points to respective table of Call action
 * @property int $call_recording plan overrides this option
 * @property int $record_after_connect when to start recording if enabled
 * @property int $admin_id
 * @property string $created_on
 * @property int $is_active
 *
 * @property RinggroupCallConnected[] $ringgroupCallConnecteds
 * @property RinggroupMember[] $ringgroupMembers
 * @property Ringgroup[] $ringgroups
 * @property RinggroupMemberCallFailed[] $ringgroupMemberCallFaileds
 * @property RinggroupCall[] $ringgroupCalls
 * @property Company $company
 * @property Admin $admin
 * @property SipEndpointCall[] $sipEndpointCalls
 * @property SipSubscriber $sipSubscriber
 */
class SipEndpoint extends \yii\db\ActiveRecord
{

    /**
     * @var
     */
    public $domain;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sip_endpoint';
    }

    public static function getSipEndpointList($company_id)
    {
        return static::findAll(['company_id' => $company_id, 'is_active' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'admin_id'], 'required'],
            [
                [
                    'company_id',
                    'no_answer_action_id',
                    'call_recording',
                    'record_after_connect',
                    'admin_id',
                    'is_active'
                ],
                'integer'
            ],
            [
                'no_answer_destination_id',
                'required',
                'when' => function ($model) {
                    return $model->no_answer_action_id === '2';
                },
                'whenClient' => "function (attribute, value) {
                    return $('#sipendpoint-no_answer_action_id').val() === '2';
                }"
            ],
            [['created_on'], 'safe'],
            [['given_name'], 'string', 'max' => 255],
            [
                ['company_id', 'given_name'],
                'unique',
                'targetAttribute' => ['company_id', 'given_name'],
                'message' => Yii::t('app', 'Custom Name has already been taken.')
            ],
            [
                ['company_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id']
            ],
            [
                ['admin_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Admin::className(),
                'targetAttribute' => ['admin_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'given_name' => Yii::t('app', 'Custom Name'),
            'no_answer_action_id' => Yii::t('app', 'No Answer Action'),
            'no_answer_destination_id' => Yii::t('app', 'No Answer Destination'),
            'call_recording' => Yii::t('app', 'Call Recording'),
            'record_after_connect' => Yii::t('app', 'Record After Connect'),
            'admin_id' => Yii::t('app', 'Admin ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCallConnecteds()
    {
        return $this->hasMany(RinggroupCallConnected::className(), ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupMembers()
    {
        return $this->hasMany(RinggroupMember::className(), ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroups()
    {
        return $this->hasMany(Ringgroup::className(), ['id' => 'ringgroup_id'])->viaTable('ringgroup_member',
            ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupMemberCallFaileds()
    {
        return $this->hasMany(RinggroupMemberCallFailed::className(), ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRinggroupCalls()
    {
        return $this->hasMany(RinggroupCall::className(),
            ['id' => 'ringgroup_call_id'])->viaTable('ringgroup_member_call_failed', ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipEndpointCalls()
    {
        return $this->hasMany(SipEndpointCall::className(), ['sip_endpoint_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSipSubscriber()
    {
        return $this->hasOne(SipSubscriber::className(), ['sip_endpoint_id' => 'id']);
    }
}
