<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $symbol
 * @property int $is_active
 *
 * @property CompanyPlanOverrideHistory[] $companyPlanOverrideHistories
 * @property Country[] $countries
 * @property CurrencyConversion[] $currencyConversions
 * @property CurrencyConversion[] $currencyConversions0
 * @property Currency[] $toCurrencies
 * @property Currency[] $fromCurrencies
 * @property PlanDurationCurrency[] $planDurationCurrencies
 * @property PlanDuration[] $planDurations
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'symbol'], 'required'],
            [['is_active'], 'integer'],
            [['name', 'code'], 'string', 'max' => 100],
            [['symbol'], 'string', 'max' => 15],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'symbol' => Yii::t('app', 'Symbol'),
            'is_active' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPlanOverrideHistories()
    {
        return $this->hasMany(CompanyPlanOverrideHistory::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyConversions()
    {
        return $this->hasMany(CurrencyConversion::className(), ['from_currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyConversions0()
    {
        return $this->hasMany(CurrencyConversion::className(), ['to_currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCurrencies()
    {
        return $this->hasMany(Currency::className(), ['id' => 'to_currency_id'])->viaTable('currency_conversion', ['from_currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCurrencies()
    {
        return $this->hasMany(Currency::className(), ['id' => 'from_currency_id'])->viaTable('currency_conversion', ['to_currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDurationCurrencies()
    {
        return $this->hasMany(PlanDurationCurrency::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanDurations()
    {
        return $this->hasMany(PlanDuration::className(), ['id' => 'plan_duration_id'])->viaTable('plan_duration_currency', ['currency_id' => 'id']);
    }
}
