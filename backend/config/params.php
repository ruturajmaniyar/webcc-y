<?php
return [
    'adminEmail' => 'admin@example.com',
    'status' => [1 => 'Active', 0 => 'Inactive'],
    'boolean' => [1 => 'True', 0 => 'False'],
    'noActionId' => [1 => 'Disconnect', 2 => 'Voicemail'],
    'ringGroupCallType' => [0 => 'Ring Simultaneously', 1 => 'Ring Sequentially'],
    'defaultPageSize' => 5,
    'recordsPerPage' => [5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100],
];
