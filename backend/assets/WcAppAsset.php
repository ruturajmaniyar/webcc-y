<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class WcAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-reset.css',
        'css/animate.css',
        'library/font-awesome/css/font-awesome.css',
        'library/ionicon/css/ionicons.min.css',
        'library/select2/select2.css',
        'css/style.css',
        'css/helper.css',
        'css/style-responsive.css',
        'css/modules/common.css',
    ];
    public $js = [
        'js/jquery-ui-1.10.1.custom.min.js',
        'js/pace.min.js',
        'js/wow.min.js',
        'js/jquery.nicescroll.js',
        'js/jquery.app.js',
        'library/select2/select2.min.js',
        'js/webcc.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
