<?php

namespace backend\controllers;

use backend\models\Country;
use Yii;
use backend\models\CompanyAllowedCountry;
use backend\models\CompanyAllowedCountrySearch;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanyAllowedCountryController implements the CRUD actions for CompanyAllowedCountry model.
 */
class CompanyAllowedCountryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyAllowedCountry models.
     * @return mixed
     * @throws Exception
     */
    public function actionIndex()
    {
        /** @var CompanyAllowedCountry $model */
        $model = new CompanyAllowedCountry();
        /** @var Country $countryModel */
        $countryModel = Country::find()->all();
        /** @var ArrayHelper $countryList */
        $countryList = ArrayHelper::map((array)$countryModel, 'id', 'name');
        /** @var CompanyAllowedCountry $companyAllowedCountryModel */
        $companyAllowedCountryModel = CompanyAllowedCountry::find()->all();
        /** @var ArrayHelper $companyAllowedCountryList */
        $companyAllowedCountryList = ArrayHelper::map((array)$companyAllowedCountryModel, 'country_id', 'country_id');

        if(Yii::$app->request->post()){
            $requestData = Yii::$app->request->post('CompanyAllowedCountry');
            $transaction = Yii::$app->db->beginTransaction();

            try{
                // Delete old entry
                CompanyAllowedCountry::deleteAll(['company_id' => Yii::$app->user->identity->company_id]);

                if(!empty($requestData['country_id'])){

                    foreach ($requestData['country_id'] as $countryId){

                        $model = new CompanyAllowedCountry();
                        $model->company_id = Yii::$app->user->identity->company_id;
                        $model->country_id = $countryId;

                        $model->save();
                    }
                }

                $transaction->commit();
                Yii::$app->session->setFlash('success', 'Change successfully applied.');
                return $this->redirect(['index']);

            }catch (Exception $e){
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Something went wrong, Please try again.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('index', [
            'model' => $model,
            'countryModel' => $countryModel,
            'countryList' => $countryList,
            'companyAllowedCountryModel' => $companyAllowedCountryModel,
            'companyAllowedCountryList' => $companyAllowedCountryList,
        ]);
    }

    /**
     * Finds the CompanyAllowedCountry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $company_id
     * @param integer $country_id
     * @return CompanyAllowedCountry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($company_id, $country_id)
    {
        if (($model = CompanyAllowedCountry::findOne([
                'company_id' => $company_id,
                'country_id' => $country_id
            ])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
