<?php

namespace backend\controllers;

use backend\models\UserSession;
use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;
use backend\models\CompanyBlacklistedIp;
use yii\helpers\Url;

/**
 * Site controller
 */
class CompanyBlacklistedIpController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $allBlacklistedIp = CompanyBlacklistedIp::findAll(['company_id' => Yii::$app->user->identity->company_id]);
        $requestData = Yii::$app->request->post();
        $blacklistedIpError = [];
        if($requestData){
            $transaction = Yii::$app->db->beginTransaction();
            try{
                if(isset($requestData['ip']) && isset($requestData['netmask']) && isset($requestData['duration'])){
                    CompanyBlacklistedIp::deleteAll(['company_id' => Yii::$app->user->identity->company_id]);
                    $result = $this->saveBlacklistedIp(true);
                    if(isset($result['error']) && $result['success'] == false){
                        $allBlacklistedIp = $this->getPostData();
                        $blacklistedIpError = $result['error'];
                        $transaction->rollback();
                        return $this->render('index',[
                            'allBlacklistedIp' => $allBlacklistedIp,
                            'blacklistedIpError' => $blacklistedIpError,
                        ]);
                    }else{
                        $save = $this->saveBlacklistedIp(false);
                        if($save['success'] == true){
                            $transaction->commit();
                            Yii::$app->session->setFlash('success','Records Saved Successfully');
                            return $this->redirect(Url::current());
                        }else{
                            $allBlacklistedIp = $this->getPostData();
                            $blacklistedIpError = $save['error'];
                            $transaction->rollback();
                            return $this->render('index',[
                                'allBlacklistedIp' => $allBlacklistedIp,
                                'blacklistedIpError' => $blacklistedIpError,
                        ]);
                        }
                    }
                }else{
                    CompanyBlacklistedIp::deleteAll(['company_id' => Yii::$app->user->identity->company_id]);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success','Records Saved Successfully');
                    return $this->redirect(Url::current());
                }
                }catch(Exception $exception){
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error','Something went wrong, Please try again!');
                    return $this->redirect(Url::current());
                }
        }
        return $this->render('index',[
                'allBlacklistedIp' => $allBlacklistedIp,
                'blacklistedIpError' => $blacklistedIpError,
            ]);
    }

    /**
     * @param $validate
     * @return array
     */
    private function saveBlacklistedIp($validate)
    {
        $result = [];
        $requestData = Yii::$app->request->post();
        $ip = $requestData['ip'];
        $netmask = $requestData['netmask'];
        $duration = $requestData['duration'];
        $count = count($ip);
        for($i = 0; $i<$count; $i++){
            /** @var CompanyBlacklistedIp $blacklistedIp */
            $blacklistedIp = new CompanyBlacklistedIp();
            $blacklistedIp->ip = $ip[$i];
            $blacklistedIp->netmask = $netmask[$i];
            $blacklistedIp->duration = $duration[$i];
            $blacklistedIp->company_id = Yii::$app->user->identity->company_id;
            if($validate){
                if(!$blacklistedIp->validate()){
                    $result['error'][$i] = $blacklistedIp->errors;
                }    
            }else{
                if(!$blacklistedIp->save()){
                    $result['error'][$i] = $blacklistedIp->errors;
                }
            }
            unset($blacklistedIp);
        }
        $result['success'] = isset($result['error']) ? false : true;
        return $result;
    }

    private function getPostData()
    {
        $requestData = Yii::$app->request->post();
        $ip = $requestData['ip'];
        $netmask = $requestData['netmask'];
        $duration = $requestData['duration'];
        $count = count($ip);
        $data = [];
        for($i =0; $i<$count;$i++){
            $data[$i]['ip'] = $ip[$i];
            $data[$i]['netmask'] = $netmask[$i];
            $data[$i]['duration'] = $duration[$i];
        }
        return $data;
    }
}
