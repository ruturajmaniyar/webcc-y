<?php

namespace backend\controllers;

use backend\models\UserSession;
use Yii;
use backend\models\Admin;
use backend\models\AdminSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['dashboard', 'profile', 'update', 'changePassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    /**
     * Lists all Admin models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admin model.
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProfile()
    {
        $userSessionDataProvider = new ActiveDataProvider([
            'query' => UserSession::find()->andFilterWhere(['admin_id' => empty(Yii::$app->user->identity->admin_id) ? Yii::$app->user->id : Yii::$app->user->identity->admin_id])->limit(10),
            'pagination' => false,
        ]);
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
            'userSessionModel' => $userSessionDataProvider
        ]);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Profile Update Successfully.');
            return $this->redirect(['profile']);
        }

        $userSessionDataProvider = new ActiveDataProvider([
            'query' => UserSession::find()->andFilterWhere(['admin_id' => empty(Yii::$app->user->identity->admin_id) ? Yii::$app->user->id : Yii::$app->user->identity->admin_id])->limit(10),
            'pagination' => false,
        ]);

        return $this->render('view', [
            'model' => $model,
            'userSessionModel' => $userSessionDataProvider
        ]);
    }
}
