<?php

namespace backend\controllers;

use backend\models\CompanyCallSetting;
use backend\models\CompanyCallSettingSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CompanyCallSettingController implements the CRUD actions for CompanyCallSetting model.
 */
class CompanyCallSettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'change-daily-call-ip', 'change-concurrent-call-ip'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompanyCallSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyCallSettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyCallSetting model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the CompanyCallSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyCallSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyCallSetting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Creates a new CompanyCallSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompanyCallSetting();

        if ($model->load(Yii::$app->request->post())) {
            $model->company_id = Yii::$app->user->identity->company_id;
            if($model->save()){
                Yii::$app->session->setFlash('success', Yii::t('app', 'Records successfully inserted.'));
                return $this->redirect(['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Update Daily Call per IP limit
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionChangeDailyCallIp()
    {

        $primaryKey = unserialize(base64_decode(Yii::$app->request->post('pk')));

        $model = $this->findModel($primaryKey);

        $model->daily_call_per_ip = (Yii::$app->request->post('value') === '') ? '0' : Yii::$app->request->post('value');
        if ($model->update(true, ['daily_call_per_ip'])) {
            return json_encode(['success' => true]);
        }
    }

    /**
     * Update Concurrent Call per IP limit
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionChangeConcurrentCallIp()
    {

        $primaryKey = unserialize(base64_decode(Yii::$app->request->post('pk')));

        $model = $this->findModel($primaryKey);

        $model->concurrent_call_per_ip = (Yii::$app->request->post('value') === '') ? '0' : Yii::$app->request->post('value');
        if ($model->update(true, ['concurrent_call_per_ip'])) {
            return json_encode(['success' => true]);
        }
    }
}
