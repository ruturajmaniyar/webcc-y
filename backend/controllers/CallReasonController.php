<?php

namespace backend\controllers;

use backend\models\CallAction;
use backend\models\CallReason;
use backend\models\CallReasonDestination;
use backend\models\CallReasonSearch;
use backend\models\Company;
use backend\models\Ringgroup;
use backend\models\SipEndpoint;
use backend\models\SipTrunk;
use backend\models\Voicemail;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CallReasonController implements the CRUD actions for CallReason model.
 */
class CallReasonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'configuration', 'get-call-destination'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'create') {
            if ($companyModel = Company::findOne([Yii::$app->user->identity->company_id])) {
                /** @var Ringgroup $callReasonCount */
                $callReasonCount = CallReason::find()->where([
                    'company_id' => Yii::$app->user->identity->company_id,
                    'parent_id' => null
                ])->count();

                /** @var int $maxRingGroupCount */
                $maxCallReasonCount = $companyModel->planDuration->plan->max_call_reasons;

                if ($maxCallReasonCount <= $callReasonCount) {
                    Yii::$app->session->setFlash('error', 'Call Reasons limit reached for your Plan.');
                    $this->redirect(['index']);
                    return false;
                }
            };

        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all CallReason models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CallReasonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CallReason model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CallReason();

        $requestData = Yii::$app->request->post('CallReason');
        if ($requestData) {
            $model->company_id = Yii::$app->user->identity->company_id;
            $model->parent_id = null;
            $model->reason = $requestData['reason'];
            $model->description = $requestData['description'];
            $model->admin_id = Yii::$app->user->id;
            $model->created_on = date('Y-d-m H:i:s');
            $model->is_active = 1;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Saved Successfully');
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CallReason model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $requestData = Yii::$app->request->post('CallReason');
        if ($requestData) {
            $model->company_id = Yii::$app->user->identity->company_id;
            $model->parent_id = null;
            $model->reason = $requestData['reason'];
            $model->description = $requestData['description'];
            $model->admin_id = Yii::$app->user->id;
            $model->is_active = $requestData['is_active'];
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Saved Successfully');
                if (Yii::$app->request->post('apply')) {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the CallReason model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CallReason the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CallReason::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Call reason configuration.
     * render 'configuration' page.
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionConfiguration($id)
    {
        $companyModel = Company::findOne([Yii::$app->user->identity->company_id]);
        /** @var int $maxRingGroupCount */
        $maxCallReasonCount = $companyModel->planDuration->plan->max_call_reasons;

        $model = $this->findModel($id);
        $requestData = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

        $callReasonCount = CallReason::getCallReasonCount(Yii::$app->user->identity->company_id, true);

        $remainingCount = $maxCallReasonCount - $callReasonCount;

        $callReasonMembers = CallReason::findAll(['parent_id' => $id]);
        $callActionMembers = CallReasonDestination::findOne(['call_reason_id' => $id]);
        $callReasonErrors = [];

        $allReasonData = $this->buildTree($id);
        
        if ($requestData) {
            try {
                $model->reason = $requestData['CallReason']['reason'];
                if ($model->save()) {
                    CallReason::deleteAll(['parent_id' => $id]);
                    CallReasonDestination::deleteAll(['call_reason_id' => $id]);
                    $callResult = $this->saveCallReason($model, true);
                    if ((isset($callResult['error']) && $callResult['success'] == false)) {
                        $callActionMembers = $this->getCallActionPostData();
                        $callReasonMembers = $this->getSubReasonPostData();
                        $callReasonErrors = isset($callResult['error']) ? $callResult['error'] : [];
                        $error = '';
                        foreach ($callReasonErrors as $value) {
                            $error .= implode($value) . ' | ';
                        }

                        Yii::$app->session->setFlash('error', $error);
                        $transaction->rollBack();
                        return $this->render('configuration', [
                            'model' => $model,
                            'callReasonCount' => $remainingCount,
                            'allReasonData' => $allReasonData,
                            'callReasonMembers' => $callReasonMembers,
                            'callReasonErrors' => $callReasonErrors,
                            'callActionMembers' => $callActionMembers,
                        ]);
                    } else {
                        $callResult = $this->saveCallReason($model, false);
                        if ($callResult['success'] == true) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', 'Saved Successfully');
                            if (Yii::$app->request->post('apply')) {
                                return $this->redirect(Url::current());
                            }
                            return $this->redirect(['index']);
                        } else {
                            $callActionMembers = $this->getCallActionPostData();
                            $callReasonMembers = $this->getSubReasonPostData();
                            $callReasonErrors = isset($callResult['error']) ? $callResult['error'] : [];
                            $error = '';
                            foreach ($callReasonErrors as $value) {
                                $error .= implode($value) . ' | ';
                            }
                            $transaction->rollback();
                            return $this->render('configuration', [
                                'model' => $model,
                                'callReasonCount' => $remainingCount,
                                'allReasonData' => $allReasonData,
                                'callReasonMembers' => $callReasonMembers,
                                'callReasonErrors' => $callReasonErrors,
                                'callActionMembers' => $callActionMembers,
                            ]);
                        }
                    }
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', 'Something Wrong, Please try again.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('configuration', [
            'model' => $model,
            'callReasonCount' => $remainingCount,
            'allReasonData' => $allReasonData,
            'callReasonMembers' => $callReasonMembers,
            'callReasonErrors' => $callReasonErrors,
            'callActionMembers' => $callActionMembers,
        ]);
    }

    public function buildTree($id)
    {
        $allData = [];
        $parentData = $this->findModel($id)->reason;
        $allData['parent'] = $parentData;
        $callReason = CallReason::find()->where(['parent_id' => $id])->asArray()->all();
        foreach ($callReason as $key => $reason) {
            $allData[] = $this->buildTree($reason['id']);
        }
        return $allData;
    }

    /**
     * @param $model
     * @param $validate
     * @return array
     */
    private function saveCallReason($model, $validate)
    {
        $result = [];
        $result['error'] = [];
        $subReasons = Yii::$app->request->post('reason');

        $officeHourId = Yii::$app->request->post('office_hour_id');
        $officeHourAction = Yii::$app->request->post('office_hours_action_id');
        $officeHourDestination = Yii::$app->request->post('office_hours_destination_id');
        $nonOfficeHourAction = Yii::$app->request->post('non_office_hours_action_id');
        $nonOfficeHourDestination = Yii::$app->request->post('non_office_hours_destination_id');

        $count = is_array($subReasons) && !empty($subReasons) ? count($subReasons) : 0;

        for ($i = 0; $i < $count; $i++) {
            $callReason = new CallReason();
            $callReason->company_id = Yii::$app->user->identity->company_id;
            $callReason->parent_id = $model->id;
            $callReason->reason = $subReasons[$i];
            $callReason->description = '';
            $callReason->admin_id = Yii::$app->user->id;
            $callReason->is_active = 1;
            if ($validate) {
                if (!$callReason->validate()) {
                    $result['error'] += $callReason->errors;
                }
            } else {
                if (!$callReason->save()) {
                    $result['error'] += $callReason->errors;
                }
            }
            unset($callReason);
        }

        if ($officeHourAction == 1) {
            $officeHourDestination = 1;
        }
        if ($nonOfficeHourAction == 1) {
            $nonOfficeHourDestination = 1;
        }
        if (isset($officeHourId) && isset($officeHourAction) && isset($nonOfficeHourAction) && isset($nonOfficeHourDestination)) {
            $callReasonDestination = new CallReasonDestination();
            $callReasonDestination->call_reason_id = $model->id;
            $callReasonDestination->office_hour_id = isset($officeHourId) ? $officeHourId : null;
            $callReasonDestination->office_hours_action_id = isset($officeHourAction) ? $officeHourAction : null;
            $callReasonDestination->office_hours_destination_id = isset($officeHourDestination) ? $officeHourDestination : null;
            $callReasonDestination->non_office_hours_action_id = isset($nonOfficeHourAction) ? $nonOfficeHourAction : null;
            $callReasonDestination->non_office_hours_destination_id = isset($nonOfficeHourDestination) ? $nonOfficeHourDestination : null;
            if ($validate) {
                if (!$callReasonDestination->validate()) {
                    $result['error'] += $callReasonDestination->errors;
                }
            } else {
                if (!$callReasonDestination->save()) {
                    $result['error'] += $callReasonDestination->errors;
                }
            }
        }


        $result['success'] = !empty($result['error']) ? false : true;
        return $result;
    }

    /**
     * @return array
     */
    private function getCallActionPostData()
    {
        $officeHourId = Yii::$app->request->post('office_hour_id');
        $officeHourAction = Yii::$app->request->post('office_hours_action_id');
        $officeHourDestination = Yii::$app->request->post('office_hours_destination_id');
        $nonOfficeHourAction = Yii::$app->request->post('non_office_hours_action_id');
        $nonOfficeHourDestination = Yii::$app->request->post('non_office_hours_destination_id');

        $data = [];
        if (isset($officeHourId) && isset($officeHourAction)) {
            $data['office_hour_id'] = $officeHourId;
            $data['office_hours_action_id'] = $officeHourAction;
            $data['office_hours_destination_id'] = $officeHourDestination;
            $data['non_office_hours_action_id'] = $nonOfficeHourAction;
            $data['non_office_hours_destination_id'] = $nonOfficeHourDestination;

        }
        return $data;
    }

    /**
     * @return array
     */
    private function getSubReasonPostData()
    {
        $subReasons = Yii::$app->request->post('reason');

        $count = count($subReasons);
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            $data[$i]['reason'] = $subReasons[$i];
        }
        return $data;
    }

    /**
     * Deletes an existing CallReason model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetCallDestination($id)
    {
        $action = CallAction::getNameById($id);
        $callDestination = [];
        switch ($action) {
            case 'SIP ENDPOINT':
                $callDestination = ArrayHelper::map(SipEndpoint::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                    'id', 'given_name');
                break;
            case 'SIP TRUNK':
                $callDestination = ArrayHelper::map(SipTrunk::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                    'id', 'name');
                break;
            case 'RING GROUP':
                $callDestination = ArrayHelper::map(Ringgroup::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                    'id', 'name');
                break;
            case 'VOICEMAIL':
                $callDestination = ArrayHelper::map(Voicemail::findAll(['company_id' => Yii::$app->user->identity->company_id]),
                    'id', 'name');
                break;
            case 'QUEUE':
                $callDestination = [];
                break;
        }
        return json_encode($callDestination);
    }
}
