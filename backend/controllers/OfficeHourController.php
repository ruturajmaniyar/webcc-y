<?php

namespace backend\controllers;

use backend\models\Company;
use backend\models\OfficeHour;
use backend\models\OfficeHourDateSlot;
use backend\models\OfficeHourHoliday;
use backend\models\OfficeHourSearch;
use backend\models\OfficeHourTimeSlot;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * OfficeHourController implements the CRUD actions for OfficeHour model.
 */
class OfficeHourController extends Controller
{

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'create') {
            if ($companyModel = Company::findOne([Yii::$app->user->identity->company_id])) {
                /** @var OfficeHour $officeHourCount */
                $officeHourCount = OfficeHour::find()->count();

                /** @var int $maxOfficeHourCount */
                $maxOfficeHourCount = $companyModel->planDuration->plan->max_call_reasons;

                if ($maxOfficeHourCount <= $officeHourCount) {
                    Yii::$app->session->setFlash('error', 'You can add only ' . $maxOfficeHourCount . ' Office Hours.');
                    $this->redirect(['index']);
                    return false;
                }
            };

        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OfficeHour models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OfficeHourSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new OfficeHour model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param null $id
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate($id = null)
    {
        $model = new OfficeHour();
        $dateSlotData = [];
        $timeSlotData = [];
        $holidayData = [];
        $dateErrors = [];
        $timeErrors = [];
        $holidayErrors = [];

        $requestData = Yii::$app->request->post();
        if ($requestData) {
            $transaction = Yii::$app->db->beginTransaction();
            $officeHourData = Yii::$app->request->post('OfficeHour');
            $model->name = $officeHourData['name'];
            $model->timezone_id = $officeHourData['timezone_id'];
            $model->company_id = Yii::$app->user->identity->company_id;
            $model->is_active = 1;
            if ($model->save()) {
                OfficeHourDateSlot::deleteAll(['office_hour_id' => $model->id]);
                OfficeHourTimeSlot::deleteAll(['office_hour_id' => $model->id]);
                OfficeHourHoliday::deleteAll(['office_hour_id' => $model->id]);
                $date = $this->saveDateData($model, true);
                $time = $this->saveTimeData($model, true);
                $holiday = $this->saveHolidayData($model, true);
                if ((isset($date['error']) && $date['success'] == false) || (isset($time['error']) && $time['success'] == false) || (isset($holiday['error']) && $holiday['success'] == false)) {
                    $dateSlotData = $this->getDatePostData();
                    $timeSlotData = $this->getTimePostData();
                    $holidayData = $this->getHolidayPostData();
                    $dateErrors = isset($date['error']) ? $date['error'] : [];
                    $timeErrors = isset($time['error']) ? $time['error'] : [];
                    $holidayErrors = isset($holiday['error']) ? $holiday['error'] : [];
                    $transaction->rollback();
                    return $this->render('create', [
                        'model' => $model,
                        'dateSlotData' => $dateSlotData,
                        'timeSlotData' => $timeSlotData,
                        'holidayData' => $holidayData,
                        'dateErrors' => $dateErrors,
                        'timeErrors' => $timeErrors,
                        'holidayErrors' => $holidayErrors,
                    ]);
                } else {
                    $date = $this->saveDateData($model, false);
                    $time = $this->saveTimeData($model, false);
                    $holiday = $this->saveHolidayData($model, false);
                    if ($date['success'] == true || $time['success'] == true || $holiday['success'] == true) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', 'Saved Successfully');
                        return $this->redirect(['index']);
                    } else {
                        $dateSlotData = $this->getDatePostData();
                        $timeSlotData = $this->getTimePostData();
                        $holidayData = $this->getHolidayPostData();
                        $dateErrors = isset($date['error']) ? $date['error'] : [];
                        $timeErrors = isset($time['error']) ? $time['error'] : [];
                        $holidayErrors = isset($holiday['error']) ? $holiday['error'] : [];
                        $transaction->rollback();
                        return $this->render('create', [
                            'model' => $model,
                            'dateSlotData' => $dateSlotData,
                            'timeSlotData' => $timeSlotData,
                            'holidayData' => $holidayData,
                            'dateErrors' => $dateErrors,
                            'timeErrors' => $timeErrors,
                            'holidayErrors' => $holidayErrors,
                        ]);

                    }
                }
            }
            $dateSlotData = $this->getDatePostData();
            $timeSlotData = $this->getTimePostData();
            $holidayData = $this->getHolidayPostData();
            $transaction->rollback();
        }
        return $this->render('create', [
            'model' => $model,
            'dateSlotData' => $dateSlotData,
            'timeSlotData' => $timeSlotData,
            'holidayData' => $holidayData,
            'dateErrors' => $dateErrors,
            'timeErrors' => $timeErrors,
            'holidayErrors' => $holidayErrors,
        ]);
    }

    /**
     * Save DateSlot Data.
     *
     * @param $model
     * @param $validate
     * @return array
     */
    private function saveDateData($model, $validate)
    {
        $result = [];
        $startDateSlot = Yii::$app->request->post('start_date');
        $endDateSlot = Yii::$app->request->post('end_date');
        if (isset($startDateSlot) && isset($endDateSlot)) {
            for ($i = 0; $i < count($startDateSlot); $i++) {
                $dateSlot = new OfficeHourDateSlot();
                $dateSlot->office_hour_id = $model->id;
                $dateSlot->start_date = $startDateSlot[$i];
                $dateSlot->end_date = $endDateSlot[$i];
                if ($validate) {
                    if (!$dateSlot->validate()) {
                        $result['error'][$i] = $dateSlot->errors;
                    }
                } else {
                    if (!$dateSlot->save()) {
                        $result['error'][$i] = $dateSlot->errors;
                    }
                }
                unset($dateSlot);
            }
        }
        $result['success'] = isset($result['error']) ? false : true;
        return $result;
    }

    /**
     * Save TimeSlot Data.
     *
     * @param $model
     * @param $validate
     * @return array
     */
    private function saveTimeData($model, $validate)
    {
        $result = [];
        $startTimeSlot = Yii::$app->request->post('start_time');
        $endTimeSlot = Yii::$app->request->post('end_time');
        $timeSlotDays = Yii::$app->request->post('run_on_days');
        if (isset($startTimeSlot) && isset($endTimeSlot)) {
            for ($i = 0; $i < count($startTimeSlot); $i++) {
                $timeSlot = new OfficeHourTimeSlot();
                $timeSlot->office_hour_id = $model->id;
                $timeSlot->start_time = Yii::$app->helper->get24HourFormat($startTimeSlot[$i]);
                $timeSlot->end_time = Yii::$app->helper->get24HourFormat($endTimeSlot[$i]);
                $timeSlot->run_on_days = !empty($timeSlotDays[$i]) ? implode(',', $timeSlotDays[$i]) : null;
                if ($validate) {
                    if (!$timeSlot->validate()) {
                        $result['error'][$i] = $timeSlot->errors;
                    }
                } else {
                    if (!$timeSlot->save()) {
                        $result['error'][$i] = $timeSlot->errors;
                    }
                }
                unset($timeSlot);
            }
        }
        $result['success'] = isset($result['error']) ? false : true;
        return $result;
    }

    /**
     * Save Holiday Data.
     *
     * @param $model
     * @param $validate
     * @return array
     */
    private function saveHolidayData($model, $validate)
    {
        $result = [];
        $holidayStartDate = Yii::$app->request->post('holidayStartDate');
        $holidayStartTime = Yii::$app->request->post('holidayStartTime');
        $holidayEndDate = Yii::$app->request->post('holidayEndDate');
        $holidayEndTime = Yii::$app->request->post('holidayEndTime');
        if (isset($holidayStartDate) && isset($holidayStartTime) && isset($holidayEndDate) && isset($holidayEndTime)) {
            for ($i = 0; $i < count($holidayStartDate); $i++) {
                $holiday = new OfficeHourHoliday();
                $holiday->office_hour_id = $model->id;
                $holiday->start_time = Yii::$app->helper->joinDateTime($holidayStartDate[$i], $holidayStartTime[$i]);
                $holiday->end_time = Yii::$app->helper->joinDateTime($holidayEndDate[$i], $holidayEndTime[$i]);
                if ($validate) {
                    if (!$holiday->validate()) {
                        $result['error'][$i] = $holiday->errors;
                    }
                } else {
                    if (!$holiday->save()) {
                        $result['error'][$i] = $holiday->errors;
                    }
                }
                unset($holiday);
            }
        }

        $result['success'] = isset($result['error']) ? false : true;
        return $result;
    }

    private function getDatePostData()
    {
        $startDate = Yii::$app->request->post('start_date');
        $endDate = Yii::$app->request->post('end_date');
        $count = count($startDate);
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            $data[$i]['start_date'] = $startDate[$i];
            $data[$i]['end_date'] = $endDate[$i];
        }
        return $data;
    }

    private function getTimePostData()
    {
        $startTime = Yii::$app->request->post('start_time');
        $endTime = Yii::$app->request->post('end_time');
        $days = Yii::$app->request->post('run_on_days');
        $count = count($startTime);
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            $data[$i]['start_time'] = $startTime[$i];
            $data[$i]['end_time'] = $endTime[$i];
            $data[$i]['run_on_days'] = isset($days[$i]) ? $days[$i] : [];
        }
        return $data;
    }

    private function getHolidayPostData()
    {
        $startDate = Yii::$app->request->post('holidayStartDate');
        $startTime = Yii::$app->request->post('holidayStartTime');
        $endDate = Yii::$app->request->post('holidayEndDate');
        $endTime = Yii::$app->request->post('holidayEndTime');
        $count = count($startDate);
        $data = [];
        for ($i = 0; $i < $count; $i++) {
            $data[$i]['holidayStartDate'] = $startDate[$i];
            $data[$i]['holidayStartTime'] = $startTime[$i];
            $data[$i]['holidayEndDate'] = $endDate[$i];
            $data[$i]['holidayEndTime'] = $endTime[$i];
        }
        return $data;
    }

    /**
     * Updates an existing OfficeHour model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dateSlotData = ArrayHelper::toArray(OfficeHourDateSlot::findAll(['office_hour_id' => $id]));
        $timeSlotData = ArrayHelper::toArray(OfficeHourTimeSlot::findAll(['office_hour_id' => $id]));
        $holidayData = ArrayHelper::toArray(OfficeHourHoliday::findAll(['office_hour_id' => $id]));
        foreach ($holidayData as $key => $value) {
            $holidayStartDate = Yii::$app->helper->splitDateTime($value['start_time']);
            $holidayEndDate = Yii::$app->helper->splitDateTime($value['end_time']);
            $holidayData[$key]['holidayStartDate'] = $holidayStartDate['date'];
            $holidayData[$key]['holidayStartTime'] = $holidayStartDate['time'];
            $holidayData[$key]['holidayEndDate'] = $holidayEndDate['date'];
            $holidayData[$key]['holidayEndTime'] = $holidayEndDate['time'];
        }
        foreach ($timeSlotData as $key => $value){
            $timeSlotData[$key]['start_time'] = Yii::$app->helper->get12HourFormat($value['start_time']);
            $timeSlotData[$key]['end_time'] = Yii::$app->helper->get12HourFormat($value['end_time']);
            $timeSlotData[$key]['run_on_days'] = explode(',',$value['run_on_days']);
        }

        $dateErrors = [];
        $timeErrors = [];
        $holidayErrors = [];

        $requestData = Yii::$app->request->post();
        if ($requestData) {
            $transaction = Yii::$app->db->beginTransaction();
            $officeHourData = Yii::$app->request->post('OfficeHour');
            $model->name = $officeHourData['name'];
            $model->timezone_id = $officeHourData['timezone_id'];
            $model->company_id = Yii::$app->user->identity->company_id;
            $model->is_active = 1;
            if ($model->save()) {
                OfficeHourDateSlot::deleteAll(['office_hour_id' => $model->id]);
                OfficeHourTimeSlot::deleteAll(['office_hour_id' => $model->id]);
                OfficeHourHoliday::deleteAll(['office_hour_id' => $model->id]);
                $date = $this->saveDateData($model, true);
                $time = $this->saveTimeData($model, true);
                $holiday = $this->saveHolidayData($model, true);
                if ((isset($date['error']) && $date['success'] == false) || (isset($time['error']) && $time['success'] == false) || (isset($holiday['error']) && $holiday['success'] == false)) {
                    $dateSlotData = $this->getDatePostData();
                    $timeSlotData = $this->getTimePostData();
                    $holidayData = $this->getHolidayPostData();
                    $dateErrors = isset($date['error']) ? $date['error'] : [];
                    $timeErrors = isset($time['error']) ? $time['error'] : [];
                    $holidayErrors = isset($holiday['error']) ? $holiday['error'] : [];
                    $transaction->rollback();
                    return $this->render('update', [
                        'model' => $model,
                        'dateSlotData' => $dateSlotData,
                        'timeSlotData' => $timeSlotData,
                        'holidayData' => $holidayData,
                        'dateErrors' => $dateErrors,
                        'timeErrors' => $timeErrors,
                        'holidayErrors' => $holidayErrors,
                    ]);
                } else {
                    $date = $this->saveDateData($model, false);
                    $time = $this->saveTimeData($model, false);
                    $holiday = $this->saveHolidayData($model, false);
                    if ($date['success'] == true || $time['success'] == true || $holiday['success'] == true) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', 'Saved Successfully');
                        if (Yii::$app->request->post('apply')) {
                            return $this->redirect(['update', 'id' => $model->id]);
                        }
                        return $this->redirect(['index']);
                    } else {
                        $dateSlotData = $this->getDatePostData();
                        $timeSlotData = $this->getTimePostData();
                        $holidayData = $this->getHolidayPostData();
                        $dateErrors = isset($date['error']) ? $date['error'] : [];
                        $timeErrors = isset($time['error']) ? $time['error'] : [];
                        $holidayErrors = isset($holiday['error']) ? $holiday['error'] : [];
                        $transaction->rollback();
                        return $this->render('update', [
                            'model' => $model,
                            'dateSlotData' => $dateSlotData,
                            'timeSlotData' => $timeSlotData,
                            'holidayData' => $holidayData,
                            'dateErrors' => $dateErrors,
                            'timeErrors' => $timeErrors,
                            'holidayErrors' => $holidayErrors,
                        ]);

                    }
                }
            }
            $dateSlotData = $this->getDatePostData();
            $timeSlotData = $this->getTimePostData();
            $holidayData = $this->getHolidayPostData();
            $transaction->rollback();
        }
        return $this->render('update', [
            'model' => $model,
            'dateSlotData' => $dateSlotData,
            'timeSlotData' => $timeSlotData,
            'holidayData' => $holidayData,
            'dateErrors' => $dateErrors,
            'timeErrors' => $timeErrors,
            'holidayErrors' => $holidayErrors,
        ]);
    }

    /**
     * Finds the OfficeHour model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return OfficeHour the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OfficeHour::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing OfficeHour model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
