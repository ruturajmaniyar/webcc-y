<?php

use app\modules\ecosmob\extensionmaster\assets\ExtensionMasterAsset;
use app\modules\ecosmob\extensionmaster\ExtensionMasterModule;
use app\modules\ecosmob\extensionmaster\models\ExtensionMaster;
use app\modules\ecosmob\tenantmaster\models\DirectoryDomain;
use app\modules\ecosmob\tenantmaster\models\TenantMaster;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\ecosmob\extensionmaster\models\ExtensionMasterSearch */
/* @var $importModel app\modules\ecosmob\extensionmaster\models\ExtensionMaster */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var  $extensionGroupsList */
/* @var  $extensionGroupMapping */

$this->title = ExtensionMasterModule::t('extensionmaster', 'exuser_master');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageHead'] = $this->title;

ExtensionMasterAsset::register($this);
?>

<?php Pjax::begin(['enablePushState' => true, 'id' => 'pjax-extension-master-index']); ?>
<div class="extension-master-index" id="extension-master-index">
    <?php echo $this->render('search/_search', ['model' => $searchModel]); ?>
    <div class="hseparator"></div>
    <?php echo $this->render('form/_import', [
        'importModel' => $importModel,
        'extensionGroupsList' => $extensionGroupsList,
        'extensionGroupMapping' => $extensionGroupMapping,
    ]); ?>
    <div class="hseparator"></div>
    <div class="card">
        <div class="card-header card-custom">
            <h6 class="mb-0 em"><span
                        class="fa fa-anchor"></span><?= ' '
                . ExtensionMasterModule::t(
                    'extensionmaster',
                    'exuser_master'
                ) ?>

                <?php
                if (ExtensionMaster::getExtensionLimit()) {
                    echo Html::a(
                        ExtensionMasterModule::t('extensionmaster', 'create'),
                        ['create'], [
                        'id' => 'hov',
                        'class' => 'btn btn-round-right btn-primary float-xs-right btn-sm hvr-icon-forward   ',
                    ]);
                } ?>
                <?= Html::a(
                    ExtensionMasterModule::t('extensionmaster', 'export'), ['export'], [
                    'id' => 'hov',
                    'class' => 'btn btn-success btn-sm pull-right btn-margin',
                    'data-pjax' => 0,
                ]) ?>
                <label class="btn-primary pull-right mr-1 help-btn-p ">
                    <i class="icon fa fa-question-circle fa-lg" data-toggle="popover"
                       data-trigger="hover" data-placement="left"
                       data-content="<?= ExtensionMasterModule::t('extensionmaster', "user_notes") ?>"></i>
                </label>
            </h6>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="basic_bootstrap_tbl custom-toolbar">
                        <?= GridView::widget([
                            'id' => 'grid-extension-master-index',
                            'dataProvider' => $dataProvider,
                            'layout' => Yii::$app->layoutHelper->get_layout_str('#extension_master_search'),
                            'options' => [
                                'class' => 'grid-view-color',
                            ],
                            'columns' => [
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update}{delete}',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'header' => ExtensionMasterModule::t(
                                        'extensionmaster', 'action'
                                    ),
                                    'contentOptions' => [
                                        'class' => 'text-center inline-class',
                                        'style' => 'width: 10%',
                                    ],
                                    'buttons' => [
                                        'update' => function ($url) {
                                            isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
                                            $url = $url . "&page=" . $page;

                                            return (1 ? Html::a('<i class="fa fa-pencil"></i>', $url, [
                                                'data-toggle' => 'popover',
                                                'data-placement' => 'top',
                                                'data-trigger' => "hover",
                                                'data-content' => ExtensionMasterModule::t(
                                                    'extensionmaster', 'update'
                                                ),
                                                'data-pjax' => 0,
                                                'class' => 'btn btn-primary btn-sm',
                                            ]) : '');
                                        },
                                        'delete' => function ($url) {
                                            isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
                                            $url = $url . "&page=" . $page;

                                            return (1 ? Html::a('<i class="fa fa-trash"></i>', $url, [
                                                'data-toggle' => 'popover',
                                                'data-placement' => 'top',
                                                'data-trigger' => "hover",
                                                'data-content' => ExtensionMasterModule::t(
                                                    'extensionmaster', 'delete'
                                                ),
                                                'class' => 'btn btn-danger btn-sm',
                                                'data-pjax' => 0,
                                                'data-confirm' => ExtensionMasterModule::t('extensionmaster',
                                                    'delete_confirm'
                                                ),
                                                'data-method' => 'post',
                                            ]) : '');
                                        },
                                    ],
                                ],

                                [
                                    'format' => 'raw',
                                    'header' => 'Login',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'value' => function ($model) {
                                        if ($model->em_status == 'Y') {
                                            $domain = TenantMaster::findDomainName($model->tm_id);
                                            $username = $model->em_number . '@' . $domain;
                                            return '<form action="/extensionmaster/extensionmaster/user-login" method="post">
                                                <input name="LoginForm[username]" value="' . $username . '" type="hidden">
                                                <button type="submit" class="btn btn-primary btn-sm" name="login-button"><i class="fa fa-sign-in"></i></button>
                                                </form>';
                                        } else {
                                            return '<button type="submit" disabled class="btn btn-primary btn-sm" name="login-button"><i class="fa fa-sign-in"></i></button>';
                                        }
                                    },
                                ],
                                [
                                    'attribute' => 'em_status',
                                    'format' => 'raw',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'value' => function ($model) {
                                        $modelName = get_class($model);
                                        return $model->em_status == 'Y' ? ExtensionMasterModule::t('extensionmaster',
                                            '<a class="btn btn-success tag-change active_status_change" data-value="' . $model->em_id . '" data-model="' . $modelName . '" data-status="em_status">' . ExtensionMasterModule::t('extensionmaster',
                                                'active') . '</button>') :
                                            ExtensionMasterModule::t('extensionmaster',
                                                '<a class="btn btn-danger tag-change inactive_status_change" data-value=' . $model->em_id . '" data-model="' . $modelName . '" data-status="em_status">' . ExtensionMasterModule::t('extensionmaster',
                                                    'inactive') . '</button>');
                                    },
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'em_name',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'em_number',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'enableSorting' => false,
                                ],
                                [
                                    'header' => ExtensionMasterModule::t(
                                        'extensionmaster', 'sip_Webusername'
                                    ),
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'value' => function ($model) {
                                        return $model->em_number . '@' . TenantMaster::findDomainName($model->tm_id);
                                    },
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'em_password',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'em_media_anchor',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model->em_media_anchor == 'ANCHOR'
                                            ? '<span class="tag tag-pill tag-danger tag-lg">'
                                            . ExtensionMasterModule::t(
                                                'extensionmaster', "ANCHOR"
                                            ) . '</span>'
                                            : '<span class="tag tag-pill tag-warning tag-lg">'
                                            . ExtensionMasterModule::t(
                                                'extensionmaster', "DEFAULT"
                                            ) . '</span>';
                                    },
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'em_email',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'contentOptions' => ['class' => 'text-center'],
                                    'enableSorting' => false,
                                ],
                                [
                                    'attribute' => 'directoryDomain',
                                    'headerOptions' => ['class' => 'text-center'],
                                    'value' => function () {
                                        $domain = DirectoryDomain::findOne(['tm_id' => Yii::$app->user->identity->tm_id]);

                                        return $domain->dd_domain;
                                    },
                                    'enableSorting' => false,
                                ],

                            ],
                            'tableOptions' => [
                                'class' => 'table table-striped display nowrap table-bordered sorting_asc',
                                'id' => 'table1',
                                'data-plugin' => 'bootstraptable',
                                'data-height' => Yii::$app->session->get('per-page-result') == 5 ? '320' : '480',
                                'data-toolbar' => '#toolbar',
                                'data-show-columns' => 'true',
                                'data-icons-prefix' => 'fa',
                                'data-mobile-responsive' => 'false',
                            ],
                        ]); ?>
                    </div>
                    <?php Pjax::end(); ?>                </div>
            </div>
        </div>
    </div>
</div>