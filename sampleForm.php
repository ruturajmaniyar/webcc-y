<?php

use app\modules\ecosmob\extensionmaster\assets\ExtensionMasterAsset;
use app\modules\ecosmob\extensionmaster\ExtensionMasterModule;
use app\modules\ecosmob\extensionmaster\models\ExtensionMaster;
use app\modules\ecosmob\tenantmaster\models\TenantMaster;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\ecosmob\extensionmaster\models\ExtensionMaster */
/* @var $form yii\widgets\ActiveForm */
/* @var $e911Locations array */
/* @var $userCallRecordKey array */

ExtensionMasterAsset::register($this);
?>

<div class="extension-master-form" id="extension-master-form">

    <?php $form = ActiveForm::begin([
        'id' => 'extension-master-active-form',
    ]); ?>
    <?= Html::hiddenInput('hdn_for_terms_n_condition', $userCallRecordKey, ['id' => 'hdn_for_terms_n_condition']); ?>
    <?= Html::hiddenInput('hdn_for_terms_n_condition_title', ExtensionMasterModule::t('extensionmaster', 'hdn_for_terms_n_condition_title'), ['id' => 'hdn_for_terms_n_condition_title']); ?>
    <div class="card-accordions" id="card-tm">
        <div id="accordion-tenant" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header">
                    <a class="card-title" data-toggle="collapse"
                       data-parent="#accordion-tenant"
                       aria-expanded="true"
                       href="#collapseOne">
                        <h6 class="mb-0">
                            <span class="fa fa-anchor"></span>
                            <b><?= ExtensionMasterModule::t('extensionmaster', 'exuser_details') ?></b>
                        </h6>
                    </a>
                </div>
                <div id="collapseOne" class="collapse in">
                    <div class="card-block card-tm">
                        <div class="row">
                            <div class="form-group">
                                <?= $form->field($model, 'em_name',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => ExtensionMasterModule::t(
                                        'extensionmaster', 'name'
                                    ),
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'name') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "name_label") . '" ></i></span>') ?>
                                <?= $form->field($model, 'em_number',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                    'maxlength' => true,
                                    'readonly' => !$model->isNewRecord,
                                    'title' => ExtensionMasterModule::t('extensionmaster', 'cant_change_exuser_number'),
                                    'placeholder' => ExtensionMasterModule::t('extensionmaster', 'exuser_number'),
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'exuser_number') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "exuser_number_label") . '" ></i></span>') ?>
                            </div>
                        </div>
                        <?php if ($model->isNewRecord) { ?>
                            <div class="raw">
                                <div class="col-xs-12 col-md-6">

                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div style="color:#55595c"><i>
                                            * <?= ExtensionMasterModule::t('extensionmaster',
                                                'cant_change_exuser_number'); ?></i>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="row">
                            <div class="form-group">
                                <?= $form->field($model, 'em_password',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => ExtensionMasterModule::t('extensionmaster', 'password'),
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'password') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "password_label") . '" ></i></span>') ?>

                                <?= $form->field($model, 'em_email',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                    'maxlength' => true,
                                    'placeholder' => ExtensionMasterModule::t('extensionmaster', 'email'),
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'email') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "email_label") . '" ></i></span>') ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php echo $form->field(
                                    $model, 'em_sip_password', [
                                        'options' => ['class' => 'col-xs-9 col-md-5'],
                                        'template' => '{label}<div class="input-group">{input}<span class="input-group-btn"><button id="show_sip_password" class="btn btn-secondary" type="button"><span id="change_icon" class="fa fa-eye"></span></button></span></div>{error}'
                                    ]
                                )->passwordInput([
                                    'placeholder' => ExtensionMasterModule::t('extensionmaster', 'em_sip_password'),
                                    'maxlength' => true,
                                    'readOnly' => 'readOnly'
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'em_sip_password') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_sip_password_label") . '" ></i></span>'); ?>


                                <div class="col-sm-1 col-xs-1">
                                    <?= Html::button(ExtensionMasterModule::t('extensionmaster', 'generate'), [
                                        'class' => 'btn btn-warning btn-sm btn-generate',
                                        'onclick' => '$.post( "' .
                                            Url::to('/extensionmaster/extensionmaster/generate-password') . '", function( data ) {
                                                $("#extensionmaster-em_sip_password").val(data);
                                            });',
                                    ]) ?>
                                </div>

                                <div class="col-xs-12 col-md-3 btn-switch">
                                    <label>
                                        <?php
                                        echo $form->field($model, 'em_aliases')->widget(
                                            Select2::classname(), [
                                            'options' => ['multiple' => true],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                                'maximumSelectionLength' => 60,
                                                'maximumInputLength' => 15,
                                                'tags' => true,
                                                'maintainOrder' => true,
                                                'tokenSeparators' => [',', ' '],
                                                'onkeypress' => (new \yii\web\JsExpression('function() {
                                                        return isValidPrfix(event); 
                                                 }'
                                                )),
                                            ],
                                        ])->label(ExtensionMasterModule::t('extensionmaster', 'exuser_aliases') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "user_aliases_label") . '" ></i></span>') ?>


                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-md-6">
                                    <div id="usePhoneGroups">
                                        <?php
                                        echo $form->field($extensionGroupMapping, 'ug_id')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map($extensionGroupsList, 'ug_id', 'ug_name'),
                                            'options' => [
                                                'placeholder' => ExtensionMasterModule::t('extensionmaster', 'select'),
                                                'multiple' => true,
                                            ],
                                        ])->label(ExtensionMasterModule::t('extensionmaster', 'user_group') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "user_group") . '" ></i></span>');

                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-5 ml-1">
                                    <label> <?= ExtensionMasterModule::t('extensionmaster', 'use_user_group') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "use_user_group_label") . '" ></i></span>' ?> </label>
                                    <?= $form->field($extensionGroupMapping, 'use_user_group', [
                                        'template' => '<div class="checkbox-squared">{input}<label for="usergroup-checkbox" class=""></label><span class=""></span></div>',
                                        'options' => ['class' => 'ml-3', 'style' => 'margin-top:5px'],
                                    ])->checkbox([
                                        'label' => false,
                                        'id' => 'usergroup-checkbox',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php
                                if (!$model->isNewRecord) {
                                    echo $form->field($model, 'em_status', ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList([
                                        'Y' => 'Active',
                                        'N' => 'Inactive',
                                    ])->label(ExtensionMasterModule::t('extensionmaster', 'status') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "status_label") . '" ></i></span>');
                                }
                                ?>
                            </div>
                        </div>

                        <div class="hseparator"></div>

                        <div class="row">
                            <div class="form-group">
                                <?= $form->field($model, 'em_external_caller_id', ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                    'maxlength' => true,
                                    'autocomplete' => 'off',
                                    'placeholder' => ExtensionMasterModule::t('extensionmaster', 'caller_id'),
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'caller_id') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "caller_id_label") . '" ></i></span>') ?>

                                <div class="note col-md-6 col-xs-12">
                                    <?= ExtensionMasterModule::t('extensionmaster', 'configuration_label') ?>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <?= $form->field($model, 'em_media_anchor',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList([
                                    'DEFAULT' => 'DEFAULT',
                                    'ANCHOR' => 'ANCHOR',
                                ])->label(ExtensionMasterModule::t('extensionmaster', 'media_anchor') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "media_anchor_label") . '" ></i></span>'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?= $form->field($model, 'em_outbound_regex',
                                    ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput(['maxlength' => 500, 'placeholder' => ExtensionMasterModule::t('extensionmaster', 'default') . ' : (.*)'])->label(ExtensionMasterModule::t('extensionmaster', 'em_outbound_regex') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_outbound_regex_label") . '" ></i></span>'); ?>
                            </div>
                        </div>
                        <?php
                        if (in_array(Yii::$app->params['services']['E911'],
                            TenantMaster::getAssignServiceList(Yii::$app->user->identity->tm_id))) {
                            ?>
                            <div class="row">
                                <div class="form-group">
                                    <?= $form->field($model, 'e9_id',
                                        ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList($e911Locations, ['prompt' => ExtensionMasterModule::t('extensionmaster', 'select')])->label(ExtensionMasterModule::t('extensionmaster', 'e9_id') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "e9_id_label") . '" ></i></span>'); ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="form-group">
                                <?php
                                if (in_array(Yii::$app->params['services']['VOICEMAIL_EXTENSION'],
                                    TenantMaster::getAssignServiceList(Yii::$app->user->identity->tm_id))) {
                                    ?>
                                    <?= $form->field($model, 'em_voicemail_service',
                                        ['options' => ['class' => 'col-xs-12 col-md-6']])->dropDownList([
                                        '1' => 'Enable',
                                        '0' => 'Disable',
                                    ])->label(ExtensionMasterModule::t('extensionmaster', 'vociemail_service') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "voicemail_service_label") . '" ></i></span>'); ?>

                                    <?= $form->field($model, 'em_voicemail_password',
                                        ['options' => ['class' => 'col-xs-12 col-md-6']])->textInput([
                                        'placeholder' => ExtensionMasterModule::t('extensionmaster', 'em_voicemail_password'),
                                        'maxlength' => true,
                                        'autocomplete' => 'new-password',
                                    ])->label(ExtensionMasterModule::t('extensionmaster', 'em_voicemail_password') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover"  data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_voicemail_password_label") . '" ></i></span>'); ?>

                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">

                                <div class="col-xs-6 col-md-3">
                                    <label> <?= ExtensionMasterModule::t('extensionmaster', 'ug_glob_addbook_mem') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "ug_glob_addbook_mem_label") . '" ></i></span>' ?> </label>
                                    <?= $form->field($model, 'em_glob_addbook_mem', [
                                        'template' => '<div class="checkbox-squared">{input}<label for="checkbox-squared1" class=""></label><span class=""></span></div>',
                                        'options' => ['class' => 'ml-3'],
                                    ])->checkbox([
                                        'label' => false,
                                        'id' => 'checkbox-squared1',
                                    ]) ?>
                                </div>

                                <div class="col-xs-6 col-md-3">
                                    <label> <?= ExtensionMasterModule::t('extensionmaster', 'ug_shared_flag') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "ug_shared_flag_label") . '" ></i></span>' ?> </label>
                                    <?= $form->field($model, 'em_shared_flag', [
                                        'template' => '<div class="checkbox-squared">{input}<label for="checkbox-squared2" class=""></label><span class=""></span></div>',
                                        'options' => ['class' => 'ml-2'],
                                    ])->checkbox([
                                        'label' => false,
                                        'id' => 'checkbox-squared2',
                                    ]) ?>
                                </div>

                                <?php if (in_array(Yii::$app->params['services']['CALL_TRANSFER'], TenantMaster::getAssignServiceList(Yii::$app->user->identity->tm_id))) { ?>
                                    <div class="col-xs-6 col-md-3">
                                        <label> <?= ExtensionMasterModule::t('extensionmaster', 'em_call_transfer') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_call_transfer_label") . '" ></i></span>' ?> </label>
                                        <?= $form->field($model, 'em_call_transfer', [
                                            'template' => '<div class="checkbox-squared">{input}<label for="em_call_transfer" class=""></label><span class=""></span></div>',
                                            'options' => ['class' => 'ml-3'],
                                        ])->checkbox([
                                            'label' => false,
                                            'id' => 'em_call_transfer',
                                        ]) ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <?php if (in_array(Yii::$app->params['services']['CALL_RECORDING'], TenantMaster::getAssignServiceList(Yii::$app->user->identity->tm_id))) { ?>
                                    <div class="col-xs-6 col-md-3">
                                        <label> <?= ExtensionMasterModule::t('extensionmaster', 'em_call_recording') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_call_recording_label") . '" ></i></span>' ?> </label>
                                        <?= $form->field($model, 'em_call_recording', [
                                            'template' => '<div class="checkbox-squared">{input}<label for="em_call_recording" class=""></label><span class=""></span></div>',
                                            'options' => ['class' => 'ml-3'],
                                        ])->checkbox([
                                            'label' => false,
                                            'id' => 'em_call_recording',
                                        ]) ?>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label> <?= ExtensionMasterModule::t('extensionmaster', 'em_auto_recording') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_auto_recording_lable") . '" ></i></span>' ?> </label>
                                        <?= $form->field($model, 'em_auto_recording', [
                                            'template' => '<div class="checkbox-squared ml-3">{input}<label for="em_auto_recording" class=""></label><span class=""></span></div>',
                                            'options' => ['style' => 'margin-left:-24px'],
                                        ])->checkbox([
                                            'label' => false,
                                            'id' => 'em_auto_recording',
                                        ]) ?>
                                    </div>
                                <?php } ?>
                                <div class="col-xs-6 col-md-3">
                                    <label> <?= ExtensionMasterModule::t('extensionmaster', 'em_allow_multi_login') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_allow_multi_login_label") . '" ></i></span>' ?> </label>
                                    <?= $form->field($model, 'em_app_multi_login', [
                                        'template' => '<div class="checkbox-squared">{input}<label for="em_app_multi_login" class=""></label><span class=""></span></div>',
                                        'options' => ['class' => 'ml-3'],
                                    ])->checkbox([
                                        'label' => false,
                                        'id' => 'em_app_multi_login',
                                    ]) ?>
                                </div>
                                <?php if (in_array(Yii::$app->params['services']['PERSONAL_AUTO_ATTENDANT'], TenantMaster::getAssignServiceList(Yii::$app->user->identity->tm_id))) {
                                    if (ExtensionMaster::getPaaLimit() || $model->em_paa_service === '1') {
                                        ?>
                                        <div class="col-xs-6 col-md-3">
                                            <label> <?= ExtensionMasterModule::t('extensionmaster', 'em_paa_service') . ' <span><i class="icon fa fa-question-circle fa-lg" data-toggle= "popover" data-placement="top" data-trigger = "hover" data-content = "' . ExtensionMasterModule::t("extensionmaster", "em_paa_service_label") . '" ></i></span>' ?> </label>
                                            <?= $form->field($model, 'em_paa_service', [
                                                'template' => '<div class="checkbox-squared">{input}<label for="em_paa_service" class=""></label><span class=""></span></div>',
                                                'options' => ['class' => 'ml-3'],
                                            ])->checkbox([
                                                'label' => false,
                                                'id' => 'em_paa_service',
                                            ]) ?>
                                        </div>

                                    <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hseparator"></div>
    <div class="row">
        <div class="form-group col-sm-offset-5 col-md-offset-5 col-xs-offset-2">
            <?= Html::submitButton(
                $model->isNewRecord
                    ? ExtensionMasterModule::t('extensionmaster', 'create')
                    : ExtensionMasterModule::t('extensionmaster', 'update'),
                [
                    'class' => $model->isNewRecord ? 'btn btn-primary btn-round-left' : 'btn btn-primary btn-round-left',
                ]) ?>
            <?php if ($model->isNewRecord) {
                echo Html::a(ExtensionMasterModule::t('extensionmaster', 'cancel'),
                    ['index'], ['class' => 'btn btn-danger btn-round-right']);
            } else {
                echo Html::a(ExtensionMasterModule::t('extensionmaster', 'cancel'),
                    ['index', 'page' => isset($page) ? $page : Yii::$app->session->get('page')],
                    ['class' => 'btn btn-danger btn-round-right']);
            } ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>